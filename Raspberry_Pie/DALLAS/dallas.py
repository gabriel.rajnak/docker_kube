import os
import time

def read_temp_raw(file):
    f = open(file,'r');
    if f.closed:
        return 
    lines = f.readlines();
    f.close()
    return lines

def read_temp(file):
    if not os.path.isfile(file):
        return ""
    lines = read_temp_raw(file)
    counter =5;
    while lines[0].strip()[-3:] != 'YES':
        if counter == 0:
             print("Chyba tam YES")
            #sys.exit(0);
             return
        counter=counter-1
        time.sleep(0.2)
        lines = read_temp_raw(file)

    pos = lines[1].find('t=')
    if pos != -1:
        temp_string = lines[1][pos+2:]
        temp = (float(temp_string) / 1000.0)
        return temp

path = "/Users/bank365/gitlab/blockchain/LAB/DALLAS/w1"

def file_list():        
    dir_list = os.listdir(path)
    return dir_list

def read_sensor(path:str):
    # f1= f"{path}/w1_slave"
    filename= f"{path}/temperature"
    value = open(filename, 'r').read() /1000.0
    item = path.split('/')
    id =item[-1]    
    result = f"temperature {{id={id}}} {value}\n"
    return result

def process()->str:
    files = file_list()
    output=""
    for f in files:
        output = output+read_sensor(f"{path}/{f}")
    return output        