# DS18b20 exporter (prometheus)

## Installation

```bash
pip install pyyaml

git clone ...
mkdir /etc/dallas
cp dallas.yaml /etc/dallas
# 
sudo cp dallas_exporter.py /usr/local/bin
sudo cp dallas_exporter.py.service /etc/systemd/system

sudo systemctl enable dallas_exporter.py.service
sudo systemctl start dallas_exporter.py.service

```

## Additional tags
You can create config file for addiotal tags for every sensor.
```yaml
metrics_name: "teplota"

internal_metrics: true

sensors:
    28-000006080231 : 
       location : 'dom'
       type : "out"
    28-000006080232 :
       location : 'vonku'
    28-000006080233 :
      location : 'hotel'
```

### ToDo
- customize listener port (8080 default).................................[ ]
   - e.g. port: 8080
- customize metrics name(temperature default) ...........................[x]
   - e.g. metrics_name: temp
- systemctl integration..................................................[ ]
- logging level setup....................................................[ ]
- verbose mode for debuging .............................................[ ]
- define no sensor metric notification
  - sensor_count  3 .....................................................[x]
- general tag for endpoint...............................................[ ]
- [local cpu temperature](https://www.raspberrypi-spy.co.uk/2020/11/raspberry-pi-temperature-monitoring/#:~:text=Reading%20temperature%20in%20Python%20It%E2%80%99s%20fairly%20easy%20to,temperature%20into%20a%20variable%3A%20cpu_temp%20%3D%20gz.CPUTemperature%20%28%29.temperature)
   - enabler in config
```bash
temp=$(('cat /sys/class/thermal/thermal_zone0/temp'/1000))
echo $temp
```


### Updates / changes
