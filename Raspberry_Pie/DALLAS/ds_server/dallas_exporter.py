from http.server import BaseHTTPRequestHandler, HTTPServer
import os
import yaml

# pip install pyyaml

path = "/sys/bus/w1/devices"
tag_file = "/etc/dallas/dallas.yaml"

configuration = {} # default empty

def file_list():        
    dir_list = os.listdir(path)
    return dir_list

def Load_tags(t_file:str):
    try:
        with open(t_file,'r') as f:
            tgs = yaml.safe_load(f)
        print(f"Local tags for sersors loaded from {t_file}")        
    except:
        print("No tags loaded.")
        tgs= {}
    return tgs

def prepare_tags(id, configuration):
    add_tags = f"id=\"{id}\""    
    # common tags
    if 'common_tags' in configuration:
        for key,value in configuration['common_tags'].items():            
            add_tags +=  f",{key}=\"{value}\""     
    # custom sersor tags    
    if 'sensors' in configuration and id in configuration['sensors']:                
        for key,value in configuration['sensors'][id].items():            
            add_tags +=  f",{key}=\"{value}\""     
    return add_tags

def read_sensor(filename:str):        
    try:
        value = int(open(filename, 'r').read()) /1000.0
        print("read sensor ", filename, value)
    except:
        print("Can't read ",filename)
        value = configuration['missing'] if 'missing' in configuration else "NONE"
    return value

def w1_temperature(id:str)->float:
    return read_sensor(f"{path}/{id}/temperature")

def cpu_temperature():
    return read_sensor("/sys/class/thermal/thermal_zone0/temp")

def metrics_formater(name:str, tags:str, value)->str:
    data = f"{name} {tags} {value}\n"
    return data

def process()->str:
    files = file_list()    
    output=""    
    metrics_name =  configuration['metrics_name'] if "metrics_name" in configuration else "temperature"
    # todo: doplnit popdmienku z konfiguraku

    value = cpu_temperature() 
    tags = prepare_tags('cpu',configuration)
    #output = output+ f"{metrics_name} {{{tags}}} {temperature}\n"
    output = output+ metrics_formater(metrics_name, tags, value)
    # w1 sensors
    for id in files:        
        if id == "w1_bus_master1":
          continue        
        temperature= w1_temperature(id)
        tags = prepare_tags(id,configuration)
        output = output+ f"{metrics_name} {{{tags}}} {temperature}\n"
    
    # report numer of w1 sensors
    count = len(files) - 1 if "w1_bus_master1" in files else 0
    tags = prepare_tags('w1_count',configuration)
    output = output+ f"count {{{tags}}} {count}\n"
        
    return output        

configuration = Load_tags(tag_file)

class MyHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.end_headers()
        response = ds.process()
        self.wfile.write(response.encode('utf-8'))

def run(server_class=HTTPServer, handler_class=MyHandler, port=8080):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print(f"Starting server on port {port}")
    httpd.serve_forever()

if __name__ == '__main__':
    run()
