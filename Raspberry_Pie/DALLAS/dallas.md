# DS18b20 teplomer
[DS18B20](https://github.com/hikhvar/w1_prometheus_exporter)

inspiracia

```bash


git clone https://github.com/hikhvar/w1_prometheus_exporter.git
cd w1_prometheus_exporter
```

```bash

sudo mkdir -p /opt/prometheus_temperature
sudo cp exporter.py /opt/prometheus_temperature
sudo cp temperature_sensor.service /etc/systemd/system

sudo systemctl enable temperature_sensor.service
sudo systemctl start temperature_sensor.service

```


scp .\*.* raspberrypi@192.168.1.67:/home/raspberrypi/ds_server/

