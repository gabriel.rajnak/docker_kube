
#
# Push metrics to prometheus pushgateway
#
import requests


# Define the Pushgateway URL and the metric data
pushgateway_url = 'https://push.aleron.sk'
metric_data = 'temperature{id="123"} 123\n'

def send_to_pushgateway():
    # Send the metric data to the Pushgateway
    try:
        response = requests.post(pushgateway_url + '/metrics/job/temperature_job', data=metric_data)
        response.raise_for_status()  # Raise an error for bad responses
        print("Metric pushed successfully.")
    except requests.exceptions.RequestException as e:
        print(f"Failed to push metric: {e}")




