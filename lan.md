# Organizacia domacej siete

## Staticke a dynamicke pridelenie adries klientom
| Nazov            | IP               | Login                   | DHCP |
|------------------|------------------|-------------------------|:----:|
| sand2            | 192.168.1.1      | c15h31cooh              |      |
| sand - GW        | 192.168.1.2      | admin / admin           |      |  
| nvo              | 192.168.1.3      | admin / C15h31cooh!     |      |
| sand0            | 192.168.1.4      | C15h31cooh!             |      |
| fort-knox        | 192.168.1.5:5000 | gabo / Uppdkv197        |      |
| zeus-server      | 192.168.1.6      | devops/1q2w3e4r         |      |
| dedko-mobil      |                  |                         |      |
| dedko-tablet     |                  |                         |      |
| Gabo-Imac        | 192.168.1.20     |                         | *    |
| Gabo-MacPro      | 192.168.1.22     |                         | *    |
| Gabo-Mobil       |                  |                         |      |
| Gabo-Bitcoin-ntb |                  |                         |      |
| Zuzka-MacMini    |                  |                         |      |
| Zuzka-mobil      |                  |                         |      |
| Zuzka-NCZI-ntb   |                  |                         |      |
| Viki-MacAir      |                  |                         |      |
| Viki-Wintel      |                  |                         |      |
| Viki-Mobil       |                  |                         |      |
| Saska-MacAir-M2  |                  |                         |      |
| saska-Mobil      |                  |                         |      |
|
| rpi-dom          | 192.168.1.110    |                         | *    |
| rpi-hotel        | 192.168.1.111    |                         | *    |
| virtualka        | 192.168.1.112    |                         | *    |
| Printer          | 192.168.1.254    |                         | *    |

- IP rezervovane na DHCP serveri

## DHCP
Bezi na sand v rozsahu 192.168.1.50 - 100.

---
# HW

### memory
```bash
dmidecode -t memory

lsmem

```