# IAM MFA
- vyber region
- IAM  
- users , create user
    - user name


Provide user access to the AWS Management Console - optional
If you're providing console access to a person, it's a best practice  to manage their access in IAM Identity Center.

Poskytnite používateľovi prístup k AWS Management Console – voliteľné
Ak osobe poskytujete prístup ku konzole, je osvedčeným postupom spravovať jej prístup v IAM Identity Center.

## Vytvorenie dummy usera len cez IAM
- name
- pridel grupu
- vygeneruj heslo
    - https://149207750652.signin.aws.amazon.com/console
    - test1
    - ipQ{G6V_

## Vytvorenie usera v IAM identity center (odporucany pristup)
- v AWS organization treba vytvorit workload accounty
    - dev
    - test
    - uat
    - prod

- v IAM identity center
    - vytvorit grupy 
- v AWS account pridelit grupu do uctov kam bude mat pristup
- v IAM Identity Center vytvor usera (add user)
    - pridel ho do grupy kam ma mat pristup


## MFA



---


## IAM policies
- identity based policies(user,group,role)
    - managed policies (reusable)
    - inline policies (pre specificky ucel)
- resource based policies   
    - S3 bucket policies
    - EC2 policies


## Identity based policies
- prideluju sa grupe, userom co mozu robit

## resource based policies
- popisuje co sa moze s danym resousom robit (read, write, delete)

# IAM for users

Priklad json zapisu :
```
{
    "Version":"2012-10-17",
    "Statement":{
        "Effect":"Deny",
        "Action":"*",
        "Resource":"*",
        "Condition": {
            "NoIpAddress": .....
        }
    }
}
```

# IAM for groups




# MFA pre usera
- povilit, aby si user mohol managovat ucet















---

# IAM aws cli


## vytvor user grupu
```aws iam create-group --group-name admin```
## Pridaj rolu grupe (napr. vyber z zozname admin)
```aws iam attach-group-policy --group-name admin --policy-arn arn```
## Vytvor usera
```aws iam create-login-profile --user-name gabo --password password```
## Vytvor user/secret key
```aws iam create-access-key --user-name gabo```

## Pridaj usera do grupy
```aws iam add-uset-to-group --group-name admin --user-name gabo```

---







