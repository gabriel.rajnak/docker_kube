# Lambda function

<https://stackoverflow.com/questions/45364967/set-expiration-of-cloudwatch-log-group-for-lambda-function>


### Expiracia logov pre lambda funckiu :

```
Resources:
  LambdaFunction:
    Type: AWS::Serverless::Function
    Properties:
      Handler: index.handler
      Runtime: nodejs6.10
      CodeUri: <your code uri>
      Policies: <your policies> 


  LambdaFunctionLogGroup:
    Type: "AWS::Logs::LogGroup"
    DependsOn: "LambdaFunction"
    Properties: 
      RetentionInDays: 14
      LogGroupName: !Join ["", ["/aws/lambda/", !Ref LambdaFunction]]
```