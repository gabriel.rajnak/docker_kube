#
# EC2 training
# https://www.youtube.com/watch?v=SLB_c_ayRMo&t=3001s
#
resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "Main vpc"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "Main GW"
  }
}

resource "aws_route_table" "my_route" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
  # route {
  #    ipv6_cidr_block        = "::/0"
  #   egress_only_gateway_id = aws_internet_gateway.gw.id
  # }
  tags = {
    Name = "my_route"
  }

}

resource "aws_subnet" "subnet" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.0.0.0/16"
  availability_zone = "eu-central-1a"
  tags = {
    Name = "Main subnet"
  }

}

resource "aws_route_table_association" "assoc" {
  subnet_id      = aws_subnet.subnet.id
  route_table_id = aws_route_table.my_route.id
}

resource "aws_security_group" "allow_tls" {
  name   = "allow_tls"
  vpc_id = aws_vpc.main.id

  ingress {
    description = "22 port"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "80 port"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "443 port"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    description = "output"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow ssh/https"
  }
}

resource "aws_network_interface" "web1" {
  subnet_id       = aws_subnet.subnet.id
  private_ips     = ["10.0.0.50"]
  security_groups = [aws_security_group.allow_tls.id]
}

resource "aws_eip" "eip1" {
  vpc                       = true
  network_interface         = aws_network_interface.web1.id
  associate_with_private_ip = "10.0.0.50"
  depends_on                = [ aws_internet_gateway.gw ]
}

resource "aws_instance" "webserver" {
  ami               = "ami-0d527b8c289b4af7f"
  instance_type     = "t2.micro"
  availability_zone = "eu-central-1a"
  key_name          = "aws-gabo"
  network_interface {
    device_index = 0
    network_interface_id = aws_network_interface.web1.id
  }
user_data = <<-EOF
  #!/bin/bash
  sudo apt update -y
  sudo apt install apache2 -y
  sudo systemctl start apache2
  sudo bash -c 'echo 'toto je moj prvy web' > /var/www/html/index.html'
  EOF
  tags = {
    Name = "Moj web server"
  }
}

