

# =======================================
# Vytvorim dve VPC
# =======================================

resource "aws_vpc" "dev_vpc" {
  cidr_block = "172.32.0.0/20"
  tags = {
    "Name" : "Development"
  }
}

resource "aws_vpc" "test_vpc" {
  cidr_block = "172.33.0.0/20"
  tags = {
    "Name" : "Test"
  }
}

# =====================================
# pridam do nich networky
# =====================================
resource "aws_subnet" "dev_sub" {
  cidr_block = "172.32.0.0/20"
  vpc_id     = aws_vpc.dev_vpc.id
  tags = {
    "Name" : "Development"
  }
}

resource "aws_subnet" "test_sub" {
  cidr_block = "172.33.0.0/20"
  vpc_id     = aws_vpc.test_vpc.id
  tags = {
    "Name" : "Test"
  }
}
# ===================================
# ec2 
# ===================================

