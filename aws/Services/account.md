# aws account

- resourses
- users/roles
- settings

## root account
- hlavny ucet, 
- urceny len na hlavnu sprabie
- nie je urceny na beznu pracu

## IAM user
- skupina opravneni na resoursy
- jeden account moze mat viac userov


## Multiple accounts
- organizovanie zdrojov
- obchadzanie limit pre jeden account(vpc,...)
- access mgm. 
- decentralizacia
- zdielanie zdrojov


## 9 Best practices AWS accounts
1. Organizanizacia podla pozutia a bezpecnosti
2. Pouzi bezpecnost na organizacnu jednotku  radsej ako na account
3. Vyhni sa velkej organizacii
4. Zacni v malom a pomaly rozsiruj
5. Vyhni sa deploymentu do hlavneho uctu
6. Oddeluj produkcne  neprodukcne prostredie
7. Priprad jeden alebo molu mnozunu workoadu to kazdeho prdukcneho accountu
8. Pouzi federativny pristup na zjednodusenie userov -> account
9. Pouzivaj automatizaciu a skalovanie

---
# Organization
- mgm. account
- member accounts(worloads)
- organization unit(OU)
    -  viac uctov
- users


# Federacia identity
- identity provide
- aws SSO
- IAM

---



---
# Google SSO
[https://aws.amazon.com/blogs/security/how-to-use-g-suite-as-external-identity-provider-aws-sso/]


---https://dev.to/aws-builders/my-personal-aws-account-setup-iam-identity-center-temporary-credentials-and-sandbox-account-39mc?signin=true