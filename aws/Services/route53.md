# Route 53
Je to DNS sluzba.

### Priklad vytvorenia zaznamu
```
resource "aws_route53_record" "twilio-1" {
  zone_id = aws_route53_zone.public.zone_id
  name    = "25620197.twilio.test.dsp.365.bank"
  type    = "CNAME"
  records = ["sendgrid.net"]
  ttl     = var.ttl
}
```

## Pristup  VPC ku route53
- mmoze byt public alebo private.
- Aby bolo route53 dostupne pre VPC, je treba spravit asociaciu a utorizaciu.
- vpc/sub_net route53 musi byt prepojene aj na sietovej urovni


```
resource "aws_route53_vpc_association_authorization" "test_vpc_onprem_test_zone" {
  vpc_id  = data.terraform_remote_state.test-legacy.outputs.vpc.vpc_id
  zone_id = aws_route53_zone.onprem_test.zone_id

  provider = aws.network
}

resource "aws_route53_zone_association" "test_vpc_onprem_test_zone" {
  vpc_id  = data.terraform_remote_state.test-legacy.outputs.vpc.vpc_id
  zone_id = aws_route53_zone.onprem_test.zone_id

  provider = aws.test
}
```
