# EKS Kubeconfig
<https://docs.aws.amazon.com/eks/latest/userguide/create-kubeconfig.html>

```
aws --profile=devops sts get-caller-identity
```


{
    "UserId": "xxxxxxxxx:rajnak@pabk.sk",
    "Account": "829421977047",
    "Arn": "arn:aws:sts::829421977047:assumed-role/AWSReservedSSO_AdministratorAccess_xxxxxxxxx/rajnak@pabk.sk"
}

```
aws eks update-kubeconfig --region region-code --name cluster-name
```

