# Cloudwatch

Hlavny logovaci nastroj.

### Piliere observability

*Logs* — any custom log data, Application Logs, Nginx Logs, Lambda Logs​

*Metrics* Represents a time-ordered set of data points. A variable to monitor. eg. Memory Usage​

*Events* — trigger an event based on a condition eg. ever hour take a snapshot of the server (now known as Amazon EventBridge)​

*Alarms* — triggers notifications based on metrics that breach a defined threshold​

*Dashboards* — create visualizations based on metrics​

### Logs
- export do S3
- stream do ElasticSearch
- stream CloudTrail do CloudWatch
- log security
- log filtering
- log retention (ako dlho uchvavat data)



### Log group
Kolekcia log streamov.

### Log Stream
Log group obsahuje streamy(log file)

### Log Event
je konkretny zaznam v log streame(timestamp, a udalost)

### Log insights
Vastroj na filtrovanie a prehladavanie eventov. Jazyk vyzera podobne ako SQL.

### Log Fields
- @message raw data logu
- @timestamp casova znacka
- @ingestionTime cas prijatia logu
- @logStream nazov zdroja dat/logu
- @log log group id.

### Log Alarm

### Log Dashboard

### CloudWatch Agent
- agent pre EC2 pre monitorovanie.
- hostlevel metriky bez potreby instalovania agenta(cpu, network, disk, status)
- agent level metriky(treba agenta) ram, disk swap, disk usage, strnakovanie, logovanie na urovni os
(/etc/aws/awslogs.conf)


### Custom high resolution metrics
logovanie na urovni sekund.(este pozriem detajly)

### EventBridge
sluzba zabezpecuje routovanie eventov kam treba.

Komponenty :
- event bus
  - default event bus
  - custom event bus
  - SaaS event bus
  - producer (sluzba emitujuca data)
  - Partner surces
  - Rules
  - Targets




