# AWS networking
---
<https://docs.aws.amazon.com/vpc/latest/userguide/how-it-works.html>

# Komponenty
- VPC (virtual private cloud) logicka jednotka, obsahuje subnety
- subnet - podsiet
- TGW (Tarnzit gateway) prepojuje VPC
- komponenty lambda, EC2 - virtualka( je v subnete)
  

## subNet
Komponenty v ramci jedneho subnetu sa vidia automaticky.

## subNet - subNet v ramci VPC
TODO: subnet -subnet networking


### Nastav routovanie so sbNetu do TGW
```
resource "aws_route" "network_wireguard_to_vpn_dsp1" {
  for_each               = toset(flatten(values(local.route_tables["network-wireguard"])))
  route_table_id         = each.value
  destination_cidr_block = data.terraform_remote_state.net_def.outputs.vpn["365-dsp1"]
  transit_gateway_id     = data.terraform_remote_state.net_tgw.outputs.tgw.tgw.id

  provider = aws.network
}
```


## VPC1-subNet - VPC2-subNet
- tranzit gw
- vpc
- subnet
- komponent

Prepojenie :
- tgw association
- tgw routes

---
