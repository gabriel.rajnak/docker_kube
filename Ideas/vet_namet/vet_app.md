

---
# VETapp namet

9.12.2024

## Aplikacia pre veterniarne ambulancie a ich klientov

- ambulacia
- salon krasy
- cvicisko/trener
- vet-taxi
- hotel pre zvierata

### Zakladne procesy

Registracia :
    - typ: 
        - chovatela
        - sluzba 
    - email
    - real name
    - description

Relationship:
    - id
    - customer
    - provider
    - uroven pristupu
    -platnost

1. Zaradenie zvierata do evidencie 
    - majitelov
    - veterinarom

2. priradenie zvierata majitelovi

3. Zmena vlastnika

4. Narodenie zvierata
    - urcenie rodicov

5. Umrtie zierata

6. Navsteva u lekara
    - vysetrenie
    - liecba
    - vakcianacia ( vakcina, expiracia )
    - operacia
    - utratenie

7. Domaca starostlivost
    - podavanie liekov
    - strihanie ....

8. Naplanovanie stretnutia
    - sluzba
    - ponukne terminy
    - zakaznik i vyberie
    - manualne/automaticke potvrdenie

9. pripomienka expiracie vakcinacie


- zoznam zakaznikov
    - primarny veterinar

- zoznam zvierat
    - podrobnosti
        - datum narodenia
        - datum umrtia
        - druh / rasa
        - rodicia
        - CHIP-ID
        - meno
        - pohlavie
        - poznamka

- vlastnictvo
    - vlastnik
    - zviera
    - od - do
    - poznamka

- zaznam o : (informaciado_karty, ukon )
    - id_kto_vlozil_zaznam (vlastnik, lekar )
    - navsteve/konzultacia/kontrola
    - vakcinacia (nova, revakcinacia, expiracia)
    - chip NOVY/ZNEMA
    - liecba
    - operacny zakrok
    - porod
    - umrtie zierata

### Spravy
    Umoznit posielat spravy od chvatela- veterinarovi a naopak.

    - id
    - timestamp
    - sender
    - reciepeint
    - relation (kod coho sa to tyka)
    - message
    - status (new, red, closed, ....)
    
### Idea
- vsetky zaznamy su majetkom zakaznika
- udeluje suhlas s nahliadnutim do zaaznamu
- umozni to lahku migraciu medzi veterinarmi

## Opravnenia ( doriesit scenare)
Navrhnut mechanizmus udelenia suhlasu pristupu do zaznamov.
    - id_owner
    - id_vet
    - id_animal
    - from
    - to
    - level (read, add, write, delete)
