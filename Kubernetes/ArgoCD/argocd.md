# ArgoCD

[https://www.youtube.com/watch?v=MeU5_k9ssrs]

- normalny/zakladny pristup je push z CI/CD do k8s
    - CI/CD potrebuje prava pristupu do k8s

- ArgoCD bezi na K8s o ktory sa stara
    - pull-uje git co ma aplikovat
    - kontroluje ci aplikacia bezi ako ma

- oddelit repozitare
    - zdrojovy kod aplikacie
    - configuracia aplikacie (HELM, k8s-manifest)
    - image repository

## Instalacia
- deploy argoCD
- deploy CRD s konfiguraciou (obsahuje git repo a k8s kam to ma aplikovat)


```https://gitlab.com/nanuchi/argocd-app-config```

```bash
# install ArgoCD in k8s
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

# access ArgoCD UI
kubectl get svc -n argocd
kubectl port-forward svc/argocd-server 8080:443 -n argocd

# login with admin user and below token (as in documentation):
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 --decode && echo

# you can change and delete init password
```

----

# HELM




# Kustomize
[https://www.youtube.com/watch?v=JLrR9RV9AFA&t=445s]

