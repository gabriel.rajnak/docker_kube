terraform {
  required_providers {
    virtualbox = {
      source = "terra-farm/virtualbox"
      version = "0.2.2-alpha.1"
    }
  }
}

provider "virtualbox" {
  # Configuration options
}

# There are currently no configuration options for the provider itself.
# http://cloud-images.ubuntu.com/noble/20231031/     
# vagrant / vagrant
resource "virtualbox_vm" "node" {
  count     = 1
  name      = format("Noble-%02d", count.index + 1)
  # image     = "https://app.vagrantup.com/ubuntu/boxes/bionic64/versions/20180903.0.0/providers/virtualbox.box"
  image     = "http://cloud-images.ubuntu.com/mantic/20231014/mantic-server-cloudimg-amd64-vagrant.box"
  cpus      = 1
  memory    = "512 mib"
  
  #user_data = file("${path.module}/user_data")

  network_adapter {
    type           = "bridged" //"hostonly"
    host_interface = "en1"
    IPAddr = "192.168.1.99/24"
  }
  provisioner "file" {
    source      = "~/.ssh/id_rsa.pub"  # Path to your public SSH key
    destination = "/tmp/id_rsa.pub"
  }
  
}

output "IPAddr" {
  value = element(virtualbox_vm.node.*.network_adapter.0.ipv4_address, 1)
}

output "IPAddr_2" {
  value = element(virtualbox_vm.node.*.network_adapter.0.ipv4_address, 2)
}
