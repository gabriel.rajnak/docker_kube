# Jobs

```bash
kubectl create job hello --image=ubuntu -- echo "hello World"
```

```bash
kubectl create cronjob hello --image=busybox --schedule="*/5 * * * *" -- echo "Hello World"

```

