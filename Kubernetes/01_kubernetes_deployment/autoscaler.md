# autoscaler

[https://www.geeksforgeeks.org/how-to-use-kubernetes-horizontal-pod-autoscaler/?ref=ml_lbp]


Metrics: [https://signoz.io/blog/kubernetes-metrics-server/]
```
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
```


kubectl edit deployments.apps -n kube-system metrics-server


kubectl port-forward service/backend-service 8080:8080


## Naporovy test 
```
siege -c 250 -t 2m http://127.0.0.1:58421
```
