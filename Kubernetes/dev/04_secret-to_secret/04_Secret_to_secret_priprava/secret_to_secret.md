# Asseco   secret to kubernetes

- TLS kluc

# Distribucia  Secretov z AWS to secret in Kubernetes secret

# Detekuje ci sa menil secret, ak ano restartne pod
https://github.com/stakater/Reloader

# Migruje secrety
https://external-secrets.io/v0.6.0-rc1/provider/aws-secrets-manager/


---
## Pracovny namespace
```kubectl create ns secret-to-secret```


## Nainstaluj reloader
```
kubectl apply -f https://raw.githubusercontent.com/stakater/Reloader/master/deployments/kubernetes/reloader.yaml 
```


# helm install stakater/reloader --set reloader.watchGlobally=false --namespace 


```
apiVersion: v1
kind: Secret
metadata:
  name: mysecret
type: Opaque
namespace: secret-to-secret
data:
  username: YWRtaW4=
  password: MWYyZDFlMmU2N2Rm
```

## Vytvorim si cvicny deployment

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: web
  annotations:
    secret.reloader.stakater.com/reload: "mysecret"
spec:
  replicas: 50
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1        # how many pods we can add at a time
      maxUnavailable: 0
  selector:
    matchLabels:
      app: web
  template:
    metadata:
      labels:
        app: web
    spec:
       containers:
        - name: nginx
          image: nginx:1.19.1
          ports:
          - containerPort: 80
          volumeMounts:
          - name: mysecret-volume
            mountPath: /mnt
       volumes:
       - name: mysecret-volume
         secret:
           secretName: mysecret
```


## Zmenim daco v mysecret

```kubectl edit secret asseco-secret```

### Vsetky pody deploymentu sa potupne rebootnu
---------


# prehadzovanie aws secret do k8s secret
[link](https://external-secrets.io/v0.6.0-rc1/provider/aws-secrets-manager/)

- nastavenie IAM pre service account



## EKS Service Account credentials

```
apiVersion: v1
kind: ServiceAccount
metadata:
  annotations:
    eks.amazonaws.com/role-arn: arn:aws:iam::046945312995:role/dsp-sandbox-infra-eks-default-cluster
  name: my-serviceaccount
  namespace: default
  ```
## Odkaz na service account v secret store
  ```
apiVersion: external-secrets.io/v1beta1
kind: SecretStore
metadata:
  name: secretstore-sample
spec:
  provider:
    aws:
      service: SecretsManager
      region: eu-central-1
      auth:
        jwt:
          serviceAccountRef:
            name: my-serviceaccount
```


## Sledovanie a prehadzovanie secretu
```
apiVersion: external-secrets.io/v1beta1
kind: ExternalSecret
metadata:
  name: watcher-gabo-secret
spec:
  refreshInterval: 1m
  secretStoreRef:
    name: watcher-gabo-secret
    kind: SecretStore
  target:
    name: mysecret
    creationPolicy: Owner
  data:
  # - secretKey: login
  #   remoteRef:
  #     key: mysecret
  #     property: username # Tom
  - secretKey: mysecret
    remoteRef:
       key: mysecret
       #property: attr # Roger
```






## Manualny rollout/restart deployment
```k rollout restart deployment nginx-deployment```



## Priprava role

## Permission
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "secretsmanager:GetResourcePolicy",
                "secretsmanager:GetSecretValue",
                "secretsmanager:DescribeSecret",
                "secretsmanager:ListSecretVersionIds"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
```

## Trust relationship
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Federated": "arn:aws:iam::046945312995:oidc-provider/oidc.eks.eu-central-1.amazonaws.com/id/7F94D7739D40659988665C21BA120DC1"
            },
            "Action": "sts:AssumeRoleWithWebIdentity",
            "Condition": {
                "StringEquals": {
                    "oidc.eks.eu-central-1.amazonaws.com/id/7F94D7739D40659988665C21BA120DC1:aud": "sts.amazonaws.com"
                }
            }
        }
    ]
}
```
