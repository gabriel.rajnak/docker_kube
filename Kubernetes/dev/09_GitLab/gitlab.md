# Gitlab

https://artifacthub.io/packages/helm/gitlab/gitlab

```bash
helm repo add gitlab http://charts.gitlab.io/

kubectl create ns gitlab

helm install my-gitlab gitlab/gitlab --version 7.8.1 -f gitlab-values.yaml -n gitlab

helm delete my-gitlab -n gitlab
kubectl delete ns gitlab
```
v secrete  gitlab-initial-root-password si pozri heslo pre roota


## Gitlab runner

https://artifacthub.io/packages/helm/gitlab/gitlab-runner
```bash
helm install my-gitlab-runner gitlab/gitlab-runner --version 0.61.0 -f gitlab-runner-values.yaml -n gitlab

helm delete my-gitlab-runner -n gitlab
```

### TODO: 
- samostatna instalacia PostgreSQL
- Gitaly




# Gitlab ci/cd cvicenie
https://docs.bitnami.com/tutorials/create-ci-cd-pipeline-gitlab-kubernetes/

