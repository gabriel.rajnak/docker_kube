


https://www.ecloudcontrol.com/restrict-a-linux-user-to-specific-kubernetes-namespace/#:~:text=RESTRICT%20A%20LINUX%20USER%20TO%20SPECIFIC%20KUBERNETES%20NAMESPACE&text=Kubernetes%20RBAC%20is%20powered%20by,the%20namespace%20it%20belongs%20in%20.

## Vygenerujem si svoj kluc a ziadost na podpisanie pomocou CA v k8s
```bash
openssl genrsa -out john.key 2048
openssl req -new -key john.key -out john.csr -subj "/CN=john"
```
- pregenerujem csr do base64 ```cat john.csr | base64 | tr -d "\n"```

```yaml
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: john-csr
spec:
  groups:
  - system:authenticated
  request: $(cat john.csr | base64 | tr -d "\n")
  signerName: kubernetes.io/kube-apiserver-client
  usages:
  - digital signature
  - key encipherment
  - client auth
 ```

 `k apply -f sign_request.yaml `


```bash
gabo@MacBookPro rbac % kubectl get csr
NAME       AGE   SIGNERNAME                            REQUESTOR   REQUESTEDDURATION   CONDITION
john-csr   54s   kubernetes.io/kube-apiserver-client   admin       <none>              Pending
```

 - approvnem CSR
 `$ kubectl certificate approve john-csr`

 - uz je schvaleny
 ```bash
 gabo@MacBookPro rbac % kubectl get csr                       
NAME       AGE    SIGNERNAME                            REQUESTOR   REQUESTEDDURATION   CONDITION
john-csr   113s   kubernetes.io/kube-apiserver-client   admin       <none>              Approved,Issued
```

## Vytvorim kubeconfig s certifikatom
- najdem nazov k8s clustra
```
gabo@MacBookPro rbac % k config get-contexts
CURRENT   NAME       CLUSTER            AUTHINFO   NAMESPACE
*         microk8s   microk8s-cluster   admin      
gabo@MacBookPro rbac % 
```
```yaml
apiVersion: v1
kind: Config
clusters:
- name: microk8s-cluster
  cluster:
    certificate-authority-data: $(kubectl config view --raw --flatten -o json | jq -r '.clusters[0].cluster."certificate-authority-data"')
    server: $(kubectl config view --raw --flatten -o json | jq -r '.clusters[0].cluster.server')
contexts:
- name: microk8s-test
  context:
    cluster: microk8s-cluster
    namespace: vyvoj
    user: john
current-context: john-context
users:
- name: john
  user:
    client-certificate-data: $(kubectl get csr john-csr -o jsonpath='{.status.certificate}')
    client-key-data: $(cat john.key | base64 | tr -d '\n')
```





https://kubernetes.io/docs/reference/kubectl/quick-reference/

https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands


