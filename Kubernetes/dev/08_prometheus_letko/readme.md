# Prometheus
```bash
kubectl create ns prometheus

prometheus-community/kube-prometheus-stack

helm upgrade kube-prometheus-stack prometheus-community/kube-prometheus-stack -f $values --wait --timeout 60m0s --version 33.2.1

# doc toto vyskusat
https://github.com/digitalocean/Kubernetes-Starter-Kit-Developers/blob/main/04-setup-observability/prometheus-stack.md

```

[Anais PrometheusRules](https://www.youtube.com/watch?v=HwB2oWUdoT4&t=1609s_) |
[clanok](https://anaisurl.com/forwarding-security-metrics-to-slack-with-prometheus-and-alertmanager/) |
[repo ku clanku](https://github.com/Cloud-Native-Security/monitor-security)

https://github.com/prometheus-operator/prometheus-operator/blob/main/Documentation/user-guides/alerting.md

## Zakladna instalacia
```
# repo
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update

# install
helm install prometheus   prometheus-community/kube-prometheus-stack -n prometheus -f env/playground/prometheus-values.yaml

# delete
helm delete prometheus -n prometheus
```

## Blackbox-exporter
```bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update

helm install blackbox-exporter prometheus-community/prometheus-blackbox-exporter -n prometheus

helm install blackbox-exporter -n prometheus
```



---
## poznamky z BTCS
## Scrape endpointov (jobs)
```bash

# Deployment of scrape configs
- As xxxx stores some secrets in the scrape configs, it's necessary to provide the config via K8s secret. This helm chart only templates an object which is then in the pipeline converted to K8s secret and applied:
  
```bash
helm template --values azure_subscriptions.yaml --values linux_service_blacklist.yaml --values windows_service_blacklist.yaml --values function_relabeling.yaml --values hetzner_federation.yaml --values other_scrapes.yaml --values azure_secure_scrapes.yaml --set azure_sp_secret=$(SP-AZURE-READ-ONLY) . | tail -n +4 > prometheus-additional.yaml

kubectl create secret generic additional-scrape-configs --from-file=prometheus-additional.yaml --dry-run=client -oyaml > additional-scrape-configs.yaml

kubectl apply -f additional-scrape-configs.yaml -n kube-prometheus-stack
```

## Alerty (rules)

```bash


```

## ToDo
- service monitor
- blackbox exporter monitor endpointov aplikacii

```
helm install prometheus   prometheus-community/kube-prometheus-stack -n prometheus -f env/playground/prometheus-values.yaml

helm upgrade prometheus   prometheus-community/kube-prometheus-stack -n prometheus -f env/playground/prometheus-values.yaml


helm delete prometheus -n prometheus
```
# ----------------------------------------------------------------
```

helm template --values test-scrapes.yaml  . | tail -n +4 > prometheus-additional.yaml

kubectl apply secret generic additional-scrape-configs --from-file=prometheus-additional.yaml --dry-run=client -oyaml > additional-scrape-configs.yaml


kubectl apply -f additional-scrape-configs.yaml -n prometheus

kubectl create -f additional-scrape-configs.yaml -n prometheus


cd ./btcs-prometheus-scrape-configs
helm template --values azure_subscriptions.yaml --values linux_service_blacklist.yaml --values windows_service_blacklist.yaml --values function_relabeling.yaml --values hetzner_federation.yaml --values other_scrapes.yaml --values azure_secure_scrapes.yaml --set azure_sp_secret=$(SP-AZURE-READ-ONLY) . | tail -n +4 > prometheus-additional.yaml
kubectl create secret generic additional-scrape-configs --from-file=prometheus-additional.yaml --dry-run=client -oyaml > additional-scrape-configs.yaml
kubectl apply -f additional-scrape-configs.yaml -n kube-prometheus-stack
```

----
# Service monitor

```yaml
# Service targeting gitlab instances
apiVersion: v1
kind: Service
metadata:
  name: gitlab-metrics
  labels:
    app: gitlab-runner-gitlab-runner
spec:
  ports:
  - name: metrics # expose metrics port
    port: 9252 # defined in gitlab chart
    targetPort: metrics
    protocol: TCP
  selector:
    app: gitlab-runner-gitlab-runner # target gitlab pods
---
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: gitlab-metrics-servicemonitor
  # Change this to the namespace the Prometheus instance is running in
  # namespace: default
  labels:
    app: gitlab-runner-gitlab-runner
    release: prometheus
spec:
  selector:
    matchLabels:
      app: gitlab-runner-gitlab-runner # target gitlab service
  endpoints:
  - port: metrics
    interval: 15s
```

# Notifikacia do Slack-u / discod-u



