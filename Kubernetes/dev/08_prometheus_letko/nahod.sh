#!/bin/bash

env=playground
# vytvor secret pre scrapovanie
# helm template -f ./env/$env/jobs/$env-scrapes.yaml  ./env/$env/jobs | tail -n +4 > prometheus-additional.yaml
# kubectl create secret generic additional-scrape-configs --from-file=prometheus-additional.yaml --dry-run=client -o yaml > ./tmp/additional-scrape-configs.yaml
# kubectl delete secret additional-scrape-configs -n prometheus

# kubectl apply -f ./tmp/additional-scrape-configs.yaml -n prometheus

# nahod rules
helm delete rules -n prometheus
sleep 1 
helm template  rules ./rules/ -n prometheus -f env/$env/rules-values.yaml

helm upgrade --install rules ./rules/ -n prometheus -f env/$env/rules-values.yaml
sleep 1

# exit


# kubectl create -f additional-scrape-configs.yaml -n prometheus
#
#
# helm delete prometheus -n prometheus


# aktualizuj prometheus
helm upgrade --install prometheus   prometheus-community/kube-prometheus-stack -n prometheus -f env/$env/prometheus-values.yaml

# clean up
rm -f ./tmp/*.yaml