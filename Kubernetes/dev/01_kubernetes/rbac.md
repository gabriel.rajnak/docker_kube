# RBAC cvicenie
[https://www.youtube.com/watch?v=jvhKOAyD8S8]
github [https://github.com/marcel-dempers/docker-development-youtube-series]


```kind create cluster --name rbac --image kindest/node:v1.20.2```

Certifikaty kubernetesu
```bash
docker exec -it rbac-control-plane bash
ls -l /etc/kubernetes/pki
```

Skopirujem si ich ku sebe
```bash
docker cp rbac-control-plane:/etc/kubernetes/pki/ca.crt ca.crt
docker cp rbac-control-plane:/etc/kubernetes/pki/ca.key ca.key

sudo chown gabo:gabo *
```

Spravim si vlastny certifikat
```bash
docker run -it -v ${PWD}:/work -w /work -v ${HOME}:/root/ --net host alpine sh

# dinstaluj ssl
apk add openssl

#start with a private key
openssl genrsa -out bob.key 2048

# vygeneruj certifikat s profilom pre usera BOB aplikacia Shopping
openssl req -new -key bob.key -out bob.csr -subj "/CN=Bob Smith/O=Shopping"

# podpis vygenerovany certifika pomocou certifikatu kubernetes
openssl x509 -req -in bob.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out bob.crt -days 1

Certificate request self-signature ok
subject=CN = Bob Smith, O = Shopping
```
### vyskoc z dockeru
```bash
export KUBECONFIG=~/.kube/new-config

# vytvor config zanam pre kubernetes  ~/.kube/config
kubectl config set-cluster dev-cluster --server=https://127.0.0.1:52807 \
        --certificate-authority=ca.crt \
        --embed-certs=true


# zvol si profil BOB
kubectl config set-credentials bob --client-certificate=bob.crt --client-key=bob.key --embed-certs=true

# context dev-cluster
kubectl config set-context dev --cluster=dev-cluster --namespace=shopping --user=bob

# prepni sa na dev profil
kubectl config use-context dev

# vyskusaj pristup 9zatial nemam pristup)
kubectl get pods 
```
# RoleBinding

```bash

kubectl create ns shopping

kubectl -n shopping apply -f role.yaml
kubectl -n shopping apply -f rolebinding.yaml

```

### Service account
```bash
kubectl -n shopping apply -f serviceaccount.yaml


# bob moze nahodit pod
kubectl -n shopping apply -f pod.yaml

kubectl -n shopping exec -it shopping-api -- bash

# Point to the internal API server hostname
APISERVER=https://kubernetes.default.svc

# Path to ServiceAccount token
SERVICEACCOUNT=/var/run/secrets/kubernetes.io/serviceaccount

# Read this Pod's namespace
NAMESPACE=$(cat ${SERVICEACCOUNT}/namespace)

# Read the ServiceAccount bearer token
TOKEN=$(cat ${SERVICEACCOUNT}/token)

# Reference the internal certificate authority (CA)
CACERT=${SERVICEACCOUNT}/ca.crt

# List pods through the API
curl --cacert ${CACERT} --header "Authorization: Bearer $TOKEN" -s ${APISERVER}/api/v1/namespaces/shopping/pods/ 

# dostaneme error, este nie je nahodeny service account


# admin context, nahodil binding
kubectl -n shopping apply -f serviceaccount-role.yaml
kubectl -n shopping apply -f serviceaccount-rolebinding.yaml

# bbo profil, bude fungovat
curl --cacert ${CACERT} --header "Authorization: Bearer $TOKEN" -s ${APISERVER}/api/v1/namespaces/shopping/pods/ 


```

```bash
kubectl config get-contexts
CURRENT   NAME        CLUSTER       AUTHINFO    NAMESPACE
*         dev         dev-cluster   bob         shopping
          kind-rbac   kind-rbac     kind-rbac


```


## Uprac
```bash
 kind get clusters
rbac
gabo@server:~$ kind delete cluster --name rbac
Deleting cluster "rbac" ...
Deleted nodes: ["rbac-control-plane"]
```