[CNCF Kubernetes and Cloud Native Associate Certification Course](https://www.youtube.com/watch?v=AplluksKvzI&t=5151s)
# Selector

- sposob vyberu objektu
- napr. 
    - Label selector (key:value)
    - Field selector
    - Node selector



# Recommeded Labels
- odporucane labels, 
- ak pouzivaju prefix tak maju globalny charakter

# Anotations
- doplnkove informacie pre modul
- napr. ingress


# PodSpecFile
- specifikacny subor pre POD
- obsahuje definiciu podu(img, port,...)


# gRPC
- RPC remote procedure call
- gRPC (google) alternativa ku REST a GraphQL, aj cez datacentra

# kubectl

```bash
kubectl command name flag
```

