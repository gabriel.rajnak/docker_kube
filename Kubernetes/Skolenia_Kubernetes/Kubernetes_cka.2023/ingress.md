# Ingress

- ako reverse proxy
- namedPort - pomenovany port namiesto cisla(name je v service)


---
# LAB

```kubectl get deployment web-auth -o yaml```

```
 apiVersion: v1
 kind: Service
 metadata:
   name: web-auth-svc
 spec:
   type: ClusterIP
   selector:
     app: web-auth
   ports:
     - name: http
       protocol: TCP
       port: 80
       targetPort: 80
```

```kubectl apply -f svc.yaml ```

```
cloud_user@k8s-control:~$ kubectl get svc
NAME           TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)   AGE
kubernetes     ClusterIP   10.96.0.1      <none>        443/TCP   86m
web-auth-svc   ClusterIP   10.96.21.113   <none>        80/TCP    20s
```


inress.yaml
```
 apiVersion: networking.k8s.io/v1
 kind: Ingress
 metadata:
   name: web-auth-ingress
 spec:
   rules:
   - http:
       paths:
       - path: /auth
         pathType: Prefix
         backend:
           service:
             name: web-auth-svc
             port:
               number: 80
```
```kubectl apply -f ingress.yaml ```

```
cloud_user@k8s-control:~$ kubectl describe ingress web-auth-ingress
Name:             web-auth-ingress
Labels:           <none>
Namespace:        default
Address:          
Ingress Class:    <none>
Default backend:  <default>
Rules:
  Host        Path  Backends
  ----        ----  --------
  *           
              /auth   web-auth-svc:80 (192.168.194.66:80,192.168.194.67:80)
Annotations:  <none>
Events:       <none>
```














