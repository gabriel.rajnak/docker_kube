# Services

## Service LAB

### Prieskum
```
cloud_user@k8s-control:~$ kubectl get deploy
NAME           READY   UP-TO-DATE   AVAILABLE   AGE
user-db        2/2     2            2           29m
web-frontend   2/2     2            2           29m
cloud_user@k8s-control:~$ kubectl get dvc
error: the server doesn't have a resource type "dvc"
cloud_user@k8s-control:~$ kubectl get svc
NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   30m
```
### podrobne info o deploymente
- vsimat si selector -> app:user-db
- vimat si port deploymentu
```kubectl get deployment user-db -o yaml```


```
apiVersion: v1 
kind: Service 
metadata: 
  name: user-db-svc 
spec: 
  type: ClusterIP 
  selector: 
    app: user-db 
  ports: 
  - protocol: TCP 
    port: 80 
    targetPort: 80
```

### Vytvorim service
```
cloud_user@k8s-control:~$ kubectl apply -f service-db.yaml 
service/user-db-svc created
```
### Overim dostupnost sluzby
```kubectl exec busybox -- curl user-db-svc```

---
# Public service

```
apiVersion: v1 
kind: Service 
metadata: 
  name: web-frontend-svc 
spec: 
  type: NodePort 
  selector: 
    app: web-frontend 
  ports: 
  - protocol: TCP 
    port: 80 
    targetPort: 80 
    nodePort: 30080
```

```kubectl apply -f service-pub.yaml```

## pozriem vypublikovane servisy
```
cloud_user@k8s-control:~$ kubectl get svc
NAME               TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
kubernetes         ClusterIP   10.96.0.1        <none>        443/TCP        36m
user-db-svc        ClusterIP   10.98.234.204    <none>        80/TCP         2m54s
web-frontend-svc   NodePort    10.102.142.201   <none>        80:30080/TCP   39s
```

## Sluzba bezi na verejnej adrese cotrol plane Kubernetesu 

```http://44.211.49.166:30080```


Done.

---
# Service DNS names

## v ramci namespace
- staci kratky nazov skuzby(user-db-svc)


## cross namespaces
- treba FQDN pre cely kubernetes cluster( web-frontend.web.svc.cluster.local  )


---
## LAB
### Co to mame ?
```
cloud_user@k8s-control:~$ kubectl get ns
NAME              STATUS   AGE
data              Active   2m21s
default           Active   2m59s
kube-node-lease   Active   3m
kube-public       Active   3m
kube-system       Active   3m
web               Active   2m21s
```

```
kubectl exec -n web busybox -- nslookup web-frontend
Redirect the output to save the results in a text file by using:

kubectl exec -n web busybox -- nslookup web-frontend >> ~/dns_same_namespace_results.txt
Look up the same Service using the fully qualified domain name by entering:

kubectl exec -n web busybox -- nslookup web-frontend.web.svc.cluster.local
Redirect the output to save the results of the second nslookup in a text file by using:

kubectl exec -n web busybox -- nslookup web-frontend.web.svc.cluster.local >> ~/dns_same_namespace_results.txt
Check that everything looks okay in the text file by using:

cat ~/dns_same_namespace_results.txt
```

### Vystup
```
cloud_user@k8s-control:~$ cat ~/dns_same_namespace_results.txt
Server:    10.96.0.10
Address 1: 10.96.0.10 kube-dns.kube-system.svc.cluster.local

Name:      web-frontend
Address 1: 10.107.62.108 web-frontend.web.svc.cluster.local
Server:    10.96.0.10
Address 1: 10.96.0.10 kube-dns.kube-system.svc.cluster.local

Name:      web-frontend.web.svc.cluster.local
Address 1: 10.107.62.108 web-frontend.web.svc.cluster.local
```



