# Schedulink

proces pridelovania podov na nody.

- poziadavky podu (requests)
- moznosti nodu
- labels

- nodesSelector(filter) - moze pridelit pod len na nodu, ktory ma label.
- nodeName - pridelenie podu konkretnemu nodu.
## NodeSelector

```kubectl label nodes node_name special=true     <-- nastavim label```

pod.yaml
```
spec:
    nodeSelector:
      special: "true"
    containers:
    ...

```

## NodeName

pod.yaml
```
spec:
    nodeName:  node_name         <------
    containers:
    ...
```

