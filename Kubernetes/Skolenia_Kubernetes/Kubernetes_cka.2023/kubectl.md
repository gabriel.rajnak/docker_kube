# kubectl

## kubectl get object

## kubectl describe object

## kubectl create -f file_name

## kubectl apply -f file_name

## kubectl exec podname -c container_name  -- command

### Objekty
- nodes  no
- pods   po
- deployment 
- service   svc
- replicaset

```
cloud_user@k8s-control:~$ kubectl get all -o wide
NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE   SELECTOR
service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   12m   <none>
cloud_user@k8s-control:~$ 
```
### Vylistuj persitent volumes utriedene podla velkosti
```
kubectl get pv --sort-by=.spec.capacity.storage
NAME         CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM   STORAGECLASS   REASON   AGE
pv0002       1Gi        RWO            Retain           Available           manual                  15m
audit-logs   2Gi        RWO            Retain           Available           manual                  15m
pv0003       3Gi        RWO            Retain           Available           manual                  15m
```

### Run command v kontaineri vypis subor
```
kubectl exec quark -n beebox-mobile -- cat /etc/key/key.txt
1267aa45
```

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  namespace: beebox-mobile
  labels:
    app: beebox-auth
spec:
  replicas: 3
  selector:
    matchLabels:
      app: beebox-auth
  template:
    metadata:
      labels:
        app: beebox-auth
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80
``` 

```
cloud_user@k8s-control:~$ kubectl apply -f deployment.yml 
deployment.apps/nginx-deployment created

cloud_user@k8s-control:~$ kubectl get deployments -n beebox-mobile
NAME               READY   UP-TO-DATE   AVAILABLE   AGE
nginx-deployment   3/3     3            3           29s
```

### Delete service
```
cloud_user@k8s-control:~$ kubectl get svc -n beebox-mobile
NAME              TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
beebox-auth-svc   ClusterIP   10.100.151.114   <none>        80/TCP    19m

cloud_user@k8s-control:~$ kubectl delete service  beebox-auth-svc -n beebox-mobile
service "beebox-auth-svc" deleted
```
