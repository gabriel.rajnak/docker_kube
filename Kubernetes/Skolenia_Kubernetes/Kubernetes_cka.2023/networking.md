# Netwoking

## Network model

- v ramci kotaineru(localhost)
- v ramci podu (cros kontainery)
- cros pody (service)

- nepouzivaju sa IP adresy, lebo su docasne , v principe nezname


## CNI plugin



```
apiVersion: v1
kind: Pod
metadata:
    name: busy-pod
spec:
    containers:
    - name: busybox
      image: radial/busyboxplus:curl
      command: ['sh', '-c', 'sleep 3600']      
---
apiVersion: v1
kind: Pod
metadata:
    name: nginx-pod
spec:
    containers:
    - name: nginx
      image: nginx:1.19.2
      ports:
      - containerPort: 80 
```

```
k exec busy-pod -- curl http://10.42.1.33

k exec busy-pod -- nslookup 10-42-1-33.default.pod.cluster.local
Server:    10.43.0.10
Address 1: 10.43.0.10 kube-dns.kube-system.svc.cluster.local

Name:      10-42-1-33.default.pod.cluster.local
Address 1: 10.42.1.33
```

# LAB fix network issue

cloud_user@k8s-control:~$ kubectl get pods
NAME                 READY   STATUS    RESTARTS   AGE
cyberdyne-frontend   0/1     Pending   0          28m
testclient           0/1     Pending   0          28m


cloud_user@k8s-control:~$ kubectl get NODES
NAME          STATUS     ROLES           AGE   VERSION
k8s-control   NotReady   control-plane   29m   v1.24.0
k8s-worker1   NotReady   <none>          28m   v1.24.0


Chyba network 
```network not ready: NetworkReady=false reason:NetworkPluginNotReady message:Network plugin returns error: cni plugin not initialized```

```
cloud_user@k8s-control:~$ kubectl describe node k8s-worker1
Name:               k8s-worker1
Roles:              <none>
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/os=linux
                    kubernetes.io/arch=amd64
                    kubernetes.io/hostname=k8s-worker1
                    kubernetes.io/os=linux
Annotations:        kubeadm.alpha.kubernetes.io/cri-socket: unix:///var/run/containerd/containerd.sock
                    node.alpha.kubernetes.io/ttl: 0
                    volumes.kubernetes.io/controller-managed-attach-detach: true
CreationTimestamp:  Mon, 12 Sep 2022 17:47:23 +0000
Taints:             node.kubernetes.io/not-ready:NoSchedule
Unschedulable:      false
Lease:
  HolderIdentity:  k8s-worker1
  AcquireTime:     <unset>
  RenewTime:       Mon, 12 Sep 2022 18:16:41 +0000
Conditions:
  Type             Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message
  ----             ------  -----------------                 ------------------                ------                       -------
  MemoryPressure   False   Mon, 12 Sep 2022 18:13:13 +0000   Mon, 12 Sep 2022 17:47:23 +0000   KubeletHasSufficientMemory   kubelet has sufficient memory available
  DiskPressure     False   Mon, 12 Sep 2022 18:13:13 +0000   Mon, 12 Sep 2022 17:47:23 +0000   KubeletHasNoDiskPressure     kubelet has no disk pressure
  PIDPressure      False   Mon, 12 Sep 2022 18:13:13 +0000   Mon, 12 Sep 2022 17:47:23 +0000   KubeletHasSufficientPID      kubelet has sufficient PID available
  Ready            False   Mon, 12 Sep 2022 18:13:13 +0000   Mon, 12 Sep 2022 17:47:23 +0000   KubeletNotReady              container runtime network not ready: NetworkReady=false reason:NetworkPluginNotReady message:Network plugin returns error: cni plugin not initialized
Addresses:
  InternalIP:  10.0.1.102
  Hostname:    k8s-worker1
Capacity:
  cpu:                2
  ephemeral-storage:  8065444Ki
  hugepages-1Gi:      0
  hugepages-2Mi:      0
  memory:             3958700Ki
  pods:               110
Allocatable:
  cpu:                2
  ephemeral-storage:  7433113179
  hugepages-1Gi:      0
  hugepages-2Mi:      0
  memory:             3856300Ki
  pods:               110
System Info:
  Machine ID:                 66a578fcdb9d4303a2948a46329a0e3f
  System UUID:                ec240a1e-e487-8b41-013a-d3b730cfa170
  Boot ID:                    635f91d3-436d-428c-9ca4-3ececc74f119
  Kernel Version:             5.13.0-1025-aws
  OS Image:                   Ubuntu 20.04.4 LTS
  Operating System:           linux
  Architecture:               amd64
  Container Runtime Version:  containerd://1.5.9
  Kubelet Version:            v1.24.0
  Kube-Proxy Version:         v1.24.0
PodCIDR:                      192.168.1.0/24
PodCIDRs:                     192.168.1.0/24
Non-terminated Pods:          (1 in total)
  Namespace                   Name                CPU Requests  CPU Limits  Memory Requests  Memory Limits  Age
  ---------                   ----                ------------  ----------  ---------------  -------------  ---
  kube-system                 kube-proxy-dgcmp    0 (0%)        0 (0%)      0 (0%)           0 (0%)         29m
Allocated resources:
  (Total limits may be over 100 percent, i.e., overcommitted.)
  Resource           Requests  Limits
  --------           --------  ------
  cpu                0 (0%)    0 (0%)
  memory             0 (0%)    0 (0%)
  ephemeral-storage  0 (0%)    0 (0%)
  hugepages-1Gi      0 (0%)    0 (0%)
  hugepages-2Mi      0 (0%)    0 (0%)
Events:
  Type    Reason                   Age                From             Message
  ----    ------                   ----               ----             -------
  Normal  Starting                 29m                kube-proxy       
  Normal  NodeHasSufficientMemory  29m (x8 over 29m)  kubelet          Node k8s-worker1 status is now: NodeHasSufficientMemory
  Normal  RegisteredNode           29m                node-controller  Node k8s-worker1 event: Registered Node k8s-worker1 in Controller
  ```

### Nebezi coredns
```
cloud_user@k8s-control:~$ kubectl get pods -n kube-system
NAME                                  READY   STATUS    RESTARTS   AGE
coredns-6d4b75cb6d-bfpt6              0/1     Pending   0          32m
coredns-6d4b75cb6d-wfmfw              0/1     Pending   0          32m
etcd-k8s-control                      1/1     Running   0          32m
kube-apiserver-k8s-control            1/1     Running   0          32m
kube-controller-manager-k8s-control   1/1     Running   0          32m
kube-proxy-dgcmp                      1/1     Running   0          32m
kube-proxy-qz56s                      1/1     Running   0          32m
kube-scheduler-k8s-control            1/1     Running   0          32m
```

### Doinstalujem calico plugin 
```kubectl apply -f https://docs.projectcalico.org/v3.15/manifests/calico.yaml```


```
cloud_user@k8s-control:~$ kubectl get pods -o wide
NAME                 READY   STATUS    RESTARTS   AGE   IP               NODE          NOMINATED NODE   READINESS GATES
cyberdyne-frontend   1/1     Running   0          36m   192.168.194.67   k8s-worker1   <none>           <none>
testclient           1/1     Running   0          36m   192.168.194.65   k8s-worker1   <none>           <none>
cloud_user@k8s-control:~$ 
```

```kubectl exec testclient -- curl 192.168.194.67```

---
# Network Policies
- defaultne je Pod otvoreny, ak sa pouzije akykolvek selektor pouzije, tak uz nie.
- pod selector - sluzi na vyper podu
- ingress -- prichadzajuci traffic
- egress -- odchod. 

## from a to selector
- podSelector
- namespaceSelector
- ipBlock

```
spec:
  ingress:
    - from:
      - podSelector:
        matchLabels:
          app: db
```

## Ports
- definuje otvorene porty budu otvorene

```
spec:
  ingress:
    - from:
      ports:
        - protocol: TCP
          port: 80
```


