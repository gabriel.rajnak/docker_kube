# Container heath (probes)

- sledovanie stavu kontainera

- startup (kontrola startu)
- liveness (prevadzka)
- readiness (kontainer je pripraveny na prevadzku)


```
apiVersion: v1
kind: Pod
metadata:
    name: liveness-pod
spec:
    containers:
    - name: busybox
      image: busybox
      command: ['sh', '-c', 'while true; do sleep 3600; done']
      livenessProbe:
        exec:
          command: ["echo","Hello wold"]
        initialDelaySeconds: 5
        periodSeconds: 5

```      

```
apiVersion: v1
kind: Pod
metadata:
    name: liveness-pod-http
spec:
    containers:
    - name: nginx
      image: nginx:1.19.1      
      livenessProbe:
        httpGet:
           path: /
           port: 80        
        initialDelaySeconds: 5
        periodSeconds: 5

```   

```
apiVersion: v1
kind: Pod
metadata:
    name: startup-pod
spec:
    containers:
    - name: nginx
      image: nginx:1.19.1      
      startupProbe:
        httpGet:
           path: /
           port: 80        
        failureThreshold: 30
        periodSeconds: 10
```   


# rediness probe

```
apiVersion: v1
kind: Pod
metadata:
    name: readiness-pod
spec:
    containers:
    - name: nginx
      image: nginx:1.19.1      
      readinessProbe:
        httpGet:
           path: /
           port: 80        
        initialDelaySeconds: 5
        periodSeconds: 5
```  