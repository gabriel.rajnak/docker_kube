
00:52:49
Exit Lab


Complete Lab
Building Self-Healing Containers in Kubernetes
1 hour duration
Practitioner
VIDEOS
GUIDE
Building Self-Healing Containers in Kubernetes
Introduction
Kubernetes offers several features that can be used together to create self-healing applications in a variety of scenarios. In this lab, you will be able to practice your skills at using features such as probes and restart policies to create a container application that is automatically healed when it stops working.

Solution
Log in to the provided lab server using the credentials provided:

ssh cloud_user@<PUBLIC_IP_ADDRESS>
Set a Restart Policy to Restart the Container When It Is Down

Find the pod that needs to be modified:

```kubectl get pods -o wide```
Take note of the beebox-shipping-data pod's IP address.

Use the busybox pod to make a request to the pod to see if it is working:

kubectl exec busybox -- curl 192.168.194.66:8080
We will likely get an error message.

Get the pod's YAML descriptor:

  kubectl get pod beebox-shipping-data -o yaml > beebox-shipping-data.yml
Open the file:

vi beebox-shipping-data.yml
Set the restartPolicy to Always:

spec:
  ...
  restartPolicy: Always
  ...
Create a Liveness Probe to Detect When the Application Has Crashed

Add a liveness probe:

spec:
  containers:
  - ...
    name: shipping-data
    livenessProbe:
      httpGet:
        path: /
        port: 8080
      initialDelaySeconds: 5
      periodSeconds: 5
    ...
Save and exit the file by pressing Escape followed by :wq.

Delete the pod:

kubectl delete pod beebox-shipping-data
Re-create the pod to apply the changes:

kubectl apply -f beebox-shipping-data.yml
Check the pod status

kubectl get pods -o wide
If you wait a minute or so and check again, you should see the pod is being restarted whenever the application crashes.

Check the http response from the pod again (it will have a new IP address since we re-created it):

kubectl exec busybox -- curl 192.168.194.70:8080
If you wish, you can explore and see what happens as the application crashes and the pod is restarted automatically.

Conclusion
Congratulations on successfully completing this hands-on lab!

Tools

Lab Diagram

Instant Terminal
Credentials

How do I connect?
Cloud Server Control Plane Node

Username


Password


Control Plane Node Private IP


Control Plane Node Public IP


Launch Instant Terminal

How do I connect?
Additional Resources
You are working for BeeBox, a company that provides regular shipments of bees to customers. The company is in the process of deploying some applications to Kubernetes that handle data processing related to shipping.

One of these application components is a pod called beebox-shipping-data located in the default namespace. Unfortunately, the application running in this pod has been crashing repeatedly. While the developers are looking into why this application is crashing, you have been asked to implement a self-healing solution in Kubernetes to quickly recover whenever the application crashes.

Luckily, the application can be fixed when it crashes simply by restarting the container. Modify the pod configuration so the application will automatically restart when it crashes. You can detect an application crash when requests to port 8080 on the container return an HTTP 500 status code.

Note: The kubeclt --export no longer works in the lab environment. An alternative would be:

kubectl get pod beebox-shipping-data -o yaml > beebox-shipping-data.yml
Learning Objectives
0 of 2 completed

Set a Restart Policy to Restart the Container When It Is Down


Create a Liveness Probe to Detect When the Application Has Crashed

Certified Kubernetes Administrator (CKA) course artwork
Building Self-Healing Containers in Kubernetes - A Cloud Guru



```
apiVersion: v1
kind: Pod
metadata:
  annotations:
    cni.projectcalico.org/containerID: 7e173d0de112f1a35c0b558fb2c1509277f6f70a05f5128e8baec15db65fc995
    cni.projectcalico.org/podIP: 192.168.194.66/32
    cni.projectcalico.org/podIPs: 192.168.194.66/32
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","kind":"Pod","metadata":{"annotations":{},"name":"beebox-shipping-data","namespace":"default"},"spec":{"containers":[{"image":"linuxacademycontent/random-crashing-web-server:1","name":"shipping-data"}],"restartPolicy":"Never"}}
  creationTimestamp: "2022-09-11T07:38:12Z"
  name: beebox-shipping-data
  namespace: default
  resourceVersion: "704"
  uid: b0c1b438-2b9e-4c1c-acc4-6f5cde513f44
spec:
  containers:
  - image: linuxacademycontent/random-crashing-web-server:1
    imagePullPolicy: IfNotPresent
    name: shipping-data
    livenessProbe:
            httpGet:
                path: /
                port: 8080
            initialDelaySeconds: 5
            periodSeconds: 5
    resources: {}
    terminationMessagePath: /dev/termination-log
    terminationMessagePolicy: File
    volumeMounts:
    - mountPath: /var/run/secrets/kubernetes.io/serviceaccount
      name: kube-api-access-xmz6s
      readOnly: true
  dnsPolicy: ClusterFirst
  enableServiceLinks: true
  nodeName: k8s-worker1
  preemptionPolicy: PreemptLowerPriority
  priority: 0
  restartPolicy: Always
  schedulerName: default-scheduler
  securityContext: {}
  serviceAccount: default
  serviceAccountName: default
  terminationGracePeriodSeconds: 30
  tolerations:
  - effect: NoExecute
    key: node.kubernetes.io/not-ready
    operator: Exists
    tolerationSeconds: 300
  - effect: NoExecute
    key: node.kubernetes.io/unreachable
    operator: Exists
    tolerationSeconds: 300
  volumes:
  - name: kube-api-access-xmz6s
    projected:
      defaultMode: 420
      sources:
      - serviceAccountToken:
          expirationSeconds: 3607
          path: token
      - configMap:
          items:
          - key: ca.crt
            path: ca.crt
          name: kube-root-ca.crt
      - downwardAPI:
          items:
          - fieldRef:
              apiVersion: v1
              fieldPath: metadata.namespace
            path: namespace
status:
  conditions:
  - lastProbeTime: null
    lastTransitionTime: "2022-09-11T07:38:30Z"
    status: "True"
    type: Initialized
  - lastProbeTime: null
    lastTransitionTime: "2022-09-11T07:39:12Z"
    status: "True"
    type: Ready
  - lastProbeTime: null
    lastTransitionTime: "2022-09-11T07:39:12Z"
    status: "True"
    type: ContainersReady
  - lastProbeTime: null
    lastTransitionTime: "2022-09-11T07:38:30Z"
    status: "True"
    type: PodScheduled
  containerStatuses:
  - containerID: containerd://6ddfb5489ebc334badfdb48509d6e602313d7af71c65c465b49a4dd40c0584c3
    image: docker.io/linuxacademycontent/random-crashing-web-server:1
    imageID: docker.io/linuxacademycontent/random-crashing-web-server@sha256:961fb2f1241af9bd1de39d62aaac28a55fa0ec215b86a0c1f67c49a13cbcd0e2
    lastState: {}
    name: shipping-data
    ready: true
    restartCount: 0
    started: true
    state:
      running:
        startedAt: "2022-09-11T07:39:10Z"
  hostIP: 10.0.1.102
  phase: Running
  podIP: 192.168.194.66
  podIPs:
  - ip: 192.168.194.66
  qosClass: BestEffort
  startTime: "2022-09-11T07:38:30Z"
  ```
  