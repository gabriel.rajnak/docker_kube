# Multi container pod


```
apiVersion: v1
kind: Pod
metadata:
    name: multi-pod
spec:
    restartPolicy: Always
    containers:
    - name: busybox
      image: busybox
      command: ['sh', '-c', 'sleep 10']      
    - name: nginx
      image: nginx
    - name: redis
      image: redis
```   
