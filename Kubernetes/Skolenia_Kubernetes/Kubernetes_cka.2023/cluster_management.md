# Management

## High avaiability

- multiple control-plane modes
- etcd, external etcd


### Drain
- bezpecne odobratie nodu 
- beziace kontainery budu korektne ukoncene a startnute inde bez prerusenia sluzby

```kubectl drain node_name --ignore-daemonsets --force```

vrat spat node 
```kubectl uncordon node_name --ignore-daemonsets```

## Upgrade k8s kubeadm

1. upgrade kubeadm on control-plane

```sudo apt-get install -y --allow-change-held-packages kubeadm-1.27.2-00```

2. kubeadm upgrade plan
```bash
kubeadm upgrade plan v1.27.2
```
3. kubeadm upgrade apply
```kubeadm upgrade apply v1.27.2```
4. upgrade kubelet, kubectl
```apt-get update kubectl ......
sudo restart daemon...
restart kubelet
```

5. uncordom control-node
```
kubectl uncordon k8s-control
```

6. drain worker nodes
7. upgrade kubeadm
8. kubeadm upgrade node
9. upgrade kubectl











