# Certified Kubernetes Administrator (CKA)

[CKA](https://learn.acloud.guru/course/certified-kubernetes-administrator/learn/fddcffaa-9fe2-4591-8715-64d4e99e9481/b1218bd0-24ab-47ff-8b97-7c7bb46f963e/watch)



### Control plane
- bezi na dedikovanom stroji
- kolekcia kompomentov zodpovednych za riadenie klustra
    - kube-api-server
    - etcd - databaza
    - kube-scheduler (veber volneho nodu pre start)
    - kube-control-manager - 

### Nodes
- tu bezia kontajnery
- kubelet bezi na kazdom node, reportuje control-plane

### Container runtime
- kontajnerizacny progrma docer alebo containerd

### Kube-proxy
- interny network kubernetes

---
## Kubeadm
- 

Ubuntu 20.04
```
multipass launch -c 2 -n plane -m 2G 22.04
multipass launch -c 2 -n node1 -m 2G 22.04
multipass launch -c 2 -n node2 -m 2G 22.04
```

/etc/hosts
```
192.168.64.14 node1
192.168.64.15 node2
192.168.64.16 plane
```





