# Storage
- volume(lokalne na workernode)
- persistent volume
- persistent volume claim

## Volume types
- NFS
- cloud storage(aws, google, azure)
- configmap, secret
- simple directory on Node(hostPath)

---
## Specialne adresare
- hostPath - finy adresat na workernode
- emptyDir - lokalne dynamicky vytvoreny adresar na node, moze byt zdielany v raamci podu medzi kontainermi


```
apiVersion: v1
kind: Pod
metadata:
    name: maintenance-pod
spec:
    containers:
    - name: busybox
      image: busybox
      command: ['sh', '-c', 'while true; do echo Success! >> /output/output.txt; sleep 5; done']
      volumeMounts:
       - name: output-vol
         mountPath: /output
    volumes:
         - name: output-vol
           hostPath:
             path: /var/data
```      

## shared volumes
```
apiVersion: v1
kind: Pod
metadata:
    name: shared-data-pod
spec:
    containers:
    - name: busybox1
      image: busybox
      command: ['sh', '-c', 'while true; do echo Success! >> /output/output.txt; sleep 5; done']
      volumeMounts:
         - name: shared-vol
           mountPath: /output
    - name: busybox2
      image: busybox
      command: ['sh', '-c', 'while true; do cat /input/output.txt; sleep 5; done']
      volumeMounts:
      - name: shared-vol
        mountPath: /input
    volumes:
    - name: shared-vol
      emptyDir: {}
```

### Pozri logy na druhom busyboxe
```kubectl logs shared-data-pod -c busybox2```


---
# Persistent volume
- abstrakcna vrstva pre volume


# Persistent volume claim
- fyzicka reprezentacia volume
- potrebuje urcit storageclass

---

# LAB

- storage class
```
apiVersion: storage.k8s.io/v1 
kind: StorageClass 
metadata: 
  name: localdisk 
provisioner: kubernetes.io/no-provisioner
allowVolumeExpansion: true
```
- pv
```
kind: PersistentVolume 
apiVersion: v1 
metadata: 
   name: host-pv 
spec: 
   storageClassName: localdisk
   persistentVolumeReclaimPolicy: Recycle 
   capacity: 
      storage: 1Gi 
   accessModes: 
      - ReadWriteOnce 
   hostPath: 
      path: /var/output
```

```
cloud_user@k8s-control:~$ kubectl get pv
NAME      CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM   STORAGECLASS   REASON   AGE
host-pv   1Gi        RWO            Recycle          Available           localdisk               52s
```

```
apiVersion: v1 
kind: PersistentVolumeClaim 
metadata: 
   name: host-pvc 
spec: 
   storageClassName: localdisk 
   accessModes: 
      - ReadWriteOnce 
   resources: 
      requests: 
         storage: 100Mi
```
```
cloud_user@k8s-control:~$ kubectl get pvc
NAME       STATUS   VOLUME    CAPACITY   ACCESS MODES   STORAGECLASS   AGE
host-pvc   Bound    host-pv   1Gi        RWO            localdisk      5s
```

```
apiVersion: v1 
kind: Pod 
metadata: 
   name: pv-pod 
spec: 
   containers: 
      - name: busybox 
        image: busybox 
        command: ['sh', '-c', 'while true; do echo Success! > /output/success.txt; sleep 5; done'] 
        volumeMounts: 
        - name: pv-storage 
          mountPath: /output 
   volumes: 
    - name: pv-storage 
      persistentVolumeClaim: 
       claimName: host-pvc
```

```
cloud_user@k8s-worker1:~$ cat /var/output/success.txt 
Success!
```







