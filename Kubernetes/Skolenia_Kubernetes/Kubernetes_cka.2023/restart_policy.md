# Selfhealing restart policy
Restartuje kontainer ak treba

- always - vzdy restaruje, aj ked je manualne stopnuta
- onFailure - ak spadne kontainer, ak je unheathy
- never  - nikdy

## Always
```
apiVersion: v1
kind: Pod
metadata:
    name: always-pod
spec:
    restartPolicy: Always
    containers:
    - name: busybox
      image: busybox
      command: ['sh', '-c', 'sleep 10']      
```   

## OnFailure
```
apiVersion: v1
kind: Pod
metadata:
    name: fail-pod
spec:
    restartPolicy: OnFailure
    containers:
    - name: busybox
      image: busybox
      command: ['sh', '-c', 'sleep 10; toto je chyba']      
```   

```
apiVersion: v1
kind: Pod
metadata:
    name: never-pod
spec:
    restartPolicy: Never
    containers:
    - name: busybox
      image: busybox
      command: ['sh', '-c', 'sleep 10']      
```   