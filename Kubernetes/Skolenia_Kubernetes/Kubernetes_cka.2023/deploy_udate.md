# Managing Kubernetes Applications with Deployments


```
cloud_user@k8s-control:~$ kubectl get deployments
NAME         READY   UP-TO-DATE   AVAILABLE   AGE
beebox-web   2/2     2            2           2m44s
```

kubectl edit deployment beebox-web

kubectl rollout status deployment.v1.apps/beebox-web

kubectl scale deployment.v1.apps/beebox-web --replicas=5
