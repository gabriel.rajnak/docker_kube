# Troubleshooting

```
cloud_user@k8s-control:~$ kubectl get nodes
NAME          STATUS     ROLES           AGE    VERSION
k8s-control   Ready      control-plane   106m   v1.24.0
k8s-worker1   Ready      <none>          106m   v1.24.0
k8s-worker2   NotReady   <none>          105m   v1.24.0
```

```kubectl get nodes```


## ssh na worker node2
```
sudo journalctl -u kubelet

cloud_user@k8s-worker2:~$ sudo systemctl start kubelet
cloud_user@k8s-worker2:~$ sudo systemctl enable kubelet
Created symlink /etc/systemd/system/multi-user.target.wants/kubelet.service → /lib/systemd/system/kubelet.service.
```

```
cloud_user@k8s-control:~$ kubectl get nodes
NAME          STATUS   ROLES           AGE    VERSION
k8s-control   Ready    control-plane   113m   v1.24.0
k8s-worker1   Ready    <none>          112m   v1.24.0
k8s-worker2   Ready    <none>          111m   v1.24.0
cloud_user@k8s-control:~$ 
```

---
# pristup na shell v pode
```kubectl describe pod pod_name```


```kubectl exec pod_name -- bash ```


```kubectl exec pod_name -c container_name -- bash ```

```kubectl exec pod_name --stdin -tty -- /bin/bash```

# Kontrola logov
```kubectl logs pod_name -c container_name```



---
# Apps
```
cloud_user@k8s-control:~$ kubectl get pods -n web
NAME                            READY   STATUS    RESTARTS   AGE
web-consumer-7fb695d79c-2qr5k   1/1     Running   0          96m
web-consumer-7fb695d79c-qbrqj   1/1     Running   0          96m
```

cloud_user@k8s-control:~$ kubectl get deployments -n web
NAME           READY   UP-TO-DATE   AVAILABLE   AGE
web-consumer   2/2     2            2           98m


cloud_user@k8s-control:~$ kubectl describe deployment web-consumer -n web
Name:                   web-consumer
Namespace:              web
CreationTimestamp:      Mon, 26 Sep 2022 12:35:04 +0000
Labels:                 <none>
Annotations:            deployment.kubernetes.io/revision: 1
Selector:               app=web-consumer
Replicas:               2 desired | 2 updated | 2 total | 2 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=web-consumer
  Containers:
   busybox:
    Image:      radial/busyboxplus:curl
    Port:       <none>
    Host Port:  <none>
    Command:
      sh
      -c
      while true; do curl auth-db; sleep 5; done
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   web-consumer-7fb695d79c (2/2 replicas created)
Events:          <none>





cloud_user@k8s-control:~$ kubectl describe pod web-consumer-7fb695d79c-2qr5k -n web
Name:         web-consumer-7fb695d79c-2qr5k
Namespace:    web
Priority:     0
Node:         k8s-worker1/10.0.1.102
Start Time:   Mon, 26 Sep 2022 12:35:32 +0000
Labels:       app=web-consumer
              pod-template-hash=7fb695d79c
Annotations:  cni.projectcalico.org/containerID: 68903aae33f649cc4c7bfa43c9ffb13cdad4077ba552724209f3607caf627db7
              cni.projectcalico.org/podIP: 192.168.194.66/32
              cni.projectcalico.org/podIPs: 192.168.194.66/32
Status:       Running
IP:           192.168.194.66
IPs:
  IP:           192.168.194.66
Controlled By:  ReplicaSet/web-consumer-7fb695d79c
Containers:
  busybox:
    Container ID:  containerd://35845f702e72a3a75ac04682ed8db6d3d49e9a1fb4a4b0549ad8b1edad536314
    Image:         radial/busyboxplus:curl
    Image ID:      sha256:4776f1f7d1f625c8c5173a969fdc9ae6b62655a2746aba989784bb2b7edbfe9b
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
      -c
      while true; do curl auth-db; sleep 5; done
    State:          Running
      Started:      Mon, 26 Sep 2022 12:35:48 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-h59t5 (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  kube-api-access-h59t5:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:                      <none>
cloud_user@k8s-control:~$ 


kubectl  logs web-consumer-7fb695d79c-2qr5k -n web
curl: (6) Couldn't resolve host 'auth-db'
curl: (6) Couldn't resolve host 'auth-db'
curl: (6) Couldn't resolve host 'auth-db'
curl: (6) Couldn't resolve host 'auth-db'
curl: (6) Couldn't resolve host 'auth-db'
curl: (6) Couldn't resolve host 'auth-db'
curl: (6) Couldn't resolve host 'auth-db'
curl: (6) Couldn't resolve host 'auth-db'
curl: (6) Couldn't resolve host 'auth-db'
curl: (6) Couldn't resolve host 'auth-db'


cloud_user@k8s-control:~$ kubectl get pod -n web web-consumer-7fb695d79c-2qr5k -o yaml
apiVersion: v1
kind: Pod
metadata:
  annotations:
    cni.projectcalico.org/containerID: 68903aae33f649cc4c7bfa43c9ffb13cdad4077ba552724209f3607caf627db7
    cni.projectcalico.org/podIP: 192.168.194.66/32
    cni.projectcalico.org/podIPs: 192.168.194.66/32
  creationTimestamp: "2022-09-26T12:35:04Z"
  generateName: web-consumer-7fb695d79c-
  labels:
    app: web-consumer
    pod-template-hash: 7fb695d79c
  name: web-consumer-7fb695d79c-2qr5k
  namespace: web
  ownerReferences:
  - apiVersion: apps/v1
    blockOwnerDeletion: true
    controller: true
    kind: ReplicaSet
    name: web-consumer-7fb695d79c
    uid: 5238f36c-605b-45c2-a482-b28856244a4d
  resourceVersion: "734"
  uid: 3a4a1cd0-bbc6-4fa4-8c39-205900160779
spec:
  containers:
  - command:
    - sh
    - -c
    - while true; do curl auth-db; sleep 5; done          <<=================================
    image: radial/busyboxplus:curl
    imagePullPolicy: IfNotPresent
    name: busybox
    resources: {}
    terminationMessagePath: /dev/termination-log
    terminationMessagePolicy: File
    volumeMounts:
    - mountPath: /var/run/secrets/kubernetes.io/serviceaccount
      name: kube-api-access-h59t5
      readOnly: true
  dnsPolicy: ClusterFirst
  enableServiceLinks: true
  nodeName: k8s-worker1
  preemptionPolicy: PreemptLowerPriority
  priority: 0
  restartPolicy: Always
  schedulerName: default-scheduler
  securityContext: {}
  serviceAccount: default
  serviceAccountName: default
  terminationGracePeriodSeconds: 30
  tolerations:
  - effect: NoExecute
    key: node.kubernetes.io/not-ready
    operator: Exists
    tolerationSeconds: 300
  - effect: NoExecute
    key: node.kubernetes.io/unreachable
    operator: Exists
    tolerationSeconds: 300
  volumes:
  - name: kube-api-access-h59t5
    projected:
      defaultMode: 420
      sources:
      - serviceAccountToken:
          expirationSeconds: 3607
          path: token
      - configMap:
          items:
          - key: ca.crt
            path: ca.crt
          name: kube-root-ca.crt
      - downwardAPI:
          items:
          - fieldRef:
              apiVersion: v1
              fieldPath: metadata.namespace
            path: namespace
status:
  conditions:
  - lastProbeTime: null
    lastTransitionTime: "2022-09-26T12:35:32Z"
    status: "True"
    type: Initialized
  - lastProbeTime: null
    lastTransitionTime: "2022-09-26T12:35:49Z"
    status: "True"
    type: Ready
  - lastProbeTime: null
    lastTransitionTime: "2022-09-26T12:35:49Z"
    status: "True"
    type: ContainersReady
  - lastProbeTime: null
    lastTransitionTime: "2022-09-26T12:35:32Z"
    status: "True"
    type: PodScheduled
  containerStatuses:
  - containerID: containerd://35845f702e72a3a75ac04682ed8db6d3d49e9a1fb4a4b0549ad8b1edad536314
    image: docker.io/radial/busyboxplus:curl
    imageID: sha256:4776f1f7d1f625c8c5173a969fdc9ae6b62655a2746aba989784bb2b7edbfe9b
    lastState: {}
    name: busybox
    ready: true
    restartCount: 0
    started: true
    state:
      running:
        startedAt: "2022-09-26T12:35:48Z"
  hostIP: 10.0.1.102
  phase: Running
  podIP: 192.168.194.66
  podIPs:
  - ip: 192.168.194.66
  qosClass: BestEffort
  startTime: "2022-09-26T12:35:32Z"



  cloud_user@k8s-control:~$ kubectl get svc -n web
No resources found in web namespace.
cloud_user@k8s-control:~$ kubectl get ns
NAME              STATUS   AGE
data              Active   105m
default           Active   106m
kube-node-lease   Active   106m
kube-public       Active   106m
kube-system       Active   106m
web               Active   105m


cloud_user@k8s-control:~$ kubectl get svc -n data
NAME      TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)   AGE
auth-db   ClusterIP   10.98.94.115   <none>        80/TCP    106m



edit : kubectl edit deployment -n web web-consumer

opravit DNS meno.

- while true; do curl auth-db.data.svc.cluster.local ; sleep 5; done



cloud_user@k8s-control:~$ kubectl get pod -n web  
NAME                            READY   STATUS        RESTARTS   AGE
web-consumer-6cdb5f5cb8-59x85   1/1     Running       0          25s
web-consumer-6cdb5f5cb8-s9ww4   1/1     Running       0          26s
web-consumer-7fb695d79c-2qr5k   1/1     Terminating   0          108m
web-consumer-7fb695d79c-qbrqj   1/1     Terminating   0          108m


cloud_user@k8s-control:~$ kubectl get pod -n web  
NAME                            READY   STATUS    RESTARTS   AGE
web-consumer-6cdb5f5cb8-59x85   1/1     Running   0          103s
web-consumer-6cdb5f5cb8-s9ww4   1/1     Running   0          104s

kubectl logs -n web web-consumer-6cdb5f5cb8-59x85



## UZ ide.
