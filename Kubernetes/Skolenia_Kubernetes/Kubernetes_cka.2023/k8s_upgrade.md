## Upgrade K8s
1. Upgrade control plane
```
kubectl drain node_name --ignore-daemonsets
sudo apt-get update
sudo apt-get install -y --allow-change-held-packages kubeadm=1.22.2-00
sudo kubeadm upgrade plan v1.22.2
sudo kubeadm upgrade apply v1.22.2
sudo apt-get install -y --allow-change-held-packages kubelet=1.22.2-00 kubectl=1.22.2-00
sudo systemctl daemon-reload
kubectl uncordon node_name
```

2. Upgrade workernode
```
kubectl drain worker_node_name --ignore-daemonsets
sudo apt-get update
sudo apt-get install -y --allow-change-held-packages kubeadm=1.22.2-00
sudo kubeadm upgrade node
sudo apt-get install -y --allow-change-held-packages kubelet=1.22.2-00 kubectl=1.22.2-00
sudo systemctl daemon-reload
sudo systemctl restart kubelet
kubectl uncordon worker_node_name
```





# LAB
```
cloud_user@k8s-control:~$ kubectl get nodes
NAME          STATUS   ROLES                  AGE   VERSION
k8s-control   Ready    control-plane,master   20m   v1.22.0
k8s-worker1   Ready    <none>                 19m   v1.22.0
k8s-worker2   Ready    <none>                 19m   v1.22.0

cloud_user@k8s-control:~$ kubectl drain k8s-control --ignore-daemonsets
node/k8s-control already cordoned
WARNING: ignoring DaemonSet-managed Pods: kube-system/calico-node-zjvln, kube-system/kube-proxy-mzp4q

sudo apt-get update
sudo apt-get install -y --allow-change-held-packages kubeadm=1.22.2-00

sudo kubeadm upgrade plan v1.22.2
sudo kubeadm upgrade apply v1.22.2

sudo apt-get install -y --allow-change-held-packages kubelet=1.22.2-00 kubectl=1.22.2-00


kubectl uncordon k8s-control

cloud_user@k8s-control:~$ kubectl get nodes
NAME          STATUS   ROLES                  AGE   VERSION
k8s-control   Ready    control-plane,master   24m   v1.22.2  <--------
k8s-worker1   Ready    <none>                 23m   v1.22.0
k8s-worker2   Ready    <none>                 23m   v1.22.0

```
