# Init container
- sluzi na nastavenie, 
- vykovana citlive operacie, pred startom app_kontainera
- sluzi na distribuciu udajov medzi kontainermi

---

```
apiVersion: v1
kind: Pod
metadata:
    name: init-pod
spec:
    restartPolicy: Always
    containers:
    - name: nginx
      image: nginx
    initContainers:
    - name: busybox
      image: busybox
      command: ['sh', '-c', 'sleep 30']      
```