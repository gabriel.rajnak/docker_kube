# Resources limits  requests (memory,cpu)

- request - kolko bume asi potrebovat, vyuziva ro scheduler
- limits  - maximum povolene, ak sa to pokusi prekonat, bude ukonceny

```
    resources:
        requests:
            cpu: "250m"
            memory: "128Mi"
        limits:
            cpu: "500m"
            memory: "256Mi"
```

