### Namespace
```
ubuntu@k3s:~$ sudo kubectl get ns
NAME              STATUS   AGE
default           Active   14d
kube-system       Active   14d
kube-public       Active   14d
kube-node-lease   Active   14d
```

```kubectl create namespace my-namespace```

```
ubuntu@k3s:~$ sudo kubectl create namespace my-namespace
namespace/my-namespace created
```
## Vylistuj pody v namespace
```
sudo kubectl get pods -n kube-system
NAME                                      READY   STATUS      RESTARTS      AGE
helm-install-traefik-crd-qxt5b            0/1     Completed   0             14d
helm-install-traefik-rjmzj                0/1     Completed   1             14d
svclb-traefik-e5d48445-dllfh              2/2     Running     2 (11m ago)   14d
svclb-traefik-e5d48445-mbs85              2/2     Running     2 (11m ago)   14d
traefik-7cd4fcff68-tpwm7                  1/1     Running     1 (11m ago)   14d
svclb-traefik-e5d48445-vw72j              2/2     Running     2 (11m ago)   14d
coredns-b96499967-vt7h4                   1/1     Running     1 (11m ago)   14d
metrics-server-668d979685-5jz94           1/1     Running     2 (11m ago)   14d
local-path-provisioner-7b7dc8d6f5-7rrxv   1/1     Running     2 (11m ago)   14d
```


### Pre vsetky namespaces
```sudo kubectl get pods --all-namespaces```

### Vytvor namespace
