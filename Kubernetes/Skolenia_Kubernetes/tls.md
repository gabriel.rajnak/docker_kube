# TLS certifikaty zaklady

## Asymentricke sifrovanie
```bash
# generuje public a private key
ssh-keygen

```

SSH
```bash
# privatny kluc
openssl genrsa -out my-bank.key 102
# verejny kluc
openssl rsa -in my-bank.key -pubout > my-bank.pem

```

CSR - certificate sign request
```bash
# vytvor key -> csr
openssl req -new -key my-bak.key -o my-back.csr -subj "/C=us/ST=CA/O=Myorg/CN=my-bank.com"
# CA podpise a posle naspat

```

# Kubernetes CA
- Root certificate (CA)
- client certificate
- server certificate
    
## Typy
- public key ( *.crt, *.pem )
- private ( *.key, *-key.pem )

## Komponenty
- kube-api apiservert.crt apiserver.key
- etcd atcdserver.crt atcdserver.key   
- kubelet kubelet.crt kubelet.key

## klienti idu cez kube-api komponent
- admin admin.cr admin.key 
- scheduler scheduler.crt scheduler.key
- kube-controller kube-controller.crt kube-controller.key
- kube-proxy kube-proxy.crt kube-proxy.key


# CA - certifikacna authorita
 - ca.key ca.crt

 # priprava certifikatom pre kubernetes
 
## Privatny kluc  self signed CA
 ```bash
openssl genrsa -out ca.key 2048
# ca.key

# vytvor CSR
openssl req -new -key ca.key -subj "/CN=kubernetes-ca" -out ca.csr
# ca.csr

# podpisem si CSR -> CRT
openssl x509 -req -in ca.csr -signkey ca.key -out ca.crt
# ca.crt
```

# Client certificate (admin)
```bash
openssl genrsa -out admin.key 2048
# admin.key

# vytvor CSR
#                                                     O=system:masters   -- bude mat pravo admin
openssl req -new -key admin.key -subj "/CN=kube-admin/O=system:masters" -out admin.csr
# admin.csr

# podpisem si CSR -> CRT
openssl x509 -req -in admin.csr -CA ca.crt -CAkey ca.key -out admin.crt
# admin.crt
```


## View certificate info
etcd
- etcd-cafile= ca.crt
- setcd-cert-file= etcd-client.crt
- key-file = etcd-client.key
- etcd-server = 127.0.0.1:2379



