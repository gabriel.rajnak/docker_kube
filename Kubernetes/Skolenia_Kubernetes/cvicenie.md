# CKA preparation

https://www.youtube.com/watch?v=Zm5sy6otLGc

1. storage
2. troubleshooting
3. workload & scheduling
4. cluster architecture & configuration
5. Service and networking
6. Aplication deployment

killercoda.com
- CKA 
## Aplication multicontainer issue


```alias k="kubectl"```

There is a multi-container Deployment in Namespace management which seems to have issues and is not getting ready.

Write the logs of all containers to /root/logs.log .

Can you see the reason for failure?


```
controlplane $ k get pods -A
NAMESPACE            NAME                                      READY   STATUS             RESTARTS      AGE
kube-system          calico-kube-controllers-9d57d8f49-9792m   1/1     Running            2 (18m ago)   12d
kube-system          canal-v2qtp                               2/2     Running            2 (18m ago)   12d
kube-system          coredns-7cbb7cccb8-qbq6x                  1/1     Running            1 (18m ago)   12d
kube-system          coredns-7cbb7cccb8-rknj5                  1/1     Running            1 (18m ago)   12d
kube-system          etcd-controlplane                         1/1     Running            1 (18m ago)   12d
kube-system          kube-apiserver-controlplane               1/1     Running            1 (18m ago)   12d
kube-system          kube-controller-manager-controlplane      1/1     Running            1 (18m ago)   12d
kube-system          kube-proxy-dm6fq                          1/1     Running            1 (18m ago)   12d
kube-system          kube-scheduler-controlplane               1/1     Running            1 (18m ago)   12d
local-path-storage   local-path-provisioner-5d854bc5c4-nhxjd   1/1     Running            1 (18m ago)   12d
management           collect-data-9b947d646-kthff              1/2     Error              4 (54s ago)   102s
management           collect-data-9b947d646-rfknl              1/2     CrashLoopBackOff   3 (50s ago)   102s
```


```bash
k get deployment -n management
k get deployment collect-data -n management -o yaml | grep image
k logs collect-data-9b947d646-kthff -c nginx  -n management  >/root/logs.log
k logs collect-data-9b947d646-kthff -c httpd  -n management  >>/root/logs.log
```

Fix the Deployment in Namespace management where both containers try to listen on port 80.

Remove one container.

```bash
controlplane $ k get deployment -A
NAMESPACE            NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
kube-system          calico-kube-controllers   1/1     1            1           12d
kube-system          coredns                   2/2     2            2           12d
local-path-storage   local-path-provisioner    1/1     1            1           12d
management           collect-data              0/2     2            0           12m  <-------------------------


controlplane $ k edit deployment collect-data -n management
deployment.apps/collect-data edited



controlplane $ k get deployment -A
NAMESPACE            NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
kube-system          calico-kube-controllers   1/1     1            1           12d
kube-system          coredns                   2/2     2            2           12d
local-path-storage   local-path-provisioner    1/1     1            1           12d
management           collect-data              1/2     2            1           14m  <-------------------------------
controlplane $ k get deployment -A
NAMESPACE            NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
kube-system          calico-kube-controllers   1/1     1            1           12d
kube-system          coredns                   2/2     2            2           12d
local-path-storage   local-path-provisioner    1/1     1            1           12d
management           collect-data              2/2     2            2           14m  <-----------------------------
```

---
## Startni node na control-plane

```alias k="kubectl"```


```bash
controlplane $ k run nginx --image=nginx --dry-run=client -o yaml 1.yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: nginx
  name: nginx
spec:
  containers:
  - args:
    - 1.yaml
    image: nginx
    name: nginx
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
controlplane $ k run nginx --image=nginx --dry-run=client -o yaml > 1.yaml

k create -f 1.yaml
controlplane $ k create -f 1.yaml 
pod/nginx created
controlplane $ ^C
controlplane $ k get pods
NAME    READY   STATUS    RESTARTS   AGE
nginx   1/1     Running   0          21s
controlplane $ k delete pod nginx
pod "nginx" deleted
controlplane $ 

```

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: nginx
  name: nginx
spec:
  nodeName: control-plane <----------------pridaj nodename
  containers:
  - image: nginx
    name: nginx
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

## Expose existing pod nginxpod as a service 

```bash
controlplane $ k expose pod nginxpod --name=nginxsvc --port=80
service/nginxsvc exposed
controlplane $ k get svc
NAME         TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1        <none>        443/TCP   12d
nginxsvc     ClusterIP   10.107.159.194   <none>        80/TCP    5s
```
kontrola svc
```bash
controlplane $ curl 10.107.159.194
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
```
## Expose pos nginxpod na nodeport 30200
```bash
controlplane $ k expose pod nginxpod --name=nginxnodeportsvc --type=NodePort --port=80
service/nginxnodeportsvc exposed
controlplane $ k get svc
NAME               TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)           AGE
kubernetes         ClusterIP   10.96.0.1        <none>        443/TCP           12d
nginxnodeportsvc   NodePort    10.105.135.128   <none>        80:31986/TCP       7s
nginxsvc           ClusterIP   10.107.159.194   <none>        80/TCP            5m26s
```
Upravit port
```bash
k edit svc nginxnodeportsvc

Change to - nodePort: 30200

```
kontrola
```bash

controlplane $ k get nodes -o wide
NAME           STATUS   ROLES           AGE   VERSION   INTERNAL-IP   EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
controlplane   Ready    control-plane   12d   v1.29.0   172.30.1.2    <none>        Ubuntu 20.04.5 LTS   5.4.0-131-generic   containerd://1.6.12
node01         Ready    <none>          12d   v1.29.0   172.30.2.2    <none>        Ubuntu 20.04.5 LTS   5.4.0-131-generic   containerd://1.6.12  <---tato node IP adresa


controlplane $ curl 172.30.2.2:30200
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```





---
https://www.youtube.com/watch?v=Xr1IlUm9E48
