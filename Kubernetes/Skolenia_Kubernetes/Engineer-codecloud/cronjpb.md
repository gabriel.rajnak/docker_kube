# cron bob

```
The Nautilus DevOps team is setting up recurring tasks on different schedules. Currently, they're developing scripts to be executed periodically. To kickstart the process, they're creating cron jobs in the Kubernetes cluster with placeholder commands. Follow the instructions below:

Create a cronjob named datacenter.
Set Its schedule to something like */3 * * * *. You can set any schedule for now.
Name the container cron-datacenter.
Utilize the nginx image with latest tag (specify as nginx:latest).
Execute the dummy command echo Welcome to xfusioncorp!.
Ensure the restart policy is OnFailure.
Note: The kubectl utility on jump_host is configured to work with the kubernetes cluster.
```
```bash
kubectl create cronjob NAME --image=image --schedule='0/5 * * * ?' -- [COMMAND] [args...] [flags] [options]

kubectl create cronjob datacenter --image=nginx:latest --schedule='0/3 * * * ?' --restart=OnFailure --dry-run=client -o yaml -- echo 'Welcome to xfusioncorp!' 
```
```yaml
apiVersion: batch/v1
kind: CronJob
metadata:
  creationTimestamp: null
  name: datacenter
spec:
  jobTemplate:
    metadata:
      creationTimestamp: null
      name: datacenter
    spec:
      template:
        metadata:
          creationTimestamp: null
        spec:
          containers:
          - command:
            - echo
            - Welcome to xfusioncorp!
            image: nginx:latest
            name: cron-datacenter
            resources: {}
          restartPolicy: OnFailure
  schedule: 0/3 * * * *
status: {}
```
