# 1.Take a backup of the etcd cluster and save it to /opt/etcd-backup.db. 

```
/etc/kubernetes/manifest/etcd.yaml vybe kluce


ETCDCTL_API=3 etcdctl --endpoints=https://127.0.0.1:2379 \
   --cacert=/etc/kubernetes/pki/etcd/ca.crt \
   --cert=/etc/kubernetes/pki/etcd/server.crt  \
   --key=/etc/kubernetes/pki/etcd/server.key \
  snapshot save /opt/etcd-backup.db

  ```

# 2. Create a Pod called redis-storage with image: redis:alpine with a Volume of type emptyDir that lasts for the life of the Pod.
```
Specs on the below.
Pod named 'redis-storage' created
Pod 'redis-storage' uses Volume type of emptyDir
Pod 'redis-storage' uses volumeMount with mountPath = /data/redis
```
```bash
k run redis-storage  --image=redis:alpine --dry-run=client -o yaml > pod.yaml 
#  pod.yaml 
```
```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: redis-storage
  name: redis-storage
spec:
  containers:
  - image: redis:alpine
    name: redis-storage
    volumeMounts:
    - mountPath:  /data/redis
      name: empty
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
  volumes:
    - name: empty
      emptyDir:
```

# 3. Create a new pod called super-user-pod with image busybox:1.28. Allow the pod to be able to set system_time.
```
The container should sleep for 4800 seconds.
Pod: super-user-pod
Container Image: busybox:1.28
Is SYS_TIME capability set for the container?
```
```bash
k run super-user-pod --image=busybox:1.28 --dry-run=client -o yaml

k exec -it redis-storage -- /bin/sh
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: super-user-pod
  name: super-user-pod
spec:
  containers:
  - image: busybox:1.28
    name: super-user-pod
    resources: {}
    command: [ '/bin/sh','-c','sleep 4800' ]
    securityContext:
      capabilities:
        add: [ "SYS_TIME"]
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

# 4. A pod definition file is created at /root/CKA/use-pv.yaml. Make use of this manifest file and mount the persistent volume called pv-1. Ensure the pod is running and the PV is bound.

mountPath: /data

persistentVolumeClaim Name: my-pvc
persistentVolume Claim configured correctly
pod using the correct mountPath
pod using the persistent volume claim?

```yaml
controlplane ~ ➜  cat pod.yaml 
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: use-pv
  name: use-pv
spec:
  containers:
  - image: nginx
    name: use-pv
    resources: {}
    volumeMounts:
    - name: data
      mountPath: /data
  volumes:
    - name: data
      persistentVolumeClaim:
        claimName: data
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: my-pvc
spec:  
  accessModes:
    - ReadWriteOnce
      #volumeName: pv-1
  resources:
    requests:
      storage: 10Mi

```

# 5. Create a new deployment called nginx-deploy, with image nginx:1.16 and 1 replica. Next upgrade the deployment to version 1.17 using rolling update.
```
Deployment : nginx-deploy. Image: nginx:1.16
Image: nginx:1.16
Task: Upgrade the version of the deployment to 1:17
Task: Record the changes for the image upgrade
```

```bash
controlplane ~ ➜  k create deployment nginx-deploy --image=nginx:1.16 --replicas=1
deployment.apps/nginx-deploy created
# upgrade
k set image deployment/nginx-deploy nginx=nginx:1.17 

kubectl rollout history  deployment nginx-deploy
deployment.apps/nginx-deploy 
REVISION  CHANGE-CAUSE
1         <none>


controlplane ~ ➜  kubectl rollout history  deployment nginx-deploy
deployment.apps/nginx-deploy 
REVISION  CHANGE-CAUSE
1         <none>
2         <none>
```


# 6. Create a new user called john. Grant him access to the cluster. John should have permission to create, list, get, update and delete pods in the development namespace . The private key exists in the location: /root/CKA/john.key and csr at /root/CKA/john.csr.

Important Note: As of kubernetes 1.19, the CertificateSigningRequest object expects a signerName.

Please refer the documentation to see an example. The documentation tab is available at the top right of terminal.
CSR: john-developer Status:Approved
Role Name: developer, namespace: development, Resource: Pods
Access: User 'john' has appropriate permissions

```bash
# mame key, crt -> csr files
# vytvor csr pre k8s 
cat /root/CKA/john.csr | base64 | tr -d "\n"
```
```yaml
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: john
spec:
  request: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURSBSRVFVRVNULS0tLS0KTUlJQ1ZEQ0NBVHdDQVFBd0R6RU5NQXNHQTFVRUF3d0VhbTlvYmpDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRApnZ0VQQURDQ0FRb0NnZ0VCQU1tUnRlS29LaUZna3BoTjNJR2pxZWQ1UjI0Tkt3NjdpRFBQSDd0MzA5UEtWZ05uCitDSzNaaStFRGFSRXA0NkQvMHl5dHh1bnR1UCtReGhTdnR5dG9nVEZTT1N4emN3bjNnMm1CaGFBTjlWbStLVysKZXZYU1ZQU2pkSytyYlAyL25XWjc0ZjRHa0NjRUp1N1BCOXl3UzMwOVcrSW5RVkRRenhoSkhHTjE5c3duUGNjbgowMmtJbkVITHFnZVdGOFFTdVlEdWhxUVVCQzFCY2pkR2ZibUZlL29UaTNEemdJNzBRSjBDNDlJTmJ3WXBSU1cyCnpzTy9VNnNKbkduNmlDcE1YWlZqTVJwMVQ3eDN3OFB2RDd4VTB4emlWSTNaY0gzVDlUT1pOOVRhR1FSeHhCZnAKajVpNUtXSjRrWGloM1Y4cDJpT2V2RzVzT2djblpMTVRIY3F6TjcwQ0F3RUFBYUFBTUEwR0NTcUdTSWIzRFFFQgpDd1VBQTRJQkFRQUp6bEQ2c1RyVWdXNmVKNk5waE1rVmpITHRicWtoTGlPdnZaaUtMYWRoRUY1MEVMUTlOelVzCnloVFBmZ0o4SmZ2VDRvNy8xclRtMkJGd080N0h0QiswdWhPZG1WT3VyZ1dQRVZSbnUrQTViUU4rS25YMVhIaksKZ2UzaUpVTmhydFFwYUl1bE1QS0FEZ1pXNUZiSlNvWGJsS1hHenF6ZUxIektPY2ZBNVZOTVRaT0txcUdoOXRVdwpqZ3drZWcxYTA2bzBHZnZLUlcwY1hNMDVUZ2Y3R3ZVTjNpK3p3aEdlYnZldW15TWQybEdCTGVxMHZ0bjNZZHl4CkwzODFmczRKR29TdFZHTVFhR2ZQbTJqVFk5L0NPeXRDWVN1b3N1Y2RKcE0xVis0UGtVOUt1RDdDblVQQTdDMkEKRU1vcGRiYnRXK2RnTlVsSUdNMEVtWndMb3Uwd09Qc0gKLS0tLS1FTkQgQ0VSVElGSUNBVEUgUkVRVUVTVC0tLS0tCg==
  signerName: kubernetes.io/kube-apiserver-client
  expirationSeconds: 86400  # one day
  usages:
  - client auth
```

```bash
# create 
controlplane ~/CKA ➜  k create -f csr.yaml 
certificatesigningrequest.certificates.k8s.io/john created

controlplane ~/CKA ➜  k get csr
NAME        AGE   SIGNERNAME                                    REQUESTOR                  REQUESTEDDURATION   CONDITION
csr-fdxwr   71m   kubernetes.io/kube-apiserver-client-kubelet   system:bootstrap:95qb2a    <none>              Approved,Issued
csr-x7rhx   72m   kubernetes.io/kube-apiserver-client-kubelet   system:node:controlplane   <none>              Approved,Issued
john        8s    kubernetes.io/kube-apiserver-client           kubernetes-admin           24h                 Pending

# approve
controlplane ~/CKA ➜  kubectl certificate approve john
certificatesigningrequest.certificates.k8s.io/john approved

controlplane ~/CKA ➜  k get csr
NAME        AGE   SIGNERNAME                                    REQUESTOR                  REQUESTEDDURATION   CONDITION
csr-fdxwr   72m   kubernetes.io/kube-apiserver-client-kubelet   system:bootstrap:95qb2a    <none>              Approved,Issued
csr-x7rhx   73m   kubernetes.io/kube-apiserver-client-kubelet   system:node:controlplane   <none>              Approved,Issued
john        77s   kubernetes.io/kube-apiserver-client           kubernetes-admin           24h                 Approved,Issued

# rola
  # Create a role named "foo" with SubResource specified
controlplane ~/CKA ✖ kubectl create role developer  --verb=create,list,get,update,delete --resource=pods -n development
role.rbac.authorization.k8s.io/developer created
# rolebinding
k create rolebinding john-developer --role=developer --user=john -n development

```

# 7. Create a nginx pod called nginx-resolver using image nginx, expose it internally with a service called nginx-resolver-service. Test that you are able to look up the service and pod names from within the cluster. Use the image: busybox:1.28 for dns lookup. Record results in /root/CKA/nginx.svc and /root/CKA/nginx.pod
```
Pod: nginx-resolver created
Service DNS Resolution recorded correctly
Pod DNS resolution recorded correctly
```

```bash
k run nginx-resolver --image=nginx:latest 
k expose pod nginx-resolver --type=ClusterIP  --name=nginx-resolver-service  --port=80
# startni pomocny pod
k run busybox --image=busybox:1.28 -- sleep 4000
k exec pod busybox -- nslookup nginx-resolver-service  > /root/CKA/nginx.svc
#  ?? TODO: nieviem
k exec pod busybox -- nslookup nginx-resolver  > /root/CKA/nginx.pod


```
# 8. Create a static pod on node01 called nginx-critical with image nginx and make sure that it is recreated/restarted automatically in case of a failure.
```
Use /etc/kubernetes/manifests as the Static Pod path for example.
static pod configured under /etc/kubernetes/manifests ?
Pod nginx-critical-node01 is up and running
```

```bash
ssh node-01

k run nginx-critical --image=nginx --replicas=1 --restart=Always dry-run=client -o yaml > pod.yaml

cp pod.yaml /etc/kubermetes/manifest/
```


