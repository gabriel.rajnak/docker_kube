# test1 treti pokus

# 1. Deploy a pod named nginx-pod using the nginx:alpine image.

Once done, click on the Next Question button in the top right corner of this panel. You may navigate back and forth freely between all questions. Once done with all questions, click on End Exam. Your work will be validated at the end and score shown. Good Luck!
```
Name: nginx-pod
Image: nginx:alpine
```
```bash
controlplane ~ ➜  k run nginx-pod --image=nginx:alpine
pod/nginx-pod created

controlplane ~ ➜  kegt pod
-bash: kegt: command not found

controlplane ~ ✖ k get pod
NAME        READY   STATUS    RESTARTS   AGE
nginx-pod   1/1     Running   0          53s
```

# 2.Deploy a messaging pod using the redis:alpine image with the labels set to tier=msg.

```
Pod Name: messaging
Image: redis:alpine
Labels: tier=msg
```
```bash
k run messaging --image=redis:alpine --labels="tier=msg"
pod/messaging created

controlplane ~ ➜  k describe pod messaging
```

# 3. Create a namespace named apx-x9984574.

Namespace: apx-x9984574
```bash
controlplane ~ ➜  k create ns apx-x9984574
namespace/apx-x9984574 created

controlplane ~ ➜  k get ns
NAME              STATUS   AGE
apx-x9984574      Active   3s
default           Active   82m
kube-flannel      Active   82m
kube-node-lease   Active   82m
kube-public       Active   82m
kube-system       Active   82m

controlplane ~ ➜  
```

# 4. nodes jsooon

```
k get nodes -o json >/opt/outputs/nodes-z3444kd9.json
```

# 5. Create a service messaging-service to expose the messaging application within the cluster on port 6379.
```
Use imperative commands.
Service: messaging-service
Port: 6379
Type: ClusterIp
Use the right labels
```
```bash
controlplane ~ ✖ k expose pod messaging --name=messaging-service --type=ClusterIP --port 6379
service/messaging-service exposed

controlplane ~ ➜  k get svc messaging-service  -o yaml
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: "2024-05-26T17:18:32Z"
  labels:
    tier: msg
  name: messaging-service
  namespace: default
  resourceVersion: "7164"
  uid: 296394d7-cb55-41db-9f16-90c628f3060f
spec:
  clusterIP: 10.111.114.0
  clusterIPs:
  - 10.111.114.0
  internalTrafficPolicy: Cluster
  ipFamilies:
  - IPv4
  ipFamilyPolicy: SingleStack
  ports:
  - port: 6379
    protocol: TCP
    targetPort: 6379
  selector:
    tier: msg
  sessionAffinity: None
  type: ClusterIP
status:
  loadBalancer: {}
  ```

  # 6. Create a deployment named hr-web-app using the image kodekloud/webapp-color with 2 replicas.
```
Name: hr-web-app
Image: kodekloud/webapp-color
Replicas: 2
```

```bash
k create deployment hr-web-app --image=kodekloud/webapp-color --replicas=2
deployment.apps/hr-web-app created


controlplane ~ ✖ k get deployment hr-web-app -o yaml
```
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    deployment.kubernetes.io/revision: "1"
  creationTimestamp: "2024-05-26T17:20:21Z"
  generation: 1
  labels:
    app: hr-web-app
  name: hr-web-app
  namespace: default
  resourceVersion: "7346"
  uid: 6cfd096f-78bb-43a2-8936-7d10304122cc
spec:
  progressDeadlineSeconds: 600
  replicas: 2
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: hr-web-app
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: hr-web-app
    spec:
      containers:
      - image: kodekloud/webapp-color
        imagePullPolicy: Always
        name: webapp-color
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
```      


# 7. Create a static pod named static-busybox on the controlplane node that uses the busybox image and the command sleep 1000.
```
Name: static-busybox
Image: busybox
```

k run static-busybox --image=busybox --dry-run=client -o yaml --command --  "sleep 1000"   > static.yaml
```yaml
controlplane ~ ➜  cat static.yaml 
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: static-busybox
  name: static-busybox
spec:
  containers:
  - command: ["/bin/sh","-c","sleep 1000"]
    image: busybox
    name: static-busybox
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

# 8. Create a POD in the finance namespace named temp-bus with the image redis:alpine.
```
Name: temp-bus
Image Name: redis:alpine
```
```bash
controlplane ~ ➜  k run temp-bus --image=redis:alpine -n finance
pod/temp-bus created
```

# 9. fix app
- init container sleeep

# 10.Expose the hr-web-app as service hr-web-app-service application on port 30082 on the nodes on the cluster.
```
The web application listens on port 8080.
Name: hr-web-app-service
Type: NodePort
Endpoints: 2
Port: 8080
NodePort: 30082
```
```bash
k expose deployment hr-web-app --name hr-web-app-service --type=NodePort --port=8080  --target-port=30082
service/hr-web-app-service exposed


```

# 11.json

```
 k get nodes -o jsonpath='{.items[*].status.nodeInfo.osImage}' > /opt/outputs/nodes_os_x43kj56.txt
 ```


 # 12. Create a Persistent Volume with the given specification: -
```
Volume name: pv-analytics
Storage: 100Mi
Access mode: ReadWriteMany
Host path: /pv/data-analytics
```
```bash
controlplane ~ ➜  k create -f pv.yaml 
persistentvolume/pv-analytics created

controlplane ~ ➜  cat pv.yaml 
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-analytics
spec:
  capacity:
    storage: 100Mi
  accessModes:
    - ReadWriteMany
  hostPath:
    path: /pv/data-analytics
```
