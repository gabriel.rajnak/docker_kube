# CKA exam 01
[https://kubernetes.io/docs/reference/kubectl/quick-reference/]
```bash

alias k=lubectl

# BASH
source <(kubectl completion bash) # set up autocomplete in bash into the current shell, bash-completion package should be installed first.
echo "source <(kubectl completion bash)" >> ~/.bashrc # add autocomplete permanently to your bash shell.

```

--------
# 1. Deploy a pod named nginx-pod using the nginx:alpine image.  -OK

Once done, click on the Next Question button in the top right corner of this panel. You may navigate back and forth freely between all questions. Once done with all questions, click on End Exam. Your work will be validated at the end and score shown. Good Luck!
```
Name: nginx-pod
Image: nginx:alpine
```
```bash
k run nginx-pod --image=nginx:alpine
pod/nginx-pod created

controlplane ~ ➜  k get pod
NAME        READY   STATUS    RESTARTS   AGE
nginx-pod   1/1     Running   0          3s
```

------
# 2. Deploy a messaging pod using the redis:alpine image with the labels set to tier=msg.
```
Pod Name: messaging
Image: redis:alpine
Labels: tier=msg
```
```bash
controlplane ~ ✖ k run messaging --image=redis:alpine --labels="tier=msg"
pod/messaging created

controlplane ~ ➜  k get pods
NAME        READY   STATUS    RESTARTS   AGE
messaging   1/1     Running   0          5s
nginx-pod   1/1     Running   0          2m28

```
--------------------
# 3. Create a namespace named apx-x9984574.
```
Namespace: apx-x9984574
```
```bash
controlplane ~ ➜  k create ns apx-x9984574
namespace/apx-x9984574 created

controlplane ~ ➜  k get ns
NAME              STATUS   AGE
apx-x9984574      Active   4s
default           Active   108m
kube-flannel      Active   108m
kube-node-lease   Active   108m
kube-public       Active   108m
kube-system       Active   108m

controlplane ~ ➜  
```


# 4. Get the list of nodes in JSON format and store it in a file at /opt/outputs/nodes-z3444kd9.json.

```bash
 k get nodes -o json >/opt/outputs/nodes-z3444kd9.json
```

# 5. Create a service messaging-service to expose the messaging application within the cluster on port 6379.   --FIX
```
Use imperative commands.
Service: messaging-service
Port: 6379
Type: ClusterIp
Use the right labels
```
```bash

k create service clusterip messaging-service --tcp=6379 


# FIX
# 
k expose pod messaging --port 6379 --name messaging-service
#
#
```
-------------------- 
# 6. Create a deployment named hr-web-app using the image kodekloud/webapp-color with 2 replicas.
```
Name: hr-web-app
Image: kodekloud/webapp-color
Replicas: 2
```
```bash
k create deployment hr-web-app --image=kodekloud/webapp-color --replicas=2
deployment.apps/hr-web-app created

controlplane ~ ➜  k get deployment
NAME         READY   UP-TO-DATE   AVAILABLE   AGE
hr-web-app   2/2     2            2           16s

```
--------------------
# 7. Create a static pod named static-busybox on the controlplane node that uses the busybox image and the command sleep 1000. -- fix
```
Name: static-busybox
Image: busybox
```
```bash
k run static-busybox --image=busybox --dry-run=client   -o yaml > pod.yaml
#
# alternatriva
k run static-busybox --image=busybox --dry-run=client   -o yaml --command -- sleep 1000 > pod.yaml
controlplane ~ ✖ cat pod.yaml 
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: static-busybox
  name: static-busybox
spec:
  containers:
  - image: busybox
    name: static-busybox
    resources: {}
    command: ["/bin/sh"]
    args: ["-c", "sleep 1000"]
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```
COPY to: ```cp pod.yaml /etc/kubernetes/manifests/```

--------------------
# 8. Create a POD in the finance namespace named temp-bus with the image redis:alpine.
```
Name: temp-bus
Image Name: redis:alpine
```

```bash
controlplane ~ ➜  k run temp-bus --image=redis:alpine -n finance
pod/temp-bus created

controlplane ~ ➜  k get pods -A
NAMESPACE      NAME                                   READY   STATUS    RESTARTS   AGE
default        hr-web-app-7f7dfbd5d9-j9wrz            1/1     Running   0          10m
default        hr-web-app-7f7dfbd5d9-rvzks            1/1     Running   0          10m
default        messaging                              1/1     Running   0          23m
default        nginx-pod                              1/1     Running   0          26m
default        static-busybox                         1/1     Running   0          2m35s
finance        temp-bus                               1/1     Running   0          4s
kube-flannel   kube-flannel-ds-7ml5f                  1/1     Running   0          130m
kube-system    coredns-69f9c977-46m8n                 1/1     Running   0          130m
kube-system    coredns-69f9c977-zfpt2                 1/1     Running   0          130m
kube-system    etcd-controlplane                      1/1     Running   0          131m
kube-system    kube-apiserver-controlplane            1/1     Running   0          131m
kube-system    kube-controller-manager-controlplane   1/1     Running   0          131m
kube-system    kube-proxy-qxk4x                       1/1     Running   0          130m
kube-system    kube-scheduler-controlplane            1/1     Running   0          131m
```

# 9. A new application orange is deployed. There is something wrong with it. Identify and fix the issue.

```bash
k get pod
NAME                          READY   STATUS                  RESTARTS      AGE
hr-web-app-7f7dfbd5d9-j9wrz   1/1     Running                 0             14m
hr-web-app-7f7dfbd5d9-rvzks   1/1     Running                 0             14m
messaging                     1/1     Running                 0             27m
nginx-pod                     1/1     Running                 0             30m
orange                        0/1     Init:CrashLoopBackOff   5 (53s ago)   3m45s
static-busybox                1/1     Running                 0             6m37s

k describe pod orange
k get pod orange -o yaml

pozriet init container,opravit sleeep
```
--------------------
# 10. Expose the hr-web-app as service hr-web-app-service application on port 30082 on the nodes on the cluster.  --- FIX
```
The web application listens on port 8080.
Name: hr-web-app-service
Type: NodePort
Endpoints: 2
Port: 8080
NodePort: 30082
```
```bash
k create service nodeport hr-web-app-service --tcp=30082:8080 

#
# FIX
k expose deploy hr-web-app --name hr-web-app-service  --type NodePort 8080
k edit svc hr-web-app
  - prepisat Nodeport na 30082


```
--------------------
# 11. Use JSON PATH query to retrieve the osImages of all the nodes and store it in a file /opt/outputs/nodes_os_x43kj56.txt.  -- fix
```
The osImages are under the nodeInfo section under status of each node.
```

```bash

controlplane ~ ➜  k get nodes  -o=jsonpath='{.items[*].status.nodeInfo.osImage}'  | jq 
parse error: Invalid numeric literal at line 1, column 7

#
# FIX
k get nodes  -o jsonpath='{.items[*].status.nodeInfo.osImage}'  > /opt/output/xxxx.txt
```
--------------------
# 12. Create a Persistent Volume with the given specification: - FIX
```
Volume name: pv-analytics
Storage: 100Mi
Access mode: ReadWriteMany
Host path: /pv/data-analytics
```
```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-analytics
spec:
  capacity: 100Mi
  accessModes:
   - ReadWriteMany
  hostPath:
    path: /pv/data-analytics
```

fix: 5 7 10 11 12
## Precvicit

```
kubectl expose    pod.deployment
persistent volume
json
```
