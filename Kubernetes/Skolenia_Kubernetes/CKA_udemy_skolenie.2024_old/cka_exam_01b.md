# test 1
# druhy pokus
# 1. Deploy a pod named nginx-pod using the nginx:alpine image.

Once done, click on the Next Question button in the top right corner of this panel. You may navigate back and forth freely between all questions. Once done with all questions, click on End Exam. Your work will be validated at the end and score shown. Good Luck!
```
Name: nginx-pod
Image: nginx:alpine
```

```bash
controlplane ~ ➜  alias k=kubectl

controlplane ~ ➜  

controlplane ~ ➜  k run nginx-pod --image=nginx:alpine
pod/nginx-pod created

controlplane ~ ➜  k get pods
NAME        READY   STATUS    RESTARTS   AGE
nginx-pod   1/1     Running   0          5s
```

# 2. Deploy a messaging pod using the redis:alpine image with the labels set to tier=msg.
```
Pod Name: messaging
Image: redis:alpine
Labels: tier=msg
```
```bash
controlplane ~ ➜  k run messaging --image=redis:alpine --labels="tier=msg"
pod/messaging created

controlplane ~ ➜  k get pods
NAME        READY   STATUS    RESTARTS   AGE
messaging   1/1     Running   0          7s
nginx-pod   1/1     Running   0          109s
```

# 3. Create a namespace named apx-x9984574.

Namespace: apx-x9984574

```bash
namespace/apx-x9984574 created

controlplane ~ ➜  k get ns
NAME              STATUS   AGE
apx-x9984574      Active   3s
default           Active   78m
kube-flannel      Active   77m
kube-node-lease   Active   78m
kube-public       Active   78m
kube-system       Active   78m
```

# 4. Get the list of nodes in JSON format and store it in a file at /opt/outputs/nodes-z3444kd9.json.

```bash
k get nodes -o json >/opt/outputs/nodes-z3444kd9.json
```

# 5. Create a service messaging-service to expose the messaging application within the cluster on port 6379.
```
Use imperative commands.
Service: messaging-service
Port: 6379
Type: ClusterIp
Use the right labels
```
```bash
controlplane ~ ➜  kubectl expose pod  messaging --port=6379 --name=messaging-service --type=ClusterIP
service/messaging-service exposed

controlplane ~ ✖ k describe svc messaging-service
Name:              messaging-service
Namespace:         default
Labels:            tier=msg
Annotations:       <none>
Selector:          tier=msg
Type:              ClusterIP
IP Family Policy:  SingleStack
IP Families:       IPv4
IP:                10.102.87.144
IPs:               10.102.87.144
Port:              <unset>  6379/TCP
TargetPort:        6379/TCP
Endpoints:         10.244.0.5:6379
Session Affinity:  None
Events:            <none>
```

# 6. Create a deployment named hr-web-app using the image kodekloud/webapp-color with 2 replicas.

```
Name: hr-web-app
Image: kodekloud/webapp-color
Replicas: 2
```
```bash
controlplane ~ ➜  k create deployment hr-web-app --image=kodekloud/webapp-color --replicas=2
deployment.apps/hr-web-app created

controlplane ~ ➜  k get pods
NAME                          READY   STATUS              RESTARTS   AGE
hr-web-app-7f7dfbd5d9-2q8hk   0/1     ContainerCreating   0          6s
hr-web-app-7f7dfbd5d9-95dkw   0/1     ContainerCreating   0          6s
messaging                     1/1     Running             0          11m
nginx-pod                     1/1     Running             0          13m
```

# 7. 
Create a static pod named static-busybox on the controlplane node that uses the busybox image and the command sleep 1000.
```
Name: static-busybox
Image: busybox
```

```bash
controlplane ~ ➜  k run static-busybox --image=busybox --command "sleep 1000" --dry-run=client -o yaml

controlplane ~ ➜  cat /etc/kubernetes/manifests/static.yaml 
controlplane ~ ➜  cat static.yaml 
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: static-busybox
  name: static-busybox
spec:
  containers:
  - command: [ 'sh','-c','sleep 1000']
    image: busybox
    name: static-busybox
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}


cp static.yaml /etc/kubernetes/manifests/static.yaml 
```

# 8. Create a POD in the finance namespace named temp-bus with the image redis:alpine.
```
Name: temp-bus
Image Name: redis:alpine
```
```bash
controlplane ~ ➜  k run temp-bus --image=redis:alpine
pod/temp-bus created

controlplane ~ ➜  k get pods
NAME                          READY   STATUS    RESTARTS   AGE
hr-web-app-7f7dfbd5d9-2q8hk   1/1     Running   0          17m
hr-web-app-7f7dfbd5d9-95dkw   1/1     Running   0          17m
messaging                     1/1     Running   0          29m
nginx-pod                     1/1     Running   0          31m
static-busybox-controlplane   1/1     Running   0          77s
temp-bus                      1/1     Running   0          3s
```

TODO: namespace  !!!!!!

# 9. A new application orange is deployed. There is something wrong with it. Identify and fix the issue.

- init kontainer nestartije
```
  Normal   Pulled     13s (x2 over 28s)  kubelet            Successfully pulled image "busybox" in 303ms (303ms including waiting)
  Warning  BackOff    0s (x3 over 27s)   kubelet            Back-off restarting failed container init-myservice in pod orange_default(0e518da2-7d21-40ff-8828-0fda68bd0a8e)
  ```
  - edituj orange (init sleeeep)



# 10. Expose the hr-web-app as service hr-web-app-service application on port 30082 on the nodes on the cluster.
``` 
The web application listens on port 8080.
Name: hr-web-app-service
Type: NodePort
Endpoints: 2
Port: 8080
NodePort: 30082
```
```bash
#kubectl expose deployment nginx --port=80 --target-port=8000

kubectl expose deployment hr-web-app --port=8080 --target-port=30082 --name hr-web-app-service --type NodePort

kubectl expose deployment hr-web-app --type=NodePort --name=hr-web-app-service --port=8080 --target-port=8080 --node-port=30082            chat gpt ???


- port - service pocuva clusterIP ???
- node-port   - fuzicky na node
- targetPort  - na pode


controlplane ~ ➜  k get svc
NAME                 TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
hr-web-app-service   NodePort    10.110.136.108   <none>        8080:32338/TCP   15s
kubernetes           ClusterIP   10.96.0.1        <none>        443/TCP          119m
messaging-service    ClusterIP   10.102.87.144    <none>        6379/TCP         33m

controlplane ~ ➜  k get svc hr-web-app-service -o yaml
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: "2024-05-24T13:14:10Z"
  labels:
    app: hr-web-app
  name: hr-web-app-service
  namespace: default
  resourceVersion: "9957"
  uid: 84c8ab48-4925-4f30-8581-1d9567113ee6
spec:
  clusterIP: 10.110.136.108
  clusterIPs:
  - 10.110.136.108
  externalTrafficPolicy: Cluster
  internalTrafficPolicy: Cluster
  ipFamilies:
  - IPv4
  ipFamilyPolicy: SingleStack
  ports:
  - nodePort: 32338
    port: 8080
    protocol: TCP
    targetPort: 30082
  selector:
    app: hr-web-app
  sessionAffinity: None
  type: NodePort
status:
  loadBalancer: {}
```

# 11. Use JSON PATH query to retrieve the osImages of all the nodes and store it in a file /opt/outputs/nodes_os_x43kj56.txt.

The osImages are under the nodeInfo section under status of each node.
```bash
controlplane ~ ➜  k get nodes -o jsonpath='{.items[].status.nodeInfo.osImage}'
Ubuntu 22.04.4 LTS
controlplane ~ ➜  k get nodes -o jsonpath='{.items[].status.nodeInfo.osImage}' > /opt/outputs/nodes_os_x43kj56.txt

controlplane ~ ➜  cat /opt/outputs/nodes_os_x43kj56.txt
Ubuntu 22.04.4 LTS

```

# 12. Create a Persistent Volume with the given specification: -
```
Volume name: pv-analytics
Storage: 100Mi
Access mode: ReadWriteMany
Host path: /pv/data-analytics
```

```yaml
controlplane ~ ➜  cat pv.yaml 
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-analytics
spec:
  capacity:
    storage: 100Mi  
  accessModes: 
    - ReadWriteMany
  hostPath:
    path: /pv/data-analytics

```

