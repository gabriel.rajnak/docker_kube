#  Certified Kubernetes Administrators Certification CKA


autokomplet: https://kubernetes.io/pt-br/docs/reference/kubectl/cheatsheet/


Certified Kubernetes Administrator: https://www.cncf.io/certification/cka/

Exam Curriculum (Topics): https://github.com/cncf/curriculum

Candidate Handbook: https://www.cncf.io/certification/candidate-handbook

Exam Tips: http://training.linuxfoundation.org/go//Important-Tips-CKA-CKAD

certifikacia: https://www.dpbolvw.net/click-100910441-15404327?sid=udemy-courses


https://kodekloud.com/pages/community    videa, komunita



ZLAVA: 10% 20KODE

# Trilogy
- basics
- CKA
- CKAD

# Slack https://kodekloud.com/pages/community


[Kubernetes The Hard Way On VirtualBox](https://github.com/mmumshad/kubernetes-the-hard-way)

[poznamky ku kurzu](https://github.com/kodekloudhub/certified-kubernetes-administrator-course)

## 2.Zakladne koncepty

### Architektura  11
- master node - riadi cely kluster(control plane)
    - etcd -key-value databaza
    - kube-scheduler - planuje pridelenie zdrojov(ram, cpu, storage)
    - node-controler 
    - repliation-controler 
    - kube-api (interna komunikacia klustra)
    - kubelet agent(na kazom node, riadi node)
    - kube-proxy(zabezpecuje komunikaciu medzi kontainermi)

### Docker vs. containerd 12
* CRI - container runtime interface
* OCI - open container initiatime

```bash
ctr image pull docker.io/...image

nerdctl ...

crictl pull busybox
crictl ps -a
crictl exec -i -t 329842983749238749823749823 ls
crictl logs 024758437059347029
crictl pods
```

### ETCD for beginner 13
- interna vysokodostupna databaza(key/value)
- port 2379

```bash
etcdctl set key1 val1
etcdctl get key

etcdctl --version

ETCDCTL_API=3 ./etcdctl version    <--- fixne verziu

# novy pristup
export ETCDCTL_API=3
etcdctl put key1 val1
etcdctl get key1
```

### ETCD in kubernetes 14
- manualne 
- kubeadm (startne etcd ako pod)
- port 2379






### ETCD commands 15

```bash
# verzia 2.0
etcdctl backup
etcdctl cluster-health
etcdctl mk
etcdctl mkdir
etcdctl set
# verzia 3
etcdctl snapshop save
etcdctl endpoint health
etcdctl get
etcdctl put

# nastav verziu
export ETCDCTL_API=3

# nastavenie certifikatov
--cacert /etc/kubernetes/pki/etcd/ca.crt     
--cert /etc/kubernetes/pki/etcd/server.crt     
--key /etc/kubernetes/pki/etcd/server.key

# priklad ako startnut etcd
kubectl exec etcd-master -n kube-system -- sh -c "ETCDCTL_API=3 etcdctl get / --prefix --keys-only --limit=10 --cacert /etc/kubernetes/pki/etcd/ca.crt --cert /etc/kubernetes/pki/etcd/server.crt  --key /etc/kubernetes/pki/etcd/server.key" 
```

### Kube-api server 16
- hlavny riadiaci komponent
- authentifikuje/autorizuje vsetky poziadavky
- aktualizuje data v etcd
- komunikuje so schedulerom

```kube-api-master```

### Kube controler manager 17

- riadi kontrolery v kluctri
    - sleduje stav
    - riadi situaciu (CRUD)

- node cotroller
    - ping 5 sekund
    - 40 sekung grace period
    - 5min. na navratenie 
- replication-controler
    - sleduje repliky
    - ak pod spadne, tak da vytvorit novy

### Kube-scheduler 18
- planuje start podov na nodoch
- ktory pod sa startne na ktorom node
    - cpu, ram poziadavky
    - labels
    - tains, toleration ...

### Kubelet 19
- nebezi ako pod, ale ako samostatna sluzba na node
- riadi aktivity na node
    - registruje nod do kludtra
    - startuje/vypina pody na node
    - monitoruje stav podu

### Kubeproxy 20
- interna siet klustra pre pody
- iptables rules

### POD 21
- beziaca instancia + imageFS(moze obsahovat viac kontajnerov)

### Pod yaml 22

```yaml
aiVersion: v1
kind: Pod
metadata:
    name: myapp-pod
    labels:
        app: myapp
        type: front-end
spec:
    containers:
      - name: nginx-server
        image: nginx
      - name: busybox
        image: busybox        
```

```bash
kubectl create -f pod.yaml
kubectl get pods
kubectl describe pod pod-name

kubectl run pod nginxpod --image=nginx --dry-run=client  -o yaml 
```

---
poracujeme na 26.


### LAB pod
```bash
kubectl get pods

kubectl get pods -A

kubectl  run nginxpod --image=nginx

kubectl describe pod pod_name           
# pozri image:

kubectl get pods -o wide
# pozri na akom node bezipod

kubectl edit pod pod_name
# zmaz druhy kontainer

kubectl describe pod pod_name
# pozri udalosti preco container nestartuje spravne  "Failed ....."

kubectl delete pod webapp

# 1 pristup
kubectl run redis --image=redis123 --dry-run -o yaml
# druhy, priamo start
kubectl run redis --image=redis123

kubectl edit pod redis
# uprav redis123 -> redis
```

### Replicaset
- stara sa o spravny pocet replik podov v deploymente
```yaml
apiVersion: v1
kind: ReplicationController
metadata:
    labels:
        app: myapp
        type: front-end
spec:
    template:
        metadata:
            name: myapp-pod
            labels:
                app: myapp
                type: front-end
        spec:
            containers:
            - name: nginx-controller
              image: nginx
    replicas: 3                  
```

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
    labels:
        app: myapp
        type: front-end
spec:
    template:
        metadata:
            name: myapp-pod
            labels:
                app: myapp
                type: front-end
        spec:
            containers:
            - name: nginx-controller
              image: nginx
    replicas: 3
    selector:
        matchLabels:
            type: front-end

```
ReplicationController a ReplicaSet sú si podobné v tom, že oba zabezpečujú, aby v clusteri vždy bežal určitý počet podov. ReplicaSet je však novšia a pokročilejšia verzia, ktorá ponúka nasledujúce výhody:

- Pod výber
- Rolling updates
- Scale to zero

```bash
#edituj replica_set.yaml  replicas:6
kubectl replace -f replica_set.yaml

kubectl scale --replicas=6 -f replica_set.yaml          <- cez subor

kubectl scale --replicas=6 replicaset myapp_replica     <-- priamo v k8s
```
## Replicaset LAB

```bash
controlplane ~ ➜  alias k="kubectl"

controlplane ~ ➜  k get pods
No resources found in default namespace.

controlplane ~ ➜  k get rs
No resources found in default namespace.

controlplane ~ ➜  k get rs
NAME              DESIRED   CURRENT   READY   AGE
new-replica-set   4         4         0       12s

# aky image
controlplane ~ ➜  k describe rs new-replica-set
Name:         new-replica-set
Namespace:    default
Selector:     name=busybox-pod
Labels:       <none>
Annotations:  <none>
Replicas:     4 current / 4 desired
Pods Status:  0 Running / 4 Waiting / 0 Succeeded / 0 Failed
Pod Template:
  Labels:  name=busybox-pod
  Containers:
   busybox-container:
    Image:      busybox777   <-------------
    Port:       <none>


# naskaluj
controlplane ~ ➜  k scale --replicas=5 rs new-replica-set
replicaset.apps/new-replica-set scaled

controlplane ~ ➜  k get rs
NAME              DESIRED   CURRENT   READY   AGE
new-replica-set   5         5         5       69s
```

### Deployment  32.
- vyssi stupen abstrakcie ako ReplicaSet, umoznuje rollout, rollback ...

```yaml
apiVersion: apps/v1
kind: Deploment
metadata:
    labels:
        app: myapp
        type: front-end
spec:
    template:
        metadata:
            name: myapp-pod
            labels:
                app: myapp
                type: front-end
        spec:
            containers:
            - name: nginx-controller
              image: nginx
    replicas: 3
    selector:
        matchLabels:
            type: front-end
```

```bash
k get all

kubectl create deployment --image=nginx nginx

kubectl create deployment --image=nginx nginx --dry-run=client -o yaml

kubectl create deployment --image=nginx nginx --replicas=4 --dry-run=client -o yaml > nginx-deployment.yaml



k create deployment httpd-frontend --image=httpd:2.4-alpine  --replicas=3  --dry-run -o yaml
W0121 11:48:32.063373    9436 helpers.go:692] --dry-run is deprecated and can be replaced with --dry-run=client

```

### Services 36.
- vytvara internu IP pre aplikaciu.    
    - nodeport (otvori port na node)
    - clusterIP
    - loadbalancer


#### NodePort
```yaml
apiVersion: v1
kind: Service
metadata:
    name: myapp-service
spec:
    type: NodePort
    ports:
        - targetPort: 80
          port: 80
          nodePort: 30008
    selector:
        app: myapp
        type: front-end
```

### Cluster IP 37.
- interny service v ramci clustra


```yaml
apiVersion: v1
kind: Service
metadata:
    name: back-end
spec:
    type: ClusterIP
    ports:
        - targetPort: 80
          port: 80          
    selector:
        app: myapp
        type: back-end
```

### Loadbalancer 38.
- verejny service v ramci clustra

```yaml
apiVersion: v1
kind: Service
metadata:
    name: back-end
spec:
    type: Loadbalancer
    ports:
        - targetPort: 80
          port: 80          
          nodePort: 30008
    selector:
        app: myapp
        type: back-end
```

kubect create service 

### Namespace 41

```kubectl create ns mamespace_name```
```yaml
apiVersion: v1
kind: Namespace
metadata:
    name: dev
```

```db-service.dev.svc.cluster.local```
- nazov servisu
- namespace
- service
- domena

Prepnutie namespace
```bash
kubectl config set-context $(kubectl config current-context) --namespace dev
```

```yaml
apiVersion" v1
kind: ResourceQuota
metadata:
    name: compute-quota
    namespace: dev
spec:
    hard:
        pods: "10"
        requests.cpu: "4"
        requests.memory: 5Gi
        limits.cpu: "10"
        limits.memory: 10Gi
```

### Imperativ and declarative 44.

```bash
kubectl run nginx --image=nginx

kubectl run nginx --image=nginx --dry-run=client -o yaml

kubectl create deployment --image=nginx nginx

kubectl create deployment --image=nginx nginx --dry-run=client -o yaml

kubectl create deployment nginx --image=nginx --replicas=4

kubectl scale deployment nginx --replicas=4

kubectl create deployment nginx --image=nginx --dry-run=client -o yaml > nginx-deployment.yaml

kubectl expose pod redis --port=6379 --name redis-service --dry-run=client -o yaml

kubectl create service clusterip redis --tcp=6379:6379 --dry-run=client -o yaml

kubectl expose pod nginx --type=NodePort --port=80 --name=nginx-service --dry-run=client -o yaml

kubectl create service nodeport nginx --tcp=80:80 --node-port=30080 --dry-run=client -o yaml


k run redis --image=redis:alpine -l tier=db

k create svc clusterip redis-service --tcp=6379:6379 --dry-run=client -o yaml >svc.yaml

k create deployment webapp --image=kodekloud/webapp-color --replicas=3 --dry-run=client -o yaml > dep.yaml

k create ns dev-ns

 k create deploy redis-deploy --image=redis --replicas=2 -n dev-ns --dry-run=client -o yaml > dep2.yaml

k create svc clusterip httpd --tcp=80:80 --dry-run=client -o yaml >> pod2.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    app: httpd
  name: httpd
spec:
  containers:
  - image: httpd:alpine
    name: httpd
    ports:
      - containerPort: 80
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
---
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    app: httpd
  name: httpd
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: httpd
  type: ClusterIP
```
---

## 3.Scheduling

### Manual scheduling

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    app: httpd
  name: httpd
spec:
  containers:
  - image: httpd:alpine
    name: httpd
    ports:
      - containerPort: 80
  nodeName: node02         <------------
    
```
LAB
```bash
alias=k=kubectl

controlplane ~ ➜  k create -f nginx.yaml
controlplane ~ ➜  k get pods
NAME    READY   STATUS    RESTARTS   AGE
nginx   0/1     Pending   0          32s
```
```kubectl replace --force -f nginx.yaml```         <--------------------- hodi sa

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  containers:
  -  image: nginx
     name: nginx
  nodeName: node01  <------------

```
- binding object
```yaml
apiVersion: v1
kind: Binding
metadata:
    name: nginx
target:
    apiversion: v1
    kind: Node
    name: node02    
```

### Labels and selectors 56

* Label - ozbacuje skupinu komponeentov(tag)
* Selector - umozni vybrat skupinu podla Label (grep)

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx
  labels:
    app: app1
    type: frontend
...    
```

LAB - labels and selectors
```bash

 k get pods -o wide  -A -L env | grep dev


controlplane ~ ➜  k get pods -o wide  -A -L bu
NAMESPACE     NAME                                     READY   STATUS      RESTARTS   AGE    IP           NODE           NOMINATED NODE   READINESS GATES   BU
kube-system   local-path-provisioner-957fdf8bc-ss7f7   1/1     Running     0          12m    10.42.0.2    controlplane   <none>           <none>            
kube-system   coredns-77ccd57875-jh7sf                 1/1     Running     0          12m    10.42.0.6    controlplane   <none>           <none>            
kube-system   helm-install-traefik-crd-jzwv8           0/1     Completed   0          12m    10.42.0.3    controlplane   <none>           <none>            
kube-system   metrics-server-54dc485875-65lmj          1/1     Running     0          12m    10.42.0.5    controlplane   <none>           <none>            
kube-system   helm-install-traefik-br5mg               0/1     Completed   2          12m    10.42.0.4    controlplane   <none>           <none>            
kube-system   svclb-traefik-d1ea3498-q6886             2/2     Running     0          11m    10.42.0.7    controlplane   <none>           <none>            
kube-system   traefik-84745cf649-vbhzs                 1/1     Running     0          11m    10.42.0.8    controlplane   <none>           <none>            
default       app-1-tbs75                              1/1     Running     0          6m1s   10.42.0.9    controlplane   <none>           <none>            finance
default       db-1-zmzqm                               1/1     Running     0          6m     10.42.0.13   controlplane   <none>           <none>            
default       app-1-5xrbk                              1/1     Running     0          6m1s   10.42.0.10   controlplane   <none>           <none>            finance
default       app-1-xwps7                              1/1     Running     0          6m1s   10.42.0.12   controlplane   <none>           <none>            finance
default       auth                                     1/1     Running     0          6m     10.42.0.18   controlplane   <none>           <none>            finance
default       app-1-zzxdf                              1/1     Running     0          6m     10.42.0.19   controlplane   <none>           <none>            finance
default       db-1-4t89s                               1/1     Running     0          6m     10.42.0.15   controlplane   <none>           <none>            
default       db-1-s2bk2                               1/1     Running     0          6m     10.42.0.14   controlplane   <none>           <none>            
default       db-1-lmthz                               1/1     Running     0          6m     10.42.0.17   controlplane   <none>           <none>            
default       db-2-sbdbm                               1/1     Running     0          6m     10.42.0.16   controlplane   <none>           <none>            finance


# poklko komponentov je v prod
k get all --selector env=prod

controlplane ~ ➜  k get pods -o wide  -A -L env | grep prod
default       auth                                     1/1     Running     0          8m50s   10.42.0.18   controlplane   <none>           <none>            prod
default       app-1-zzxdf                              1/1     Running     0          8m50s   10.42.0.19   controlplane   <none>           <none>            prod
default       db-2-sbdbm                               1/1     Running     0          8m50s   10.42.0.16   controlplane   <none>           <none>            prod
default       app-2-ksb2g                              1/1     Running     0          8m51s   10.42.0.11   controlplane   <none>           <none>            prod

controlplane ~ ➜  k get rs  -o wide  -A -L env | grep prod
default       db-2                               1         1         1       9m1s   busybox                  busybox                                   env=prod,tier=db                                                                                             prod
default       app-2                              1         1         1       9m2s   busybox                  busybox                                   !bu,env=prod,tier=frontend                                                                                   prod

controlplane ~ ➜  k get svc  -o wide  -A -L env | grep prod
default       app-1            ClusterIP      10.43.224.155   <none>         3306/TCP                     9m11s   name=app-2                                                                      prod
# vyber prod a finance
controlplane ~ ➜  k get pod -A -o wide -L env -L bu | grep prod | grep finance
default       auth                                     1/1     Running     0          11m   10.42.0.18   controlplane   <none>           <none>            prod   finance
default       app-1-zzxdf                              1/1     Running     0          11m   10.42.0.19   controlplane   <none>           <none>            prod   finance
default       db-2-sbdbm                               1/1     Running     0          11m   10.42.0.16   controlplane   <none>           <none>            prod   finance


k get pods --selector env=dev


```

### Tains and tolerations
- urcuje ktory pod sa kde moze startnut

* tain - oznacenie na workernode
* tolaration - pod toleruje dane oznacenie, moze sa startnut

```bash
k taint nodes node_name key=value:NoSchedule
```
Taint-effect:
- noSchedule
- PrefferNoScedule
- NoExecute

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: nginx-container
    image: nginx
  tolerations:
  - key: "app"
    operation: "Equal"
    value: "blue"
    effect: "NoSchedule"
```

```bash
# pozri taint na master node
k describe node kubemaster | grep Taint
Taints:  node-role.kubernetes.io/master:NoSchedule
```

LAB taints and tolerations

```bash

controlplane ~ ➜  k describe node node01 | grep -i taints
Taints:             <none>

controlplane ~ ✖ k taint node node01 spray=mortein:NoSchedule
node/node01 tainted

controlplane ~ ➜  k describe node node01 | grep -i taints
Taints:             spray=mortein:NoSchedule


controlplane ~ ➜  k replace --force  -f pod.yaml 
pod "mosquito" deleted
pod/mosquito replaced

controlplane ~ ➜  k get pods
NAME       READY   STATUS    RESTARTS   AGE
mosquito   1/1     Running   0          2s

controlplane ~ ➜  cat pod.yaml 
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: mosquito
  name: mosquito
spec:
  containers:
  - image: nginx
    name: mosquito
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
  tolerations:
    - key: "spray"
      value: "mortein"
      operator: "Equal"
      effect: "NoSchedule"
status: {}



controlplane ~ ➜  k describe node controlplane | grep -i taint
Taints:             node-role.kubernetes.io/control-plane:NoSchedule

# odstran taint
controlplane ~ ✖ k taint node controlplane node-role.kubernetes.io/control-plane:NoSchedule-
node/controlplane untainted

```

### NodeSelector 62.

- 

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: nginx-container
    image: nginx
  nodeSelector:
    size: Large        <----------------- podla label na node    
```
Label node
``` k label node node1 size=Large```

### Node affinity 63.
- dplnkove planovanie nodu

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: nginx-container
    image: nginx
  nodeAffinity:
    requiredDuringSchedulingIngnoreDuringExecution:
      nodeSelectorTerms:
      - matchExpressions:
        - key: size
          operator: In
          value:
          - Large
          - Medium
```
Node affinity LAB

```bash
# kolo labelov ma node01
controlplane ~ ➜  k describe node node01 | grep -i -A 5 label
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/os=linux
                    kubernetes.io/arch=amd64
                    kubernetes.io/hostname=node01
                    kubernetes.io/os=linux

# sprav label color=blue
k label node node01 color=blue
```
```bash
# Create a new deployment named blue with the nginx image and 3 replicas.
controlplane ~ ➜  k create deployment blue --image=nginx --replicas=3 --dry-run=client -o yaml
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: blue
  name: blue
spec:
  replicas: 3
  selector:
    matchLabels:
      app: blue
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: blue
    spec:
      containers:
      - image: nginx
        name: nginx
        resources: {}
status: {}
```

```bash
# Which nodes can the pods for the blue deployment be placed on?
controlplane ~ ➜  k get node -o wide -L color
NAME           STATUS   ROLES           AGE   VERSION   INTERNAL-IP     EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION   CONTAINER-RUNTIME    COLOR
controlplane   Ready    control-plane   16m   v1.27.0   192.33.183.12   <none>        Ubuntu 20.04.6 LTS   5.4.0-1106-gcp   containerd://1.6.6   
node01         Ready    <none>          15m   v1.27.0   192.33.183.3    <none>        Ubuntu 20.04.5 LTS   5.4.0-1106-gcp   containerd://1.6.6   blue

# Which nodes can the pods for the blue deployment be placed on?
# Make sure to check taints on both nodes!

controlplane ~ ➜  k get pods -o wide
NAME                    READY   STATUS    RESTARTS   AGE     IP           NODE           NOMINATED NODE   READINESS GATES
blue-5d9664c795-75bw4   1/1     Running   0          2m29s   10.244.1.2   node01         <none>           <none>
blue-5d9664c795-fl57f   1/1     Running   0          2m29s   10.244.0.4   controlplane   <none>           <none>
blue-5d9664c795-hb6vz   1/1     Running   0          2m29s   10.244.1.3   node01         <none>           <none>
```
pridaj afinity
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: blue
  name: blue
spec:
  replicas: 3
  selector:
    matchLabels:
      app: blue
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: blue
    spec:
      containers:
      - image: nginx
        name: nginx
        resources: {}
      affinity:
          nodeAffinity:
            requiredDuringSchedulingIgnoredDuringExecution:
              nodeSelectorTerms:
              - matchExpressions:
                 - key: color
                   operator: In
                   values:
                   - blue
status: {}
```

Create a new deployment named red with the nginx image and 2 replicas, and ensure it gets placed on the controlplane node only.
Use the label key - node-role.kubernetes.io/control-plane - which is already set on the controlplane node.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: red
  name: red
spec:
  replicas: 2
  selector:
    matchLabels:
      app: red
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: red
    spec:
      containers:
      - image: nginx
        name: nginx
        resources: {}
      affinity:
          nodeAffinity:
            requiredDuringSchedulingIgnoredDuringExecution:
              nodeSelectorTerms:
              - matchExpressions:
                 - key: node-role.kubernetes.io/control-plane
                   operator: Exists
status: {}
```

### Tains and toleration vs. Node affinity

todo : precivit

### Resource requirements aand limits 67.

```yaml
...
spec:
    resources:
        requests:             <-- minimalne poziadavky
            memory: "4Gi"
            cpu: 100m
        limits:               <--- maximalne povoleme>
            memory: "6Gi"           <-- kill ak prekona  (out of memory kill)
            cpu: 1                  < ---cpu trotling ak dosiahne
```


LimitRange
```yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: cpu-resource-constraint
spec:
  limits:
  - default: # this section defines default limits
      cpu: 500m
    defaultRequest: # this section defines default requests
      cpu: 500m
    max: # max and min define the limit range
      cpu: "1"
    min:
      cpu: 100m
    type: Container
```

[resource quota](https://kubernetes.io/docs/concepts/policy/resource-quotas/)


LAB
```bash

controlplane ~ ➜  k get pods
NAME       READY   STATUS      RESTARTS   AGE
elephant   0/1     OOMKilled   0          5s


```

### DeamonSet 71.
- pod beziaci na kazdom node
- vacsinou sluzi na servisne ucely
    - moniting
    - networking

```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: fluentd-elasticsearch
  namespace: kube-system
  labels:
    k8s-app: fluentd-logging
spec:
  selector:
    matchLabels:
      name: fluentd-elasticsearch
  template:
    metadata:
      labels:
        name: fluentd-elasticsearch
    spec:
      containers:
      - name: fluentd-elasticsearch
        image: quay.io/fluentd_elasticsearch/fluentd:v2.5.2
```
```kubectl get ds```

Daemonset LAB
```bash

controlplane ~ ➜  k get ds -A
NAMESPACE      NAME              DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
kube-flannel   kube-flannel-ds   1         1         1       1            1           <none>                   97s
kube-system    kube-proxy        1         1         1       1            1           kubernetes.io/os=linux   100s

```
### Static POD 74.
- samostatne startnuty pod
- funguje aj bez master nodu
- definicia /etc/kubernetes/manifests/ (pod-manifest-path)
    - pod1.yaml
    - pod2.yaml
- vidno ho ```docker ps```


Static POD LAB
```bash

kubectl run --restart=Never --image=busybox static-busybox --dry-run=client -o yaml --command -- sleep 1000 > /etc/kubernetes/manifests/static-busybox.yaml

kubectl run --restart=Never --image=busybox:1.28.4 static-busybox --dry-run=client -o yaml --command -- sleep 1000 > /etc/kubernetes/manifests/static-busybox.yaml
```

### Multiple schedulers 77.
TODO: Multiple schedulers

### Scheduler profile
```
https://github.com/kubernetes/community/blob/master/contributors/devel/sig-scheduling/scheduling_code_hierarchy_overview.md
https://kubernetes.io/blog/2017/03/advanced-scheduling-in-kubernetes/
https://jvns.ca/blog/2017/07/27/how-does-the-kubernetes-scheduler-work/
https://stackoverflow.com/questions/28857993/how-does-kubernetes-scheduler-work
```


### Logging and Monitoring 84.
- metrics server (in memory)
  - kubelet
  - node monitoring
  - pod monitoring
```bash
controlplane ~ ➜  k top node
NAME           CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%   
controlplane   267m         0%     1141Mi          0%        
node01         57m          0%     296Mi           0%        

controlplane ~ ➜  k top pod
NAME       CPU(cores)   MEMORY(bytes)   
elephant   14m          31Mi            
lion       1m           18Mi            
rabbit     107m         178Mi  


TODO: odskusat ./kubectl top po -n kube-system --sort-by=cpu                                                                             [ruby-2.3.7p456]
NAME                               CPU(cores)   MEMORY(byt
```

### Managing application logs


```
kubectl logs -f pod_name


kubectl logs -f pod_name container_name   <--- ak je v pode viac kontainerov
```


### Application LifeCycke management 90.


### Rolling update 92.

```bash
kubectl rollout status deplyment/myapp

kubectl rollout history

# recreate - downtime during rollout
# destroy / create all

# rolling update
# down/up 

# rollback
kubectl rollout undo deployment/myapp

kubectl


```
### Configuration

```bash
docker run ubuntu

docker ps -a
```

```
FROM ubuntu
CMD sleep 5
```


docker run ubuntu-sleeper 10
```
FROM ubuntu
ENTRYPOINT ["sleep"]
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: ubuntu-sleeper
spec:
  containers:
    - name: ubuntu-sleeper
      image: ubuntu-sleeper
      args: ["10"]
```

### Config env. 100.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: ubuntu-sleeper
spec:
  containers:
    - name: ubuntu-sleeper
      image: ubuntu-sleeper
      args: ["10"]
      env:
        - name:  APP_COLOR    <---------- key value
          value: pink

        - name: APP_COLOR
          valueFrom: 
            configMapKeyRef:

        - name: APP_COLOR
          valueFrom:
            secretKeyRef:

```
### ConfigMap
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: app-config
data:
  APP_COLOR: blue
  APP_MODE: prod
```

Imperativny sposob
```bash
kubectl create configmap name --from-literal=APP_COLOR=blue --from-literal=APP_MOD=prod

kubectl create configmap name --from-file=app_config.properties


app_config:
  APP_COLOR: blue
  APP_MODE: prod


kubectl get configmaps  

kubectl describe configmaps  name

```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: ubuntu-sleeper
spec:
  containers:
    - name: ubuntu-sleeper
      image: ubuntu-sleeper
      args: ["10"]
      envFrom:
        - configMapRef:
              name: app-config             <- vsetky hodnoty z configmap
      # alebo
      env:
        - name: APP_COLOR
          valueFrom: 
            configMapKeyRef:
              name: app-config
              key: APP_COLOR            <- namapuje ednu hodnotu

```

```bash
k create cm webapp-config-map --from-literal=APP_COLOR=darkblue --from-literal=APP_OTHER=disregard
```

TODO: create configmap precvicit, imperativny pristup


### Secrets 104.

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: app-secret
data:
  login: (base64 encoded)    <===  echo -n "taje heslo" | base64
  password: 
```

```bash
k create secret generic secret_name --from-literal=key=value

k create secret generic secret_name --from-file=app_config.properties


app_config
login: root
password: tajneheslo



k get secrets
k describe secrets
k get secret app-sercer -o yaml

# dekoduj
 echo -n "2342342332423=" | base64 --decode

```
```yaml

      envFrom:
        - secretRef:
              name: app-config             <- vsetky hodnoty z configmap
      # alebo
      env:
        - name: APP_COLOR
          valueFrom: 
            secretKeyRef:
              name: app-config
              key: APP_COLOR            <- namapuje ednu hodnotu

  volumes:
  - name: app-secret
    secret:
      secretName: app-secret
```

### Scale application 109.

### MultiContainer Pod .


```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: pod
  name: yellow
spec:
  containers:
  - image: busybox
    name: lemon
    command: ['sleep','3600']
  - image: redis
    name: gold
```

```   k exec pod/app -n elastic-stack -- cat /log/app.log```

### MultiContainer Pod architecture
- init container
- sidecar


## Cluster maintanance

### OS Upgrade

drain - vyprazdni node
cordon / uncordon - zakaze/povoli start novych podov



k cordon node01
k drain node01 --ignore-daemonsets



### k8s upgrade kubeadm

- master node
- worker node

```bash
# master node
apt-get upgrade -y kubeadm=1.12.0-00
kubeadm  upgrade apply v1.12.0
apt-get upgrade -y kubelet=1.20.0-00
systemctl restart kubelet

k uncordon controlplane

# worker node
kubect drain node-1
apt-get upgrade -y kubeadm=1.27.0-00
# apt-get install kubeadm=1.27.0-00

apt-get upgrade -y kubelet=1.27.0-00
# apt-get install kubelet=1.27.0-00 
kubeadm upgrade node config --kubelet-version v1.27.0
systemctl daemon-reload
systemctl restart kubelet

kubect uncordon node-1
```

### Backup restore
TODO: natrenovat backup/restore s parametrami
- vzdy treba ETCDCTL_API=3
- --endpoints=https://127.0.0.1:2379


- resource configuration
  ```kubectl get all -A -o yaml > all_deployment.yaml```
- etcd
  /var/lib/etcd
```bash
#
# vyber parametre pre etcd na master node
#
ps -ef | grep etcd
# --cacert
# --cert
# --endpoints
# --key-file


ETCDCTL_API=3 etcdctl --endpoints=https://127.0.0.1:2379 --cacert=...   --cert=.... --key-file=   snapshot save snapshot.db



ETCDCTL_API=3 etcdctl snapshot status snapshot.db
#
# restore
#
systemctl stop etcd.service

ETCDCTL_API=3 etcdctl snapshot restore snapshot.db  --data-dir /var/lib/etcd 

# nastav spravneho vlastnika
chown -R etcd:etcd /var/lib/etcd 


systemctl daemon-reload
systemctl restart etcd.service
systemctl start kube-apiserver


https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/#backing-up-an-etcd-cluster
https://github.com/etcd-io/website/blob/main/content/en/docs/v3.5/op-guide/recovery.md
https://www.youtube.com/watch?v=qRPNuT080Hk
```
  ### cvicenie
  ```bash
  export  ETCDCTL_API=3


  ETCDCTL_API=3 etcdctl --endpoints=https://192.14.236.3:2379 \
                      --cacert=/etc/kubernetes/pki/etcd/ca.crt \
                      --cert=/etc/kubernetes/pki/etcd/server.crt \
                      --key=/etc/kubernetes/pki/etcd/server.key \
                      snapshot save  /opt/snapshot-pre-boot.db


ETCDCTL_API=3 etcdctl snapshot restore  /opt/snapshot-pre-boot.db --data-dir=/var/lib/etcd

# updatni manifest /etc/kubetnets/manifes/etcd.yaml
# adresar /var/lib/etcd   -> /var/lib/etcd_from_backup
# restartni etcd pod

```

### cvicenie 2
TODO precvicit backup/restore etcd
```


```


### Security 138.

### Security primitives 140.

### Authetification
- identifikovanie usera ci ma pristup, kto to je




### Authorisation
- opravnenie co moze user robit

users.csv
```csv
passwd, user, u0001
passwd, user, u0001
passwd, user, u0001
passwd, user, u0001
```

### TLS v kubernetes 143.
- garantuje doveru v komunikacii

Certifikaty:
- root
- klient
- server

Typy certifikatov :
- cetifikate(public key)  *.crt *.pem
- private key  *.key *-key.pem

#### Serverove komponenty
kube-api server ---> etcd server
- api.crt
- api.key

etcd server(CA)   --> kubelet server
- etcdserver.crt
- etcdserver.key

kubelet server
- kubelet.crt
- kubelet.key

#### Klient komponenty komunikuju cez kubectl rest api

admin user
- admin.crt
- admin.key

scheduler
- scheduler.crt
- cheduler.key

kube contrller- manager
- controller-manager.crt
- controller-manager.key

kube-proxy
- kube-proxy.crt
- kube-proxy.key

Certifikacna autorika clustra:
- ca.crt
- ca.key

<<<<<<< HEAD
## Certificate API 151.
- certifikacna autorita kubernetesu
- sluzi na tvormnu user certifikatov

```bash
# vugeneruj kluc
openssl genrsa -out jave.key 2048
# vygeneruj CSR (request)
openssl req -new -key jane.key -subj "/CN=jane" -out jane.csr
#
cat jane.csr | base64                    <-- 


apiversion: certificates.k8s.io/v1
kined: CertificateSigningRequest
metadata:
  name: jane
spec:
  expirationSeconds: 600
  usages:
  - digital signature
  - key encipherment
  - server auth
  request:
      data z cat jane.csr | base64 


kubectl get csr

kubectl certificate approve jane

```

LAB
```bash
cat akshay.csr | base64 -w 0

---
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: akshay
spec:
  groups:
  - system:authenticated
  request: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURSBSRVFVRVNULS0tLS0KTUlJQ1ZqQ0NBVDRDQVFBd0VURVBNQTBHQTFVRUF3d0dZV3R6YUdGNU1JSUJJakFOQmdrcWhraUc5dzBCQVFFRgpBQU9DQVE4QU1JSUJDZ0tDQVFFQTFuTFl2NzlzQ3FrNFlNd0lJMUJ2K0pYUnhyMkMxMUtjc0U0WG9ndllzaFhTClBWbFExbVpObUwvVTJDNkNtVHJBY21HZ2w5Zjh4QjZMRDV1QkhEVndYZ21QTitweFpyOGl6RytNcUpjTVlidU0KanpWRVhHZ3NQbGwwN1JCZTd5eDl2TU1nU05IN0UxVUNibk9tOTJpc1F1UGhrb1Z3WEZuMWFEZWJCWElXM1FKYgpoNW1TU25janF3SVBXN2pocDVZa054VGVncHRLRzNkUEI1MzJVcmV2MGtreGtESkZJci9RYXZmaDArYW84bHJ2CisyUGFOb0VQMG0wU3VTbGJzT2xhSzRKdEhtSzFHYTVjUXJWcE5EYUcwcmZWVTdZa2l3NWFvZUJXYkpBcHZGSFUKcVg5Z0dPZHk4eSt5anNwQUdTeWVuVTUrREFtTnNDcnJ0K28yaitxMi93SURBUUFCb0FBd0RRWUpLb1pJaHZjTgpBUUVMQlFBRGdnRUJBQUFzblBTdW91a0pvK2lKcEgybzJJc1VaejdaQVR5Wlgwd2YwZkpTZUhTaHUrbVBXTXFDCnQ0VVZ0dExod0c5YnNZWW1PM1J0VHBtT3lrQmNOYTEvQlVZUGxqdDJUeEc3N2M2Y3JDUXBwaXpaWFI1MlVuQXMKQ0ZYTkxoRWltT2NyOUtKY3NEQnYyUEUwQk0yczRmbGlybmN0QUNCT3ZnUUk4MWM4bjU5R3BBa2FwTkVTZEdpYwpSQngzSkNzeXFyUldjYU4vMll1UWlSUXJ0TlA3ZnlIYzczdEg3M1IxcGNFTmVlNGptSkJlYktNWlgvVGRMU1hmClFNbk14MnJBRWFZVnJJekVvb3dPaUVKU0hGTFZSaGpBRzQ0bkVpRHFaUkx6WWJiZXpwYnJIc21XQ3c2VXphQmMKM2lZU2I1Szc1MzVFd1ZzQnhGODNjSEN2MU9PYmhDNWhyVUk9Ci0tLS0tRU5EIENFUlRJRklDQVRFIFJFUVVFU1QtLS0tLQo=
  signerName: kubernetes.io/kube-apiserver-client
  usages:
  - client auth


kubectl apply -f akshay-csr.yaml

controlplane ~ ✖ k get csr
NAME        AGE     SIGNERNAME                                    REQUESTOR                  REQUESTEDDURATION   CONDITION
akshay      44s     kubernetes.io/kube-apiserver-client           kubernetes-admin           <none>              Pending
csr-qtcgr   8m44s   kubernetes.io/kube-apiserver-client-kubelet   system:node:controlplane   <none>              Approved,Issued


controlplane ~ ✖ k certificate  approve akshay
certificatesigningrequest.certificates.k8s.io/akshay approved

controlplane ~ ➜  k get csr
NAME          AGE     SIGNERNAME                                    REQUESTOR                  REQUESTEDDURATION   CONDITION
agent-smith   16s     kubernetes.io/kube-apiserver-client           agent-x                    <none>              Pending
akshay        2m43s   kubernetes.io/kube-apiserver-client           kubernetes-admin           <none>              Approved,Issued
csr-qtcgr     10m     kubernetes.io/kube-apiserver-client-kubelet   system:node:controlplane   <none>              Approved,Issued

controlplane ~ ✖ k get csr agent-smith -o yaml
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  creationTimestamp: "2024-02-16T12:23:14Z"
  name: agent-smith
  resourceVersion: "1260"
  uid: 856dada2-45db-4e4f-9263-a3b7244998ed
spec:
  groups:
  - system:masters
  - system:authenticated

controlplane ~ ➜  k certificate deny agent-smith
certificatesigningrequest.certificates.k8s.io/agent-smith denied

controlplane ~ ✖ k get csr 
NAME          AGE     SIGNERNAME                                    REQUESTOR                  REQUESTEDDURATION   CONDITION
agent-smith   4m8s    kubernetes.io/kube-apiserver-client           agent-x                    <none>              Denied
akshay        6m35s   kubernetes.io/kube-apiserver-client           kubernetes-admin           <none>              Approved,Issued
csr-qtcgr     14m     kubernetes.io/kube-apiserver-client-kubelet   system:node:controlplane   <none>              Approved,Issued

controlplane ~ ➜  k delete csr agent-smith
certificatesigningrequest.certificates.k8s.io "agent-smith" deleted

```
## Kubeconfig 154
.kube/config
```yaml
apiVersiom: v1
kind: Config
current-context: my-admin@my-kube-playground
clusters:
  - name: my-kube-playground
    cluster:
      certificate-authoroty: ca.crt
      server: https://my-kube:6443
Contexts:
  - name: my-admin@my-kube-playground
    context:
      cluster: my-kube-playground
      user: my-admin
      namespace: testovacia

Users:


```

```kubectl config view```

```kubectl use-context pro-user@production```    prepnutie contextu


LAB:
```bash
=======
### Tvorba certifikatov

```bash
# vygeneruj privatny kluc pre CA
openssl genrsa -out ca.key 2048
# CRS certificate signing request
openssl req -new -key ca.key -subj "/CN=Kubernetes-ca" -out ca.csr
# podpis certifikat pomocou privatneho kluca ca.key
openssl x509 -req -in ca.csr -signkey ca.key -out ca.crt

# admin user
openssl genrsa -out admin.key 2048
# request
openssl  req -new -key admin.key -subj "/CN=kube-admin/O=system:masters" -out admin.csr
# podpis certifikat
openssl x509 -req -in admin.csr -CA ca.crt -CAkey ca.key -out admin.crt





```








### Network policy
- nastavenie siete v k8s
  - kube-router
  - calico
  - Romana
  - weve-net
- ingress
- egress

```yaml
policyTypes:
- Ingress
ingress:
- from:
  - podSelector:
      mathcjLabels:
        name: api-pod
  ports:
  - protocol: TCP
    port: 3306    
```

```yaml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: db-policy
spec:
  podSelector:
    matchLabels:
      role: db
  policyTypes:
  - Ingress
  ingress:
  - from:
    - podSelector:
        match:abels:
          name: api-pod
    ports:
    - protocol: TCP
      port: 3306
```

## Storage 182.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: myfrontend
      image: nginx
      volumeMounts:
      - mountPath: "/var/www/html"
        name: mypd
  volumes:
    - name: mypd
      persistentVolumeClaim:
        claimName: myclaim
```


# Networking 203.

## Network Namescapce  203.
- virtual netwoking
```bash
ip link set veth-red netns red

ip link add v-net ...

netstat -nplt | grep sched
```

## Pod networking 221.


## DNS 223
- v ramci jedneho NS staci nazov service
- z druheho NS treba web-service.apps.svc.cluster.local   <<-- FQDN
  - apps je namespace pre web

## Core DNS
- kube-dns  << globalny dns pre cely cluster
- /etc/coredns/Corefile 
- 
```bash

# Kubeadm

The vagrant file used in the next video is available here:

https://github.com/kodekloudhub/certified-kubernetes-administrator-course

Here's the link to the documentation:

https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/

here: https://uklabs.kodekloud.com/topic/practice-test-cluster-installation-using-kubeadm/
https://www.youtube.com/watch?v=-ovJrIIED88&list=PL2We04F3Y_41jYdadX55fdJplDvgNGENo&index=18


# Troubleshooting

## Application
- nakreslit mapu komponentov ako na sebe zavisia
  - ingress 
    - na akom hoste, pathe pocuva
    - kam smeruje (service:port)
  - service 
    - na akomporte pocuva
    - kam smeruje (selector, spravny port)
  - deployment app (pozri logs a describe)
    - aky exportovany port ?
    - pouziva paramatre ?
      - env
      - secret
      - configmap
    - ak vyuziva iny servis
      - spravna url, port ??
  - config map
  - stoprage


## Control palne
```bash
k get pods
k get nodes
# master node
service kube-apiserver status
service kube-controller-manager status
service kube-scheduler status
# worker node
service kubelet status
service kubeproxy status

kubectl logs kube-apiserver-master -n kube-system
sudo joutnatctl -fu kube-apiseerver

# worker node
top
df -h
free
```
 ## kubelet
 ```bash
 cd /var/lib/kubelet/
 # tu su konfuguraky tykajuce sa wrokernodu
 # a kubelet
 ```

# Json Path
 
- umoznuje vystup vo formate json a parsovat podobne ako ```jq```.

```
semicolon   - bodkočiarka
colon   - dvojbodka
comma   - čiarka
hyphen  - pomlcka

```bash
kubectl get node -o json

```

```

