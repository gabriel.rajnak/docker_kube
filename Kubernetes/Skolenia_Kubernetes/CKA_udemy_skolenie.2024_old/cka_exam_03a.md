# Cka test 3

# 1. Create a new service account with the name pvviewer. Grant this Service account access to list all PersistentVolumes in the cluster by creating an appropriate cluster role called pvviewer-role and ClusterRoleBinding called pvviewer-role-binding.
Next, create a pod called pvviewer with the image: redis and serviceAccount: pvviewer in the default namespace.

```
ServiceAccount: pvviewer
ClusterRole: pvviewer-role
ClusterRoleBinding: pvviewer-role-binding
Pod: pvviewer
Pod configured to use ServiceAccount pvviewer ?
```
```bash

controlplane ~ ➜  kubectl create serviceaccount pvviewer
serviceaccount/pvviewer created

controlplane ~ ➜  k get sa
NAME       SECRETS   AGE
default    0         37m
pvviewer   0         4s

# ako 
kubectl create clusterrole pvviewer-role --verb=list  --resource=persistentvolume
clusterrole.rbac.authorization.k8s.io/pvviewer-role created


controlplane ~ ✖ kubectl create  clusterrolebinding  pvviewer-role-binding --clusterrole=pvviewer-role --serviceaccount=defualt:pvviewer
clusterrolebinding.rbac.authorization.k8s.io/pvviewer-role-binding created


controlplane ~ ➜  cat pod.yaml 
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: pvviewer
  name: pvviewer
spec:
  serviceAccountName: pvviewer
  containers:
  - image: redis
    name: pvviewer
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
 ```
 ---
 # 2.List the InternalIP of all nodes of the cluster. Save the result to a file /root/CKA/node_ips.


Answer should be in the format: InternalIP of controlplane<space>InternalIP of node01 (in a single line)

```
 k get nodes -o wide | grep Ready | awk '{print $6 }'  > /root/CKA/node_ips



 Solution:
 Explore the jsonpath loop.
kubectl get nodes -o jsonpath='{.items[*].status.addresses[?(@.type=="InternalIP")].address}' > /root/CKA/node_ips

 ```

 # 3.Create a pod called multi-pod with two containers.
Container 1: name: alpha, image: nginx
Container 2: name: beta, image: busybox, command: sleep 4800

```
Environment Variables:
container 1:
name: alpha

Container 2:
name: beta

Pod Name: multi-pod
Container 1: alpha
Container 2: beta
Container beta commands set correctly?
Container 1 Environment Value Set
Container 2 Environment Value Set
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: multi-pod
  name: multi-pod
spec:
  containers:
  - image: nginx
    name: alpha
    env:
    - name: name
      value: "alpha"
  - image: busybox
    name: beta
    command: [ "sh","-c", "sleep 4800" ]
    env:
    - name: name
      value: "beta"
```

# 4. Create a Pod called non-root-pod , image: redis:alpine

runAsUser: 1000
fsGroup: 2000

Pod non-root-pod fsGroup configured
Pod non-root-pod runAsUser configured
```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: non-root-pod
  name: non-root-pod
spec:
  securityContext:
    runAsUser: 1000
    fsGroup: 2000
  containers:
  - image: redis:alpine
    name: non-root-pod
    resources: {}
      #    securityContext:
      #     allowPrivilegeEscalation: false
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

# 5. We have deployed a new pod called np-test-1 and a service called np-test-service. Incoming connections to this service are not working. Troubleshoot and fix it.
Create NetworkPolicy, by the name ingress-to-nptest that allows incoming connections to the service over port 80.


Important: Don't delete any current objects deployed.
Important: Don't Alter Existing Objects!
NetworkPolicy: Applied to All sources (Incoming traffic from all pods)?
NetWorkPolicy: Correct Port?
NetWorkPolicy: Applied to correct Pod?
