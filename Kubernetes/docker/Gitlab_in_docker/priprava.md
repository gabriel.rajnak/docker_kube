# Priprava Gitlab-u v lokalnom kubernetes



        docker volume create  gitlab_data


        export gitlabdata="/Users/bank365/docker_data/gitlab_data/"

        docker run -d -p 7443:443 -p 7780:80 -p 7722:22 \
        --name gitlab \
        --cpus 4 \
        --restart always \
        --volume $gitlabdata/config:/etc/gitlab \
        --volume $gitlabdata/logs:/var/log/gitlab \
        --volume $gitlabdata/data:/var/opt/gitlab \
        gitlab/gitlab-ce:latest


docker volume create gitlab-runner-config

docker run --name gitlab-runner -d --rm -t -i gitlab/gitlab-runner

docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest
