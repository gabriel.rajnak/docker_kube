# Poznamky
<https://www.markdownguide.org/basic-syntax/>

## TEST

### MOJE



### Priprav Dockerfile
```
FROM nginx:latest
COPY index.html /usr/share/nginx/html
```

### Vybuilduj docker image
docker build -t moj_web .

### Start docker image
docker run -d -p 80:80 moj_web


```
bank365@MacBookPro docker % docker ps
CONTAINER ID   IMAGE                                    COMMAND                  CREATED          STATUS          PORTS                                                      NAMES
344e828a535f   moj_web                                  "/docker-entrypoint.…"   25 seconds ago   Up 24 seconds   0.0.0.0:80->80/tcp                                         eager_meitner
18cda64be850   gitlab/gitlab-runner:latest              "/usr/bin/dumb-init …"   5 weeks ago      Up 2 minutes                                                               gitlab-runner
c9599968f097   cr.portainer.io/portainer/portainer-ce   "/portainer"             2 months ago     Up 2 minutes    0.0.0.0:8000->8000/tcp, 0.0.0.0:9443->9443/tcp, 9000/tcp   portainer
bank365@MacBookPro docker % docker exec -it  344e828a535f /bin/bash
```

