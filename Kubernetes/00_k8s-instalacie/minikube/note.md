# Kubernetes
## miniKube

```
minikube start
```

kubectl get all

kubectl get pods
```
bank365@MacBookPro docker % kubectl get pods
NAME                          READY   STATUS    RESTARTS       AGE
fs-example-6967c76cfb-gzxs9   1/1     Running   3 (118s ago)   52d
```


```
bank365@MacBookPro docker % kubectl get ns
NAME              STATUS   AGE
default           Active   52d
kube-node-lease   Active   52d
kube-public       Active   52d
kube-system       Active   52d
bank365@MacBookPro docker % 
```

kubectl apply -f file.yml


