#!/bin/bash
# https://dev.to/arc42/enable-ssh-access-to-multipass-vms-36p7
# https://medium.com/@ahmadb/using-multipass-with-cloud-init-bc4b92ad27d9
# namet https://medium.com/@theko2fi/manage-your-multipass-vms-with-ansible-a84cdd7bcbe8

ram="2G"
cpu=2
image="23.10"
disk="10G"

function create_vm()
{
multipass list
echo "Starujem virtualky"
for vm in `grep -v \# hosts.list`; do
    echo $vm
    multipass launch -n $vm -c $cpu -m $ram -d $disk $image --cloud-init cloud-init.yaml
    multipass transfer  install.sh $vm:/home/ubuntu/
    #multipass transfer 
    #multipass transfer 
    multipass exec $vm -- bash /home/ubuntu/install.sh
    multipass exec $vm -- sudo reboot
done
}

function start_vm()
{
for vm in `grep -v \# hosts.list`; do
    echo $vm
    multipass start $vm
done
}

function stop_vm()
{
for vm in `grep -v \# hosts.list|tac`; do
    echo $vm
    multipass stop $vm
done
}

function delete_vm()
{
for vm in `grep -v \# hosts.list|tac`; do
    echo $vm
    multipass stop $vm
    multipass delete $vm
done

multipass purge
}

function list_vm()
{
    multipass list
}
# ===========================================================

case $1 in

create)
    echo "create"
    create_vm
    list_vm
    ;;
start)
    echo "start"
    start_vm
    list_vm
    ;;
stop)
    echo "stop"
    stop_vm
    list_vm
    ;;
delete)
    echo "delete"
    delete_vm
    list_vm
    ;;
*)
    echo "help"
    echo "---------------------------------------------------"
    echo "mp.sh create"
    echo "mp.sh start"
    echo "mp.sh stop"
    echo "mp.sh delete"
    echo "---------------------------------------------------"
esac

rm -f list

