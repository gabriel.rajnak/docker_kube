# Local installation K8s

```bash
sudo hostname set-hostnaame k8s-control
sudo hostname set-hostnaame k8s-worker1
sudo hostname set-hostnaame k8s-worker2
```

/etc/hosts
```
192.168.1.21 k8s-control
192.168.1.22 k8s-worker1
192.168.1.23 k8s-worker2
```

### Install containerd na MASTER node

### Moduly
```bash
/etc/modules-load.d/containerd.conf
overlay
br_netfilter
EOF


sudo modprobe overlay
sudo modprobe br_netfilter


/etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-ip6tanles = 1

sudo sysctl --system 
```
## Containerd

```bash
sudo apt-get update && sudo apt-get install -y containerd


sudo mkdir /etc/containerd
sudo containerd config default | sudo tee /etc/containerd/config.toml

sudo systemctl restart containerd

sudo swapoff -a
sudo apt-get update && sudoapt-get install -y apt-transport-https curl 
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

cat << EOF | sudo tee /etc/apt/source.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF

sudo apt-get update

sudo apt-get install -y kubelet=1.27.0 kubeadm=1.27.0-00 kubectl=1.27.0-00
sudo apt-get install -y kubelet kubeadm kubectl

sudo apt-mark hold kubelet kubeadm kubectl


```

# Install na worker node

- moduly
- containerd

## Inicializacia k8s na master node
```bash

sudo kubeadm init --pod-network-cidr=192.168.64.0/24 --kubernetes-version=1.28.2


mkdir ~/.kube
sudo cp -i /etc/kubernetes/admin.conf ~/.kube/config
sudo chown $(id -u):$(id -g) ~/.kube/config

kubectl get nodes

kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml

```
## Pripoj workernody do clustra

```bash
# vygeneruj token na mastri
kubeadm token create --print-join-command
.....

# na worker node 1 a 2
kubeadm join 192.168.1.21 --token ..... 
# pockaj chvilu

```

kubeadm join 192.168.64.96:6443 --token t19mj7.bttnqx11kdjnorm7 \
	--discovery-token-ca-cert-hash sha256:be678b275b5cce57ce07f7d09fe434287b5306268f4b2634ca9337c4b55324c8 

# get cotext 
        