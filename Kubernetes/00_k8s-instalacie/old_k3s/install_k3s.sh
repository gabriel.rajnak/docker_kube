#!/bin/bash

apt -qq -y update
apt -qq -y upgrade
# Disable traefik
# export INSTALL_K3S_EXEC="server --no-deploy traefik"

# Create k3s cluster
curl -sfL https://get.k3s.io | sh -s -

