#!/bin/bash

servername=k3s-demo
image=22.04
ram=2G
storage=5G
token="$PWD/$servername.yaml"
# multipass set local.driver=hyperkit
# -----------------------------------------------------
clear
echo "************************************"
echo "Priprava kubernetes virtualky"
echo "************************************"
echo "Hostname : $servername"
status=`multipass list | grep $servername`
echo $status
if [ ! -z "$status" ]; then
    echo "Virtualka $servername uz existuje. Nastartujem ju."
    read a
    multipass start $servername
    multipass list | grep $servername

else
# -----------------------------------------------------    
#export INSTALL_K3S_EXEC="server --no-deploy traefik"
# Create k3s cluster
#curl -sfL https://get.k3s.io | sh -s -


    echo "Pre pokracovanie stlac Enter alebo Ctrl+C"
    read a 
    multipass launch -c 2 -n $servername -m $ram -d $storage $image
    # multipass exec $servername -- sudo apt -y update 
    # multipass exec $servername -- sudo apt -y upgrade 
    # echo "Instalujem k3s"
    # multipass exec $servername -- curl -sfL https://get.k3s.io -o /home/ubuntu/k3s.sh
    # # multipass exec $servername -- /bin/bash /home/ubuntu/k3s.sh --no-deploy=traefik       
    # multipass exec $servername -- /bin/bash /home/ubuntu/k3s.sh 

    multipass transfer  install_k3s.sh $servername:/home/ubuntu/install_k3s.sh
    multipass exec $servername -- sudo /bin/bash /home/ubuntu/install_k3s.sh 


    IP=`multipass list | grep $servername | awk '{print $3}'`
    multipass exec $servername sudo cat /etc/rancher/k3s/k3s.yaml  | sed  -e s/127.0.0.1/$IP/g > $token
    export KUBECONFIG=$token
    echo " "    
    echo "TODO:    sudo echo $IP    $servername >>/etc/hosts"
fi
echo " "
echo "TODO:    export KUBECONFIG=$token"
echo " "
echo " "



