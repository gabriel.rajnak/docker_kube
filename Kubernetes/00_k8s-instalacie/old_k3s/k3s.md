# k3s Kubernetes

### Startni virtualku
```multipass launch -c 2 -n k3s -m 2G -d 5G 22.04```

### Pripoj sa na virtualku
```multipass shell mykube```

### Nainstaluj k3s

```
sudo apt update
sudo apt upgrade
curl -sfL https://get.k3s.io | sh -
sudo cat  /etc/rancher/k3s/k3s.yaml
```

## Nastav pristup z mackooku
```
token="$PWD/token.yaml" ; echo $token
IP=`multipass list | grep k3s | awk '{print $3}'`
multipass exec k3s sudo cat /etc/rancher/k3s/k3s.yaml  | sed  -e s/127.0.0.1/$IP/g > $token
export KUBECONFIG=$token
```




# Stare poznamky

Treba prepisat IP adresu na ktorej pocuva : (multipath list)
```server: https://127.0.0.1:6443```

### Uloz config do suboru na locale

export KUBECONFIG="/Users/bank365/gitlab/docker_kube/token.yaml"
export KUBECONFIG="/Users/bank365/Documents/Treningy/docker_kube//token.yaml"

# Pridaj dalsie worker nody

## Naistaluj k3s na dalsiu virtualku 

```multipass launch -c 1 -n k3s-worker -m 2G -d 5G 22.04```

### Na master node ziskaj token
```
sudo cat /var/lib/rancher/k3s/server/node-token
K10ebc74ce3ef9146790ea990c17d2f03c6eaff44bf62e05a3baf17d0cd489bb130::server:08663bc3143b91b1b640165a47f93aa5
```

## Spusti na novom workernode
```
curl -sfL https://get.k3s.io | K3S_URL=https://192.168.64.21:6443  K3S_TOKEN=K10d1d5df450d072ddff1d686739b00db48c797e5522c37d0531a135d4e381a73d1::server:05a08f293b8d8e17550fb930be66ddee sh -
```


---
# Stare poznamky

## Nainstaluj k3s
[https://medium.com/platformer-blog/up-and-running-k3s-with-multipass-on-imac-or-macbook-pro-bee069247cc0]

```
sudo apt update
sudo apt upgrade
curl -sfL https://get.k3s.io | sh -
```


### Ziskaj konfig pre k3s /etc/rancher/k3s/k3s.yaml

Treba prepisat IP adresu na ktorej pocuva :
```server: https://127.0.0.1:6443```


vyexportuj konfig s celou cestou.
```
export KUBECONFIG="/Users/bank365/Document_local/gitlab_repository/docker_kube/nginx-proxy/k3s_config.yaml"
```


[https://headworq.org/en/how-to-connect-to-kubernetes/]


---


```

   16  sudo groupadd  k3s
   18  sudo vi /etc/group          -- pridaj ubuntu do grupy k3s
   19  id
   26  sudo chown root:k3s /etc/rancher/k3s/k3s.yaml 
   28  sudo chmod 740 /etc/rancher/k3s/k3s.yaml 
   32  ls -la /etc/rancher/k3s/k3s.yaml 
   ```


#### pod.yml
```
apiVersion: v1
kind: Pod
metadata:
  name: cowfortune
spec:
  containers:
    - name: funbox
      image: wernight/funbox
      command: ["/bin/bash"]
      args: ["-c", "while true; do fortune | cowsay ; sleep 15; done"]
````


# Zakazanie traefik
[https://www.suse.com/support/kb/doc/?id=000020082]

https://blog.thenets.org/how-to-create-a-k3s-cluster-with-nginx-ingress-controller/

export INSTALL_K3S_EXEC="server --no-deploy traefik"
# Create k3s cluster
curl -sfL https://get.k3s.io | sh -s -



## Ingress :
https://devopscube.com/configure-ingress-tls-kubernetes/


https://www.nginx.com/blog/automating-certificate-management-in-a-kubernetes-environment/


certifikaty : https://www.fullstaq.com/knowledge-hub/blogs/setting-up-your-own-k3s-home-cluster