locals {
  hostname = lower("${lookup(var.env_short_map, var.environment, null)}ge${var.vm_short_name}0${lookup(var.target_ip_map, var.target_node, null)}vm")
}

terraform {
  required_providers {
    proxmox = {
      version = "1.0.0"
      source  = "github.com/telmate/proxmox"
    }
  }
}

resource "proxmox_vm_qemu" "main" {
  name        = local.hostname
  target_node = var.target_node
  desc        = var.vm_full_name

  clone      = var.clone_template
  full_clone = true
  agent      = var.qemu_agent
  os_type    = var.os_type
  cores      = var.cpu_cores
  sockets    = var.sockets
  cpu    = var.cpu_type
  memory = var.memory
  balloon = var.memory_balloon
  scsihw = var.scsihw

  cipassword = var.cipassword


  # Data disk
  disk {
    id           = 1
    size         = var.data_disk_size
    type         = var.data_disk_type
    storage      = var.disk_storage
    storage_type = var.storage_type
    ssd          = var.data_disk_ssd
    discard      = var.data_disk_discard
    format       = var.data_disk_format
    iothread     = var.data_disk_iothread
  }

  # Management Interface
  network {
    id     = 0
    model  = var.ifmodel
    bridge = var.svif
  }

  ipconfig0 = "ip=${lookup(var.sv_node_iprange, var.target_node, null)}${var.lb_ip}/24,gw=${lookup(var.sv_node_iprange, var.target_node, null)}1"

  sshkeys = <<EOF
  ${var.sshkey}
  EOF

  lifecycle {
    ignore_changes = [
      network,
      cipassword,
      bootdisk,
      disk,
      ciuser
    ]
  }
}
