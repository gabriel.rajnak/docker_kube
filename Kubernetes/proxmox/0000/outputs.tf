output "info" {
  value = {
    type      = var.vm_short_name             == null ? "NULL" : var.vm_short_name
    hostname  = proxmox_vm_qemu.main.name     == null ? "NULL" : proxmox_vm_qemu.main.name
    ip        = proxmox_vm_qemu.main.ssh_host == null ? "NULL" : proxmox_vm_qemu.main.ssh_host
  }
}