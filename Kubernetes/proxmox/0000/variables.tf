variable "cipassword" {
}

variable "environment" {
  type        = string
  description = "name of hte env"
}

variable "vm_short_name" {
  type        = string
  description = "Short Name of the VM"
}

variable "target_node" {
  type        = string
  description = "Target node on the hypervisor"
}

variable "vm_full_name" {
  type        = string
  description = "Full Name of the VM"
}

variable "clone_template" {
  type        = string
  description = "Template to clone for the VM"
  default     = "ubuntu-20.04-template"
}

variable "os_type" {
  type        = string
  description = "Which provisioning method to use, based on the OS type.values: ubuntu, centos, cloud-init"
  default     = "ubuntu"
}

variable "qemu_agent" {
  type        = number
  description = "Activate QEMU Agent for VM"
  default     = 1
}

variable "cpu_cores" {
  type        = string
  description = "Amount of cpu cores"
  default     = "2"
}

variable "sockets" {
  type        = string
  description = "Amount of sockets"
  default     = "1"
}

variable "vcpus" {
  type        = string
  description = "Amount of vcpus"
  default     = "2"
}

variable "cpu_type" {
  type        = string
  description = "cpu type"
  default     = "host"
}

variable "memory" {
  type        = string
  description = "Amount of memory"
  default     = "8192"
}

variable "memory_balloon" {
  type        = number
  description = "Minimum amount of memory in case if dynamic"
  default     = 0
}

variable "scsihw" {
  type        = string
  description = "SCSI Controller"
  default     = "virtio-scsi-pci"
}

variable "ifmodel" {
  type        = string
  description = "Interface Model"
  default     = "virtio"
}

variable "svif" {
  type        = string
  description = "Service Interface"
  default     = "vmbr4006"
}

variable "sshkey" {
  type        = string
  description = "ssh key"
  default     = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIL1Gm8loriHr3e61kvIvX1XZN1QiDl2YxQfYbDN9XHhK ubuntu@commander01"
}

variable "data_disk_size" {
  type        = number
  description = "Data Disk Size in GB"
  default     = 256
}

variable "data_disk_type" {
  type        = string
  description = "data disk type"
  default     = "virtio"
}

variable "disk_storage" {
  type        = string
  description = "disk storage"
  default     = "vm-data"
}

variable "storage_type" {
  type        = string
  description = "storage type"
  default     = "lvm"
}

variable "data_disk_iothread" {
  type        = bool
  description = "iothread"
  default     = false
}

variable "data_disk_ssd" {
  type        = bool
  description = "ssd emulation"
  default     = false
}

variable "data_disk_discard" {
  type        = string
  description = "discard values ignore|on"
  default     = "on"
}

variable "data_disk_format" {
  type        = string
  description = "Data disk format"
  default     = "raw"
}

variable "data_disk_backup" {
  type        = bool
  description = "backup option"
  default     = true
}

variable "target_ip_map" {
  default = {
    nsanpn01 = 1
    nsanpn02 = 2
    nsanph01 = 3
    nsanpn03 = 4
    nsanpn04 = 5
    nsanph02 = 6
    nsanpn05 = 7
    nsanpn06 = 8
    nsanph03 = 9
  }
}

#variable "sv_gw_map" {
#  default = {
#    nsanpn01 = "10.255.6.1"
#    nsanpn02 = "10.255.6.2"
#    nsanph01 = "10.255.6.3"
#  }
#}

#variable "sv_env_iprange_map" {
#  default = {
#    Test = "10.255.6."
#    Prod = "10.255.7."
#  }
#}

variable "sv_node_iprange" {
  default = {
    nsanpn01 = "10.255.101."
    nsanpn02 = "10.255.102."
    nsanph01 = "10.255.103."
    nsanpn03 = "10.255.104."
    nsanpn04 = "10.255.105."
    nsanph02 = "10.255.106."
    nsanpn05 = "10.255.107."
    nsanpn06 = "10.255.108."
    nsanph03 = "10.255.109."
  }
}

variable "env_short_map" {
  default = {
    Test = "t"
    Prod = "p"
  }
}

variable "lb_ip" {
  type        = number
  description = "IP of the loadbalancer"
}
