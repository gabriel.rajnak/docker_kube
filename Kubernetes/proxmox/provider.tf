terraform {
  required_providers {
    proxmox = {
      source  = "bpg/proxmox"
      version = "0.68.0" # x-release-please-version
    }
  }
}
locals {
  virtual_environment_endpoint             = "https://192.168.1.6:8006/"
  virtual_environment_ssh_username         = "root"
  virtual_environment_token                = "root@pam!root=bde533e2-f0c3-4fc7-a57a-92768d2e3f6c"

}

provider "proxmox" {
  endpoint  = local.virtual_environment_endpoint
  username="root@pam"
  password ="1q2w3e4r"
  #api_token = local.virtual_environment_token
  insecure = true
  ssh {
    agent    = true
    #username = local.virtual_environment_ssh_username
  }
}
# ===============================================================================

