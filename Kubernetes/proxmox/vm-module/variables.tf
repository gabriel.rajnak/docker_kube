variable "name" {
  type        = string
  description = "Short Name of the VM"
}

variable "cpu" {
  type        = number
  description = "CPU-core"
  default = 1
}

variable "ram" {
  type        = number
  description = "memory"
  default = 2048
}

variable "boot" {
  type        = number
  description = "system disk"
  default = 20
}

variable "data_disk" {
  type        = number
  description = "Data disk"
  default = 1
}

variable "ip" {
  type        = string
  description = "IP"
}

variable "gw" {
  type        = string
  description = "GW"
  default = "192.168.1.1"
}

# not used yet
variable "dns" {
  type        = string
  description = "DNS"
  default = "192.168.1.1"
}

variable "img" {
  type        = string
  description = "Installation image"  
}

variable "user_data" {
  type        = any
  description = "User data for boot configuration"  
  default = null
}
