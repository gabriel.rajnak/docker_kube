terraform {
  required_providers {
    proxmox = {
      source  = "bpg/proxmox"
      version = "0.68.0" # x-release-please-version
    }
  }
}

# data "local_file" "ssh_public_key" {
#   filename = "/Users/gabo/.ssh/id_ed25519.pub"
# }

resource "proxmox_virtual_environment_vm" "ubuntu_vm" {
  name      = var.name
  node_name = "olymp"

  agent {
    enabled = false
  }

  cpu {
    cores = var.cpu
  }

  memory {
    dedicated = var.ram
    floating  = var.ram # set equal to dedicated to enable ballooning
  }

  initialization {
    ip_config {
      ipv4 {
        address = var.ip
        gateway = var.gw
        //resolver = var.dns        
      }
    }

    # user_account {
    #   username = "ubuntu"
    #   keys     = [trimspace(data.local_file.ssh_public_key.content)]
    #   password ="1q2w3e4r"
    # }
    user_data_file_id = var.user_data 
  }

  disk {
    datastore_id = "local-lvm"
    file_id      = var.img  #proxmox_virtual_environment_download_file.ubuntu_cloud_image.id
    interface    = "virtio0"
    iothread     = true
    discard      = "on"
    size         = var.boot
  }

  disk{
    datastore_id = "local-lvm"
    interface    = "virtio1"
    iothread     = true
    discard      = "on"
    file_format  = "raw"
    size         = var.data_disk
  }

  network_device {
    bridge = "vmbr0"
  } 

   
}

# nano /etc/pve/storage.cfg

# dir: local
#     path /var/lib/vz
#     content backup,iso,vztmpl,snippets

# systemctl restart pvedaemon
# systemctl restart pvestatd


# resource "proxmox_virtual_environment_file" "cloud_config" {
#   content_type = "snippets"
#   datastore_id = "local"
#   node_name    = "olymp"

#   source_raw {
#     data = <<-EOF
#     #cloud-config
#     users:
#       - default
#       - name: ubuntu
#         groups:
#           - sudo
#         shell: /bin/bash
#         ssh_authorized_keys:
#           - ${trimspace(data.local_file.ssh_public_key.content)}
#         sudo: ALL=(ALL) NOPASSWD:ALL
#     runcmd:
#         - apt update
#         - apt install -y qemu-guest-agent net-tools
#         - timedatectl set-timezone Europe/Bratislava
#         - systemctl enable qemu-guest-agent
#         - systemctl start qemu-guest-agent
#         - echo "done" > /tmp/cloud-config.done
#     EOF

#     file_name = "cloud-config.yaml"
#   }
# }


