locals {
  machines = {
    # NAMADA
    #man = { name="Linux-vm1", ram=16028,cpu=4, data_disk=65,ip="192.168.1.10/24" }
    #mani = { name="Linux-vm1", ram=8028,cpu=4, data_disk=65,ip="192.168.1.11/24" }
    # LINUX lab
    # vm1 = { name="Linux-vm1", ram=2028,cpu=3, data_disk=65,ip="192.168.1.10/24" }
    # vm2 = { name="Linux-vm2", ram=2028,cpu=2, data_disk=10,ip="192.168.1.11/24" }
    # vm3 = { name="Linux-vm3", ram=2028,cpu=3, data_disk=10,ip="192.168.1.12/24" }
    # vm4 = { name="Linux-vm4", ram=2028,cpu=6, data_disk=10,ip="192.168.1.13/24" }    



    #  vm5 = { name="Linux-vm5", ram=2028,cpu=3, data_disk=65,ip="192.168.1.21/24" }
    #  vm6 = { name="Linux-vm6", ram=2028,cpu=2, data_disk=10,ip="192.168.1.22/24" }
    #  vm7 = { name="Linux-vm7", ram=2028,cpu=3, data_disk=10,ip="192.168.1.23/24" }
    #  vm8 = { name="Linux-vm8", ram=2028,cpu=6, data_disk=10,ip="192.168.1.24/24" }    

  #   # # K8s lab
    k8s1 = { name="k8s", ram=8028,cpu=2, boot=50, data_disk=10,ip="192.168.1.20/24" }    
    #  k8s2 = { name="k8s-worker-1", ram=4028,cpu=2,boot=50, data_disk=10,ip="192.168.1.21/24" }    
    #  k8s3 = { name="k8s-worker-2", ram=4028,cpu=2,boot=50, data_disk=10,ip="192.168.1.22/24" }    
    #  k8s4 = { name="k8s-worker-3", ram=4028,cpu=2,boot=44, data_disk=33,ip="192.168.1.23/24" }    
    #  k8s5 = { name="k8s-worker-4", ram=4028,cpu=2,boot=50, data_disk=10,ip="192.168.1.24/24" }    
   }
}

module "labs" {
  for_each = local.machines

  source = "./vm-module"
  name = each.value.name
  cpu = each.value.cpu  
  ram = each.value.ram
  ip = each.value.ip
  boot = try(each.value.boot, 10)
  data_disk = each.value.data_disk
  img = proxmox_virtual_environment_download_file.ubuntu_cloud_image.id
  user_data = proxmox_virtual_environment_file.cloud_config.id  

  providers = {
    proxmox = proxmox
  }
}

resource "proxmox_virtual_environment_download_file" "ubuntu_cloud_image" {
  content_type = "iso"
  datastore_id = "local"
  node_name    = "olymp"

  url = "https://cloud-images.ubuntu.com/oracular/current/oracular-server-cloudimg-amd64.img"
  file_name          = "oracular-server-cloudimg-amd64.img"
}

data "local_file" "ssh_public_key" {
  filename = "/Users/gabo/.ssh/id_ed25519.pub"
}

# nano /etc/pve/storage.cfg

# dir: local
#     path /var/lib/vz
#     content backup,iso,vztmpl,snippets

# systemctl restart pvedaemon
# systemctl restart pvestatd

resource "proxmox_virtual_environment_file" "cloud_config" {
  content_type = "snippets"
  datastore_id = "local"
  node_name    = "olymp"

  source_raw {
    data = <<-EOF
    #cloud-config
    users:
      - default
      - name: ubuntu
        shell: /bin/bash
        ssh_authorized_keys:
          - ${trimspace(data.local_file.ssh_public_key.content)}
        groups:
          - sudo
        shell: /bin/bash          
        sudo: ALL=(ALL) NOPASSWD:ALL
    runcmd:
        - apt update
        - apt install -y qemu-guest-agent net-tools prometheus-node-exporter
        - timedatectl set-timezone Europe/Bratislava
        - systemctl enable qemu-guest-agent
        - systemctl start qemu-guest-agent
        - echo "done" > /tmp/cloud-config.done
    EOF

    file_name = "cloud-config.yaml"
  }
}


        # shell: /bin/bash
        # #ssh_authorized_keys:
        # #  - ${trimspace(data.local_file.ssh_public_key.content)}
