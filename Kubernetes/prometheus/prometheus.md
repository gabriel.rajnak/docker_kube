# Prometheus monitoring

[https://bitnami.com/stack/prometheus]

helm https://artifacthub.io/packages/helm/bitnami/prometheus

helm install my-release oci://registry-1.docker.io/bitnamicharts/prometheus


# v akom stave je alert manager
```
(rate(alertmanager_notifications_failed_total{job="kube-prometheus-stack-alertmanager",namespace="kube-prometheus-stack"}[5m]) / ignoring (reason) group_left () rate(alertmanager_notifications_total{job="kube-prometheus-stack-alertmanager",namespace="kube-prometheus-stack"}[5m])) > 0.01

```

