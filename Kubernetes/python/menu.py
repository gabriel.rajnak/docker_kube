# --------------------------------------------------------------
# MENU
# --------------------------------------------------------------

def create(record):
    print("record created", record)
    pass

def read(id):
    pass

def update(record):
    pass

def delete(id):
    pass
# ===============================================================
# CLI GUI
# ===============================================================
def cli_create():
    print("New record")
    name= input("Name : ")
    phone= input("phone : ")
    email = input("email :")
    create( {"name":name,"phone":phone,"email":email })
    pass

def cli_read():
    pass

def cli_update():
    pass

def cli_delete():
    pass
# ===============================================================
# HTTP REST API
# ===============================================================
def http_create():
    pass



# ===============================================================
def menu():
    item = input()
    return item

# ===============================================================
def main():
    while True:
        item = menu()
        if item == "":
            break
        match item:
            case "create":
                print("M:create")
                cli_create()
            case "read":
                print("M:read")
            case "update":
                print("M:update")
            case "delete":
                print("M:delete")
    print("End.")
# ===============================================================          
if __name__ == "__main__":
    main()
# ===============================================================              



