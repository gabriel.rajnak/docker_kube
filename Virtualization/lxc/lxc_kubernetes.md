# LXC a kubernetes
[https://medium.com/@selvamraju007/setting-up-k8s-cluster-using-lxc-lxd-58f0c288af3e]

```sh
lxc profile create k8s
$ cat k8s-profile-config | lxc profile edit k8s
$ lxc profile list
+---------+---------+
|  NAME   | USED BY |
+---------+---------+
| default | 0       |
+---------+---------+
| k8s     | 0       |
+---------+---------+
```

```
config:
  limits.cpu: "2"
  limits.memory: 2GB
  limits.memory.swap: "false"
  linux.kernel_modules: ip_tables,ip6_tables,netlink_diag,nf_nat,overlay
  raw.lxc: "lxc.apparmor.profile=unconfined\nlxc.cap.drop= \nlxc.cgroup.devices.allow=a\nlxc.mount.auto=proc:rw
    sys:rw"
  security.privileged: "true"
  security.nesting: "true"
description: LXD profile for Kubernetes
devices:
  eth0:
    name: eth0
    nictype: bridged
    parent: lxdbr0
    type: nic
  root:
    path: /
    pool: default
    type: disk
name: k8s
used_by: []
```
# List dostupnych image
```sh
lxc image list images: | grep ubuntu
| ubuntu/focal/desktop (3 more)            | 48139587b175 | yes    | Ubuntu focal amd64 (20241109_0025)        | x86_64       | VIRTUAL-MACHINE | 997.13MiB  | Nov 9, 2024 at 12:00am (UTC)  |
| ubuntu/jammy/desktop (3 more)            | 9b892d1418c9 | yes    | Ubuntu jammy amd64 (20241109_0025)        | x86_64       | VIRTUAL-MACHINE | 994.56MiB  | Nov 9, 2024 at 12:00am (UTC)  |
| ubuntu/noble/desktop (3 more)            | 369956db266e | yes    | Ubuntu noble amd64 (20241109_0025)        | x86_64       | VIRTUAL-MACHINE | 1113.14MiB | Nov 9, 2024 at 12:00am (UTC)  |
devops@zeus:~$ 
```

# Vytvor nody
```sh
$ lxc launch ubuntu kmaster --profile k8s
Creating kmaster
Starting kmaster

$ lxc launch images:centos/9-Stream kworker1 --profile k8s
Creating kworker1
Starting kworker1

$ lxc launch images:centos/9-Stream kworker2 --profile k8s
Creating kworker2
Starting kworker2
```