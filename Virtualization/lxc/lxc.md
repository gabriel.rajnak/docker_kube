# LXC
[https://github.com/justmeandopensource/kubernetes/blob/master/lxd-provisioning/README.md]


## Inicializacia
```bash
sudo lxd init
sudo lxc profile create developer
cat profile | sudo lxc profile edit developer
 
```

```bash
sudo ufw allow in on lxdbr0
sudo ufw route allow in on lxdbr0
sudo ufw route allow out on lxdbr0
```
```lxc launch ubuntu:24.04 developer --profile developer```

```bash
   lxc list
    lxc delete <whatever came from list>
    lxc image list
    lxc image delete <whatever came from list>
    # I did not actually need to delete lxdbr0
    lxc network list
    lxc network delete <whatever came from list>
    echo ‘{“config”: {}}’ | lxc profile edit default
    lxc storage volume list default
    lxc storage volume delete default <whatever came from list>
    lxc storage delete default
```

```bash
   sudo brctl show
   sudo  ip link set lxbrd1 down
   sudo  brctl delbr lxbrd1
```    


## install docker
```bash
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

```




https://computingpost.medium.com/how-to-create-lxc-containers-using-terraform-5aaa2dc2aec0

