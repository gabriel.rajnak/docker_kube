# Virtualizacia na MAC-u
## Multipass

```brew install --cask multipass```

```brew reinstall --cask multipass```

https://multipass.run/docs/launch-command
### Dostupne image
```
% multipass find
Image                       Aliases           Version          Description
18.04                       bionic            20220523         Ubuntu 18.04 LTS
20.04                       focal,lts         20220530         Ubuntu 20.04 LTS
21.10                       impish            20220604         Ubuntu 21.10
22.04                       jammy             20220604         Ubuntu 22.04 LTS
anbox-cloud-appliance                         latest           Anbox Cloud Appliance
charm-dev                                     latest           A development and testing environment for charmers
docker                                        latest           A Docker environment with Portainer and related tools
minikube                                      latest           minikube is local Kubernetes```
```

## Vytvor virtualku
```multipass launch -c 2 -n pokus2 -m 2G -d 5G 22.04```
```multipass launch -c 2 -n k3s -m 2G 22.04```
```multipass launch -c 2 -n nfs -m 512M 22.04```
```
multipass list   - zoznam  virtualok
multipass find  - ake su dostupne image
multipass launch 
multipass delete pokus  - delete
multipass purge         - totlane zmazanie
```

### ssh na virtualku
```multipass shell vm_name```



---
### Stop VM

### Nework

[https://multipass.run/docs/additional-networks]

### Odistalovanie
### multipass fix auth
- odinstaluj a nainstaluj
```sudo sh "/Library/Application Support/com.canonical.multipass/uninstall.sh"```
