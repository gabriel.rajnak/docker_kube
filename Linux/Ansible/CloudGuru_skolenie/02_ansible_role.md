# Ansible roles
```bash
cd /etc/ansible/roles/
sudo mkdir baseline 
sudo chown ansible.ansible baseline
mkdir /etc/ansible/roles/baseline/{templates,tasks,files}
echo "---" > /etc/ansible/roles/baseline/tasks/main.yaml
cp /home/ansible/resources/motd.j2 /etc/ansible/roles/baseline/templates/
```

motd.j2
```
cat motd.j2 
###############################################################################
{{ ansible_hostname }} is....
     ___      .__   __.      _______. __  .______    __       _______
    /   \     |  \ |  |     /       ||  | |   _  \  |  |     |   ____|
   /  ^  \    |   \|  |    |   (----`|  | |  |_)  | |  |     |  |__
  /  /_\  \   |  . `  |     \   \    |  | |   _  <  |  |     |   __|
 /  _____  \  |  |\   | .----)   |   |  | |  |_)  | |  `----.|  |____
/__/     \__\ |__| \__| |_______/    |__| |______/  |_______||_______|

.___  ___.      ___      .__   __.      ___       _______  _______  _______
|   \/   |     /   \     |  \ |  |     /   \     /  _____||   ____||       \
|  \  /  |    /  ^  \    |   \|  |    /  ^  \   |  |  __  |  |__   |  .--.  |
|  |\/|  |   /  /_\  \   |  . `  |   /  /_\  \  |  | |_ | |   __|  |  |  |  |
|  |  |  |  /  _____  \  |  |\   |  /  _____  \ |  |__| | |  |____ |  '--'  |
|__|  |__| /__/     \__\ |__| \__| /__/     \__\ \______| |_______||_______/


################################################################################
```

baseline/tasks/deploy_motd.yaml
```yaml
---
- template:
     src: motd.j2
     dest: /etc/motd    
```

main.yaml
```yaml
---
- name: configure moth
  import_tasks: deploy_motd.yaml
- name: deploy nagios
  import_tasks: deploy_nagios.yaml
```

/etc/ansible/roles/baseline/deploy_nagios.yaml
```yaml
---
- yum: name=nrpe state=latest


```



