# Ansible by doing


Control
```bash
sudo yum install epel-release
sudo yum install ansible
```

Na oboch masinach
```sudo useradd ansible```


```sudo passwd ansible```

control, vytvor kluce
```bash
sudo su - ansible
ssh-keygen
ssh-copy-id ansible@workstation
```

Na workstattion
```bash
sudo visudo
ansible ALL=(ALL) NOPASSWD:ALL
```

inventory
```
workstation
```

git-install.yaml
```yaml
--- # install git
- hosts: workstation
  become: yes
  tasks:
  - name: install git
    yum:
      name: git
      state: latest
```

```ansible-playbook -i inventory git-install.yaml```

---
```bash
[ansible@control1 ~]$ ansible -i inventory node1 -m ping
node1 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    }, 
    "changed": false, 
    "ping": "pong"
}
```