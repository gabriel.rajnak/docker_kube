# Git zaklady

## Nastavenie usera

```bash
git config -global user.name "Gabo Rajnak"
git config -global user.email "gabriel.rajnak@gmail.com"
```

## Vyrvorenie repozitara

```bash
mkdir hello-world
cd hello-world
git init
```


## Pull z repozitara
```bash
git pull https://<repozitar>
```

## Pridaj zmeny

```bash
git add .
```

## Commit
```bash
git commit -m "comentar co sa zmenilo"
```

## Aktualny stav
```bash
git status
git log
```

```git diff```  -- zmeny

```git push origin <branch name>```

## Nova branch
```git branch <branch name>```

## Prepni brach
```git checkout <branch name>```

## Spojenie
```git merge <branch name>```

## Rebase
```git rebase```









