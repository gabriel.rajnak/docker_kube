# Prometheus na LUNUX-e

- Ubuntu linux
- update/upgrade
```bash
sudo apt-get update
sudo apt-get upgrade
sudo reboot
sudo do-release-upgrade
```

# Instalacia Prometheus

[https://www.youtube.com/watch?v=h4Sl21AKiDg]

```bash
sudo apt-cache search prometheus
# potrebujeme:
# - prometheus
# - prometheus-blackbox-exporter
sudo apt-get install prometheus prometheus-blackbox-exporter 
```

Pre kamery :
```
https://github.com/Haivision/srt-prometheus-exporter
https://dahuawiki.com/Category:DH_SD-Prometheus
# priklad
https://grafana.com/grafana/dashboards/10689-dashboard-cftv/
```

## Konfiguracia Promethea
```bash
cd /etc/prometheus/
# zaloha povodneho konfiguraku ak by sa daco pokazilo
sudo cp prometheus.yml prometheus.yml_backup
```
priklad monitorovania www.zoznam.sk
```yaml
  - job_name: 'blackbox'
    metrics_path: /probe
    params:
      module: [http_2xx]
    static_configs:
      - targets:
        - https://ww.zoznam.sk    # Target to probe with https.
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: localhost:9115  # The blackbox exporter's real hostname:port.


  - job_name: 'blackbox-ping'
    metrics_path: /probe
    params:
      module: [icmp]
    static_configs:
      - targets:
        - 192.168.1.1
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: localhost:9115  # The blackbox exporter's real hostname:port.        
```
## Start/stop/restart
Definnicne subory pre start/stop su v ```/lib/systemd/system```

```bash
# Start 
sudo systemctl start prometheus.service

# Stop
sudo systemctl stop prometheus.service

# Restart
sudo systemctl restart prometheus.service
```
## Kontrola ci bezi
```bash
pepo@pepo:/lib/systemd/system$ ss -tnlp
State             Recv-Q            Send-Q                       Local Address:Port                       Peer Address:Port            Process
LISTEN            0                 4096                            127.0.0.54:53                              0.0.0.0:*
LISTEN            0                 4096                         127.0.0.53%lo:53                              0.0.0.0:*
LISTEN            0                 4096                                     *:22                                    *:*
LISTEN            0                 4096                                     *:9115                                  *:*                             <- blackbox exporter   
LISTEN            0                 4096                                     *:9090                                  *:*                             <- node exporter
LISTEN            0                 4096                                     *:9100                                  *:*                             <- prometheus
```

## Web browser

http://192.168.1.57:9090


## Logy

---
# Grafana
[https://grafana.com/docs/grafana/latest/setup-grafana/installation/debian/]

## Instalacia
```bash
sudo apt-get install -y apt-transport-https software-properties-common wget
sudo mkdir -p /etc/apt/keyrings/
wget -q -O - https://apt.grafana.com/gpg.key | gpg --dearmor | sudo tee /etc/apt/keyrings/grafana.gpg > /dev/null
echo "deb [signed-by=/etc/apt/keyrings/grafana.gpg] https://apt.grafana.com stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list
echo "deb [signed-by=/etc/apt/keyrings/grafana.gpg] https://apt.grafana.com beta main" | sudo tee -a /etc/apt/sources.list.d/grafana.list
# Updates the list of available packages
sudo apt-get update
# Installs the latest OSS release:
sudo apt-get install grafana
```

## Start / stop

```bash
sudo systemctl daemon-reload
sudo systemctl start grafana-server
sudo systemctl status grafana-server


sudo systemctl status grafana-server
# nastav autostart
sudo systemctl enable grafana-server.service

sudo systemctl start grafana-server

sudo systemctl stop grafana-server

sudo systemctl restart grafana-server
```


```bash
ss -tnlp
State                     Recv-Q                    Send-Q                                       Local Address:Port                                        Peer Address:Port                    Process
LISTEN                    0                         4096                                            127.0.0.54:53                                               0.0.0.0:*
LISTEN                    0                         4096                                         127.0.0.53%lo:53                                               0.0.0.0:*
LISTEN                    0                         4096                                                     *:22                                                     *:*
LISTEN                    0                         4096                                                     *:3000                                                   *:*   <-- grafana
LISTEN                    0                         4096                                                     *:9115                                                   *:*
LISTEN                    0                         4096                                                     *:9090                                                   *:*
LISTEN                    0                         4096                                                     *:9100                                                   *:*
```

## Web browser

http://192.168.1.57:3000/login

user: admin
pass: admin

nastavit nove heslo 

## Nastavenie
1. Datas ource

https://grafana.com/grafana/dashboards/

Node Exporter Full
Blackbox Exporter (HTTP prober) 13659
Blackbox exporter - ICMP  20338

