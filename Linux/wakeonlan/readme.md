# WakeOnLan


```sh
gabo@MacBookPro ~ % arp -a
? (192.168.1.1) at f8:d1:11:7e:59:14 on en0 ifscope [ethernet]
? (192.168.1.4) at 40:ed:0:ef:57:ac on en0 ifscope [ethernet]
? (192.168.1.5) at 90:9:d0:4c:17:f7 on en0 ifscope [ethernet]   <----
? (192.168.1.52) at ac:72:dd:a8:b6:2c on en0 ifscope [ethernet]
? (192.168.1.53) at 54:b8:74:5a:93:9c on en0 ifscope [ethernet]
? (192.168.1.72) at 40:1a:58:6:fc:41 on en0 ifscope [ethernet]
? (192.168.1.75) at 96:26:a7:32:6a:48 on en0 ifscope [ethernet]
? (192.168.1.255) at ff:ff:ff:ff:ff:ff on en0 ifscope [ethernet]
mdns.mcast.net (224.0.0.251) at 1:0:5e:0:0:fb on en0 ifscope permanent [ethernet]
gabo@MacBookPro ~ % 
```

# Python
[https://pypi.org/project/wakeonlan/]


```sh
    python3 -m venv wakeonlan
    source wakeonlan/bin/activate
    python3 -m pip install wakeonlan

pip install wakeonlan
```

## Python script

```python
from wakeonlan import send_magic_packet
mac="90:9:d0:4c:17:f7".replace(':','.')
send_magic_packet(mac)
```

