# Koncept

- alertmanager/opsgieny
    - alert rule
- prometheus scraping
    - scrape job
- nginx http/reverse-proxy   (IP:80/443) --> task network open port(prometheus->host:80/443)
    - exporter (old exporter)
    - file with metrics
- shell script wrapper (app transaction)
    - grep send/failed -> metric
- transaction
    - test output -> grep key word   send/failed


# Priprava prostredia

- na hypervizore
```bash
multipass launch -c 2 -n server -m 2G 22.04
# stop
multipass stop server
multipass list 
Name                    State             IPv4             Image
server                  Running           192.168.64.42    Ubuntu 22.04 LTS

multipass shell server
```
- na virtualke
```bash
# install nginx
sudo apt install python3-pip
pip3 install fastapi uvicorn
sudo apt install uvicorn
sudo apt-get install nginx

sudo mkdir /var/www/prometheus


sudo systemctl restart nginx.service
```
/etc/nginx/sites-available/default
```config
server {
        listen 80;
        listen [::]:80;
        server_name localhost 127.0.0.1;
        root /var/www/prometheus;

        access_log /var/log/nginx/reverse-access.log;
        error_log /var/log/nginx/reverse-error.log;

       location / {
               try_files $uri $uri/ =404;
       }
       location /exporter {
                    rewrite /exporter / break;
                    proxy_pass http://127.0.0.1:8000;
                    proxy_set_header    X-Forwarded-For $remote_addr;
      }
   }
```

### Start nginx a koltrola logov
`sudo systemctl restart nginx.service`
`journalctl -xeu nginx.service`

### Externe spustenie exportera
- `uvicorn exporter:app --host 0.0.0.0 --port 8000`
- TODO: prerobit na systemd
