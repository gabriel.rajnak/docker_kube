import uvicorn
from fastapi import FastAPI
from fastapi.responses import PlainTextResponse

app = FastAPI()

@app.get("/",response_class=PlainTextResponse)
async def root():    
    return "metrics { mesh='data'}   123"

if __name__ == "__main__":

    uvicorn.run(
        "exporter:app",
        host="0.0.0.0",
        port=8000,
        reload=False
    )

# VYVOJ : uvicorn exporter:app --reload
#
# PROD:   uvicorn exporter:app --host 0.0.0.0 --port 8000
#
#         pocuva na localhost:8000/