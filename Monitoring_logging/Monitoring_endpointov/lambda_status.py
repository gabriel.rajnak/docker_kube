import time
from datetime import datetime
import json
import requests


lambda_list = [
    {"name": "google", "url": "https://www.google.sk", "key": "google"},
#    {"name": "zoznam", "url": "https://test.bankservice.365.local:5001/ltd/Default.asmx", "key": "OK"}
]

index = "lambda-" + datetime.now().strftime("%Y-%m-%d")
url = "https://vpc-kibana-logs-zxkxtzmydlnf6gfhxwjwwtds5i.eu-central-1.es.amazonaws.com/"
elastic_url = url + index + "/_doc"
print(elastic_url)
# =============================================================


def lambda_check(lamb):
    #print("Test: ",lamb['name']," : ",lamb['url'])
    t0 = time.time()
    try:
        response = requests.get(lamb['url'])
    except:
        response = None

    t1 = time.time()
    latency = t1-t0
    #print("Status code: ", response.status_code)
    #print(t0, t1,latency)
    if response != None:
        if response.status_code < 400:
            if lamb['key'] in response.text:
                status = "ok"
            else:
                status = "missing : "+lamb['key']
        else:
            status = "connection failed : " + str(response.status_code)
    else:
        status = "connection failed : network issue"
    result = {
        "timestamp": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
        "name":    lamb['name'],
        "url":     lamb['url'],
        "latency": latency,
        "status": status
    }
    return result
# =============================================================


def post_to_elastic(data, url):

    for item in data:
        print(item)
        response = requests.post(url, json=item)


# =============================================================
result = []
for lamb in lambda_list:
    result.append(lambda_check(lamb))

post_to_elastic(result, elastic_url)
