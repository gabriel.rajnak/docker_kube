import json
import urllib3
# zkc_onprem_test
lambda_list = [
                {"name": "GetDocument", "url": "https://test.bankservice.365.local:5000/soap/serviceDgEndpoint.php", "key": "OK"},
                {"name": "SignDocument", "url": "https://test.bankservice.365.local:5001/ltd/Default.asmx", "key": "OK"},                
              ]

def lambda_check(lamb):    
    # print(lamb)
    urllib3.disable_warnings()
    http = urllib3.PoolManager(cert_reqs='CERT_NONE')
    status = "none"
    try:
        r = http.request('GET', lamb['url'])        
        status= r.status
        print(r.text)
    except:        
        status="error"
    
    result = {"url":lamb['url'], "status": status}
    print(result)
    return result
def lambda_handler(event, context):
    # TODO implement
    result = []
    for lamb in lambda_list:
        result.append(lambda_check(lamb))
        
    print(result)
    return {
        'statusCode': 200,
        'body': json.dumps(result)
    }


# lambda_handler(1,2)
