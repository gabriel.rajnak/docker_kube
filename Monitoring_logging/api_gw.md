# API gateway model

## Aplikacna API GW
- spravovana developermi
- je tesne pred lamdou
  - stage
  - method
  - validation
  - transformation

## Integracna API GW
- spravovana devops
- je nadradena pred aplikacnou API GW
- sluzi ako hlavny vstupny bod pre service
- robi sa per consumer
- obsahuje oAuth, autorizer
- musi mat predradeny VPC endpoint

## VPC endpoint  VPCE-xxxxxxxx
- patri konkretnemu accountu a konkretnemu VPC
- ma IP adresu
- vesa sa nanho vzdy jedna sluzba/api gw
  - ukazuje na integracnu API GW


 poznamka
 