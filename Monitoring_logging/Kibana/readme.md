# Wireguard monitor do Elasticu


## Jednoducha kontrola wireguardu

```
watch -n 5 "sudo wg | grep -v hour | grep hand -B1"
```
```
  allowed ips: 10.214.214.17/32
  latest handshake: 25 seconds ago
--
  allowed ips: 10.214.214.18/32
  latest handshake: 33 seconds ago
--
  allowed ips: 10.214.214.12/32
  latest handshake: 40 seconds ago
--
  allowed ips: 10.214.214.11/32
  latest handshake: 1 minute, 11 seconds ago
--
  allowed ips: 10.214.214.16/32
  latest handshake: 1 minute, 19 seconds ago
--
  allowed ips: 10.214.214.10/32
  latest handshake: 1 minute, 37 seconds ago
--
  allowed ips: 10.214.214.20/32
  latest handshake: 1 minute, 58 seconds ago
--
  allowed ips: 10.214.214.7/32
  latest handshake: 1 minute, 58 seconds ago
  ```

Skontroluj si ci je v wg_agent.py nastaveny aktualny endpoint na elastic.

```url="https://vpc-es-gabo-klhlt5aw6tkcxalpwcocmx3m5y.eu-central-1.es.amazonaws.com/wireguard/_doc"```


### Nastav cron.d
```
cat wg_monitor 

*/5 * * * * root /usr/bin/wg | /usr/bin/python3 /home/ubuntu/wg_agent.py > /dev/null
```

