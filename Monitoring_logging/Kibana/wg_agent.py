import time
from datetime import datetime 
import json
import requests
import random

# ==========================================================
dt = datetime.now()
ts= dt.strftime("%Y-%m-%d")

url="https://vpc-kibana-logs-zxkxtzmydlnf6gfhxwjwwtds5i.eu-central-1.es.amazonaws.com/wireguard-"+ts+"/_doc"
TIMEOUT = 300    # last connection in seconds

# ==========================================================
def myprint(data):
    t = type(data)
    print(t)
    if t == "<class \'dict\'>" :
        for key,value in data:
            print(key," : ",value)
    else:
        print(data)
 

def parser(data, prefix, postfix):
    if (data.find(prefix)== -1) or (data.find(postfix)== -1):
         return  ""
    s1 = data.split(prefix)
    s2 = s1[1].split(postfix)
    value =  s2[0].strip()
    return value

def getIP(data):
    return parser(data,'allowed ips:','/32')

def getTraffic(data):
    tmp = parser(data,'transfer:','sent')
    return tmp

def getHandshake(data):
    last  = parser(data, "handshake:","transfer")
    fields = last.split(" ")
    handshake = 0
    index = 0
    if (len(fields)<3):
        return 9999
    while (index < len(fields)):
        if fields[index].find("ago")>=0:
            break
        time = int(fields[index])
        index +=1
        if fields[index].find("second")>=0:
            pass
        if fields[index].find("minute")>=0:
            time = time * 60
        if fields[index].find("hour")>=0:
            time = time * 3600
        handshake = handshake + time
        index = index + 1
    return handshake

def simpleStatus(items):    
    dt = datetime.now()
    result = []
    for item in items:
        result.append(getIP(item))
    msg = {         
         'timestamp': dt.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
         'count': len(result),
         'clients': result
    }
    return msg

def  postToElastic(url, data):
    print("Post : ",url)
    print(data)
    response = requests.post(url, json=data)
    #print("Status code: ", response.status_code)
    #print("Printing Entire Post Request")
    #print(response.json())


#text_file = open("wg", "r")
#data = text_file.read()
#text_file.close()
data =""
for line in sys.stdin:
    data = data +"\n" + line

items = data.split('peer:')
# filtruj prihlasenych # nie starsich ako TIMEOUT
# result = list(filter(lambda data: data.find("latest")>=0 and data.find("hour")==-1, items)) 
result = list(filter(lambda data: getHandshake(data) < TIMEOUT, items)) 
 
postToElastic(url,simpleStatus(result))

#cat wg_monitor 
# */5 * * * * root /usr/bin/wg | /usr/bin/python3 /home/ubuntu/wg_agent.py > /dev/null
