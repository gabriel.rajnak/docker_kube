import time
from datetime import datetime
#import json
import requests
from urllib3.exceptions import InsecureRequestWarning

#import check_list as config

# config list moved to check_list
# target_url = "https://kibana.int.devops.dsp.365.bank/"
# lambda_list = [
# ]
# =============================================================

index_name = "dp-service"
target_url = "https://kibana.int.devops.dsp.365.bank/"

lambda_list = [
    # DEV
    # danubepay.int.dev.dsp.365.bank
    {"name": "dev-digital", "url": "http://danubepay.int.dev.dsp.365.bank:9063/v1/institutions/65365/health/check", "key": "OK"},
    {"name": "dev-epin", "url": "http://danubepay.int.dev.dsp.365.bank:9069/v1/institutions/65365/health/check", "key": "OK"},
    {"name": "dev-auth", "url": "http://danubepay.int.dev.dsp.365.bank:9066/v1/institutions/65365/health/check", "key": "OK"},
    # test
    # danubepay.int.test.dsp.365.bank
    {"name": "test-digital", "url": "http://danubepay.int.test.dsp.365.bank:9063/v1/institutions/65365/health/check", "key": "OK"},
    {"name": "test-epin", "url": "http://danubepay.int.test.dsp.365.bank:9069/v1/institutions/65365/health/check", "key": "OK"},
    {"name": "test-auth", "url": "http://danubepay.int.test.dsp.365.bank:9066/v1/institutions/65365/health/check", "key": "OK"},
    # uat
    # danubepay.int.uat.dsp.365.bank
    {"name": "uat-digital", "url": "http://danubepay.int.uat.dsp.365.bank:9063/v1/institutions/65365/health/check", "key": "OK"},
    {"name": "uat-epin", "url": "http://danubepay.int.uat.dsp.365.bank:9069/v1/institutions/65365/health/check", "key": "OK"},
    {"name": "uat-auth", "url": "http://danubepay.int.uat.dsp.365.bank:9066/v1/institutions/65365/health/check", "key": "OK"},
    # prod
    # danubepay.int.prod.dsp.365.bank
    {"name": "prod-digital", "url": "https://danubepay.int.prod.dsp.365.bank:9063/v1/institutions/65365/health/check", "key": "OK"},
    {"name": "prod-epin", "url": "https://danubepay.int.prod.dsp.365.bank:9069/v1/institutions/65365/health/check", "key": "OK"},
    {"name": "prod-auth", "url": "https://danubepay.int.prod.dsp.365.bank:9066/v1/institutions/65365/health/check", "key": "OK"}
]
# =============================================================
index = index_name+ "-"+ datetime.now().strftime("%Y-%m-%d")
elastic_url = target_url + index + "/_doc"
# print(elastic_url)
# =============================================================

def lambda_check(lamb):
    #print("Test: ",lamb['name']," : ",lamb['url'])
    t0 = time.time()
    print(lamb['name'])
    status= "connected"
    response="__none__"
    output =""
    try:
        requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)       
        response = requests.get(lamb['url'], timeout=3, verify=False)
    except requests.exceptions.ConnectionError as e:
        #print("----- Connection error ----------------")
        #print (e)            
        #print("---------------------------------------")
        status = "no answer"
          
    # print(response)  
    t1 = time.time()
    latency = t1-t0 
    if latency> 3:
        status= "timeout"      
    if status=='connected':      
        status = status+" " + str( response.status_code  )
        output = response.text
        print(output)
        
    #print(status)

    

    result = {
        "timestamp": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
        "name":    lamb['name'],
        "url":     lamb['url'],
        "latency": latency,
        "status": status,
        "response": output
    }
    return result
# =============================================================
def post_to_elastic(data, url):
    # print("URL: ",url)
    for item in data:
        # print("POST",item)
        response = requests.post(url, json=item)
# =============================================================

# print(__name__)
if __name__ =="__main__":
    result = []
    for lamb in lambda_list:
        result.append(lambda_check(lamb))

    post_to_elastic(result, elastic_url)
    