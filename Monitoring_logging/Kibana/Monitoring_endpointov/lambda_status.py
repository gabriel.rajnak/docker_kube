import time
from datetime import datetime
import json
import requests


lambda_list = [
                {"name": "DEV", "url": "https://dsp-network-vpn-infra-dp-dev-8330fc7a90b48fa4.elb.eu-central-1.amazonaws.com:9061/health/check", "type":"telnet","key": "OK"},
                {"name": "UAT", "url": "https://dsp-network-vpn-infra-dp-uat-d1aa160acd85a67c.elb.eu-central-1.amazonaws.com:9061/health/check", "type":"telnet", "key": "OK"},
                {"name": "PROD", "url": "https://dsp-network-vpn-infra-dp-prod-934230904049599e.elb.eu-central-1.amazonaws.com:9061/health/check", "type":"telnet", "key": "OK"},
                
              ]
index = "danDANUBEPAY-VPN-" + datetime.now().strftime("%Y-%m-%d")
url = "https://kibana.int.devops.dsp.365.bank/"
elastic_url = url + index + "/_doc"
print(elastic_url)

# =============================================================
def telnet_check(endpoint):
    # expplode IP, port
    parts = endpoint['url'].split("/")
    parts2 = parts[2].split(":")
    ip   = parts2[0]
    port = parts2[1]
    



    return "xxx"




def lambda_check(lamb):
    #print("Test: ",lamb['name']," : ",lamb['url'])
    t0 = time.time()
    print(lamb['name'])
    status= "connected"
    response="__none__"
    try:
        response = requests.get(lamb['url'], timeout=3, verify=False)
    except requests.exceptions.ConnectionError as e:
        #print("----- Connection error ----------------")
        #print (e)            
        #print("---------------------------------------")
        status = "no answer"
              
    t1 = time.time()
    latency = t1-t0 
    if latency> 3:
        status= "timeout"      
    if status=='connected':      
        status = status+" " + str( response.status_code  )
        
    print(status)

    

    result = {
        "timestamp": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
        "name":    lamb['name'],
        "url":     lamb['url'],
        "latency": latency,
        "status": status
    }
    return result
# =============================================================


def post_to_elastic(data, url):

    for item in data:
        print("POST",url,item)
        response = requests.post(url, json=item)


# =============================================================
result = []
for lamb in lambda_list:
    result.append(lambda_check(lamb))

post_to_elastic(result, elastic_url)
