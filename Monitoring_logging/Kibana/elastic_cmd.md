

DELETE /vector-2022.04.01


DELETE /wireguard

DELETE /gabo

DELETE /dev-aws_tags_evaluation-2022.03*

DELETE /dev-*2022.03*

DELETE /lambda-2022-03*

DELETE /cwl*


# obsadenost

GET _cat/allocation

GET /_cat/indices?v
```
health status index                           uuid                   pri rep docs.count docs.deleted store.size pri.store.size
yellow open   vector-2022.04.05               yuCbh3C4S5OJVE-PFchgNg   5   1     123800            0     60.4mb         60.4mb
yellow open   eks-label                       zDs7yn0nTUm-rRby2Olezg   5   1        155            0      2.3mb          2.3mb
green  open   kibana_sample_data_logs         HAIl8cL2Q_y9Ty9OIaG7sQ   1   0      14074            0     10.4mb         10.4mb
green  open   .opendistro-reports-definitions -g9-3US7QcGMy4pjbMc0gQ   1   0          0            0       208b           208b
yellow open   wireguard-2022-04-05            jtvd4lYoQF6ioGeNUo1YnA   5   1          2            0      9.5kb          9.5kb
green  open   .kibana_1                       QHEkEaRPTYaFl53a_MU81w   1   0        172           51      134kb          134kb
green  open   .opendistro-reports-instances   7vRXXTJwQwK8Q037_yGl0A   1   0          0            0       208b           208b
```


# volne miesto
GET _cat/nodes?h=h,diskAvail