#!/bin/bash
PROFILE="dev"

echo "Zapnut logovanie  v [ $PROFILE ] prostredi alebo Crtl+C"
read ans

aws --profile dev  logs describe-log-groups --max-items 999 | grep GroupName |grep -v LogsToElasticsearch | cut -d'"' -f4 | grep lambda > lambda_list
for lambda in `cat lambda_list` ; do 
echo $lambda
aws --profile $PROFILE  logs put-subscription-filter \
      --log-group-name $lambda \
      --filter-name "test" \
      --filter-pattern " " \
      --destination-arn "arn:aws:lambda:eu-central-1:504367035429:function:LogsToElasticsearch_kibana-logs_829421977047"

aws --profile $PROFILE logs put-retention-policy \
    --log-group-name $lambda \
    --retention-in-days 7

done
rm -f lambda_list