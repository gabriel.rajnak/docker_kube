from fastapi import FastAPI
import random
import time 

'''
    Exporter emulator for testing
'''

app = FastAPI()

tags = '{host="localhost" chain="abc"}'

# Static
'''
    Static metric
'''
@app.get("/static")
def static_m():
 return f'static_metric {tags} 0'

@app.get("/random")
def random_m():
 rnd =  random.randint(0,100)
 return f'random_metric {tags} {rnd}'

@app.get("/seconds")
def seconds():
    ts =(int) time.time() % 60
    return f'incremental {tags} {ts}'
 
