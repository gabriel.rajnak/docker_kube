#!/bin/bash
PROFILE="dev"
clear
echo "Zdaj filter pre zapnutie logovania na $PROFILE (lambda,rds,Gateway)"
read filter
echo "Stahujem zoznam lambda funkcii....."
aws --profile $PROFILE  logs describe-log-groups --max-items 9999 | grep GroupName |grep -v opensearch| cut -d'"' -f4 | grep $filter > lambda_list
arn=`aws ssm get-parameter --name "/app/common/infra/cw/elasticsearchLambdaArn" --profile $PROFILE | grep Value | cut -d'"' -f 4`
clear
echo "Cloudwatch ($filter) :"
echo "-----------------------"
cat lambda_list
echo "-----------------------"
echo "ARN lambda funkcie  : $arn"
echo "-----------------------"
echo "Zapnut logovanie  v [ $PROFILE ] prostredi alebo Crtl+C"
read ans


for lambda in `cat lambda_list` ; do 
echo $lambda
aws --profile $PROFILE  logs delete-subscription-filter \
      --log-group-name $lambda \
      --filter-name "test" \
      

aws --profile $PROFILE  logs put-subscription-filter \
      --log-group-name $lambda \
      --filter-name "Extract logs to Kibana" \
      --filter-pattern " " \
      --destination-arn $arn

aws --profile $PROFILE logs put-retention-policy \
    --log-group-name $lambda \
    --retention-in-days 7

done
rm -f lambda_list