#!/bin/bash


function lambda_cleanup()
{
    PROFILE=$1
    echo "*************************************************************"
    echo "***   Cleanup cloudwatch : $PROFILE"
    echo "*************************************************************"
    echo "Stahujem zoznam lambda funkcii..."
    aws  --profile $PROFILE  lambda list-functions --output json --output json --max-items 9999 --no-paginate \
        | grep FunctionName \
        | cut -d'"' -f 4 \
        > lambda.list

    echo "Stahujem zoznam cloudwatch group v [$PROFILE]..."
    aws --profile $PROFILE logs describe-log-groups   \
        | grep roupName \
        | grep lambda \
        | cut -d '"' -f 4    \
        > cloudwatch.list


    for i in `cat cloudwatch.list`; do
    name=`echo $i | cut -d'/' -f4 `
    status=`grep $name lambda.list`
    if [ -z "$status" ]; then
        echo "Delete ... $i"
        aws --profile $PROFILE  logs \
            delete-log-group \
            --log-group-name $i

    else             
        echo "OK ....... $i "
    fi
    done
    rm -f *.list
}


# echo "Stahujem zoznam rds ..."
# aws  --profile $PROFILE  rds describe-db-instances --output json  \   
#        | grep '"DBInstanceIdentifier'     \
#        | cut -d'"' -f 4 \
#        > rds.list

echo "Zmazanie starych cloudwatch group pre neexistujuce lambda funkcie"
echo "Enter/Ctrl+C"
read n

lambda_cleanup devops
lambda_cleanup dev
lambda_cleanup test 
lambda_cleanup uat

