


def LoadBook(path):
    with open(path) as f:
        text = f.read()
    return text


def getPrice(text):
    items = text.split('"')
    return items[3]


if __name__ == "__main__":
    path = "price.html"
    text = LoadBook(path)
    items = text.split("<")
    for item in items:
        if item.find('data-micro-price')>=0:
            #print(f"->>>>{item}")
            print(getPrice(item))
    
