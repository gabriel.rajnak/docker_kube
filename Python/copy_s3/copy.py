from concurrent import futures
import os
from google.cloud import storage

SOURCE_BUCKET_NAME = 'source'
DESTINATION_BUCKET_NAME = 'destination'
MAX_WORKERS = 10

def ensure_bucket(client, bucket_name):
    """Ensure the bucket exists. Create if it does not."""
    try:
        return client.get_bucket(bucket_name)
    except storage.exceptions.NotFound:
        return client.create_bucket(bucket_name)

def copy_blob(source_bucket, dest_bucket, blob_name):
    """Copies a blob from the source bucket to the destination bucket."""
    source_blob = source_bucket.blob(blob_name)
    destination_blob = dest_bucket.blob(blob_name)

    # Copy the blob
    destination_blob.rewrite(source_blob)
    ##print(f"Copied blob {blob_name} to {DESTINATION_BUCKET_NAME}")

def backup_bucket(message, content):
    client = storage.Client()

    # Ensure both buckets exist
    source_bucket = ensure_bucket(client, SOURCE_BUCKET_NAME)
    dest_bucket = ensure_bucket(client, DESTINATION_BUCKET_NAME)

    # List all blobs in the source bucket
    blobs = list(source_bucket.list_blobs())

    # Use ThreadPoolExecutor for parallel uploads
    with futures.ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
        copy_tasks = [
            executor.submit(copy_blob, source_bucket, f'{dest_bucket}/xxx', blob.name)
            for blob in blobs
        ]

        # Wait for all the futures to complete
        futures.wait(copy_tasks)

if __name__ == '__main__':
    backup_bucket(message, content)