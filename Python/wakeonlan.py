# pip install wakeonlan
from wakeonlan import send_magic_packet


known_computers = {
    'proxmox'   :  '00:9c:02:a7:f8:6f'
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    link/ether 00:9c:02:ab:db:e9 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::b463:57b:7439:b1ef/64 scope link noprefixroute
    link/ether 52:54:00:53:56:48 brd ff:ff:ff:ff:ff:ff
    link/ether 52:54:00:53:56:48 brd ff:ff:ff:ff:ff:ff
}


for name,mac in known_computers.items():
    print("Wake up {name}")
    send_magic_packet(mac)

