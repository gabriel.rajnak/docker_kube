class RBA:
    data ={}

    def __init__(self,user):
        self.LoadData(user)

    def userid(self):
        return self.data.id
    
    def LoadData(self, user):
        self.data = {
            'id': 123,
            'tables' : {
                    "aaa":"GET,POST,UPDATE,DELETE",
                    "bbb":"GET",
                    "ccc":"POST",
                    "ddd":"",
                    }
            }
        
    def canDo(self, objectName, method):        
        if 'tables' in self.data and objectName in self.data['tables'] and self.data['tables'][objectName].find(method) >= 0:
            return True
        return False
    
if __name__ =="__main__":
    role = RBA('pepe')
    print( role.canDo('aaa','GET') )
    print( role.canDo('aaa','POST') )
    print( role.canDo('ddd','POST') )
    print( role.canDo('zzz','POST') )


