# import pytest
import rba as rba



def test_happy():
    role = rba.RBA('pepe')
    assert role.evaluate('aaa','GET') == True, "Should be true"
    assert role.evaluate('aaa','POST') == True, "Should be true"
    assert role.evaluate('aaa','UPDATE') == True, "Should be true"
    assert role.evaluate('aaa','DELETE') == True, "Should be true"


def test_unhappy():
    role = rba.RBA('pepe')
    assert role.evaluate('bbb','GETX') == False, "Should be false"
    assert role.evaluate('ddd','GET') == False, "Should be false"
    assert role.evaluate('xxx','GET') == False, "Should be false"



if __name__ =="__main__":
    tests()

