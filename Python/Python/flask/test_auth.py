import auth as auth 

def test_empty():
    print("Hlupe testy...")
    action = auth.Auth()
    assert action.login("","") == None, "Should be None"
    assert action.login("user","") == None, "Should be None"
    assert action.login("","password") == None, "Should be None"

def test_basic():
    action = auth.Auth()
    action.users ={
        'gabo': { 'id':"1234", "passwd":'pass123'}
    }
    print(action.users)
    assert action.login("gabo","123") == None, "Should be None"    
    assert action.login("gabo","pass123") == '1234', "Should be 1234"     
    assert action.login("gabo","pass1") == None, "Should be None"     

if __name__ =="__main__":
    print("testujeme....")
    test_empty()
    test_basic()
