
class Auth:
    users ={}
    # Example:
    # {
    #     'gabo': { 'id':"1234", "passwd":'pass123'}
    #     'gabo': { 'id':"1234", "passwd":'pass123'}
    # }

    def __init__(self):
        pass
        # load data from file or DB


    def login(self, user:str, passwd:str)->str:        
        if user == "" or passwd == "":
            return None
        if user in self.users and 'passwd' in self.users[user] and passwd == self.users[user]['passwd']:
            return self.users[user]['id']
        return None
        

def debug():
    action = Auth()
    print(action.login("",""))

    
if __name__ =="__main__":
    debug()