from flask import Flask, request
import rba as rba
import auth as auth

config = { 
           'host': 'localhost',
           'port': 5000           
         }

app = Flask(__name__)

@app.route('/')
def index():
    return 'Index Page'

@app.route('/hello/<name>')
def hello(name=""):
    return f'Hello, World {name}'

# ================================================================
'''
 API :
  GET    /api/table_name/id
  POST   /api/table_name/id
  UPDATE /api/table_name/id
  DELETE /api/table_name/id

SEARCH:
  GET    /search/table_name/{condition}

'''

def process(objectName, id):
    pass

@app.route('/api/<objectName>/<id>') # ,methods=['GET']
def action(objectName:str, id:str):     
    # username = request.args['username']
    # password = request.args['password']
    # method = request.method
    return f"hello {objectName} - {id}"

    # # Login, who are you ?
    # autohirization = auth.Auth()
    # user = autohirization.login(username,password)
    # if user ==None:
    #     authetificaationFailed()
    #     return
    # # what can you do ?
    # role = rba.RBA()
    # if role.canDo(objectName, method):          
    #     process(objectName,id)
    # else:
    #     authorizationFailed()
    
# @app.route('/api/<objectName>/<id>',methods=['POST'])
# def post(objectName, id):
#     process(objectName,id)

# @app.route('/api/<objectName>/<id>',methods=['UPDATE'])
# def update(objectName, id):
#     process(objectName,id)

# @app.route('/api/<objectName>/<id>',methods=['DELETE'])
# def delete(objectName, id):
#     process(objectName,id)


def authetificaationFailed():
    return "Authentification failed"

def authorizationFailed():
    return "Authorization failed"

app.run( host=config['host'], port=config['port'] )

# flask --app hello run
# flask --app flask_server run --debug
