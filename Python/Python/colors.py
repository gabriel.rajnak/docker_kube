
name= "hero"
superpower ="fire"
enemy="wolf"
food="pizza"
live ="lake"

print(f"Hello \33[31m{name}\33[0m! Your ability to {superpower} will make sure you never have to look at {enemy} again. Go eat {food} as you walk down the streets of {live} and use {superpower} for good and not evil!")

"""
Color	Value
Default	0
Black	30
Red	    31
Green	32
Yellow	33
Blue	34
Purple	35
Cyan	36
White	37

12 factor app
https://opensource.com/article/21/11/open-source-12-factor-app-methodology

https://100daysofkubernetes.io/overview.html
https://www.100daysofcloud.com/


"""