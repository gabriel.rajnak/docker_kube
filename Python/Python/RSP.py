from getpass import getpass as input

print("E P I C    🪨  📄 ✂️    B A T T L")

print("player 1")
p1 = input("R/P/S")

print("player 2")
p2 = input("R/P/S")

game = p1.upper()+p2.upper()
print(game)

if game in ["RR", "PP" ,"SS"]:
  print("play again")

if game in ["RS", "PR" ,"SP"]:
  print("player 1 won")

if game in ["SR", "RP" ,"PS"]:
  print("player 2 won")

#RR RP RS 
#PR PP PS
#SR SP SS
