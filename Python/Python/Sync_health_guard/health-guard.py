#!/usr/bin/python3

import flask


config = { 'filename': '/var/blockchain/node_status//health',  
           'host': 'localhost',
           'port': 6000,
           # This is HTTP status reported when OK, syncing     
           'health_ok': 200,
           # This is HTTP status reported when ERR, not syncing
           'health_error': 500
         }

app = flask.Flask(__name__)

@app.route('/health', methods=['GET'])
def eval_sync():
    # Get status from file
    try:
        with open(config['filename'], 'r') as fh:
            fc_status = fh.readline().strip()
    except IOError as err:
        print(f'ERROR: can\'t open file {config["filename"]}')
        fc_status = ""
        return "ERROR", config['health_error']
    finally:
        fh.close()


    if (fc_status == 'node_health_ok' ):
        return "OK", config['health_ok']
    else:
        return "ERROR", config['health_error']

app.run( host=config['host'], port=config['port'] )
