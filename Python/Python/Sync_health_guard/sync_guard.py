#!/usr/bin/python3

import re
import requests
import time


config = {
    # Check delay [seconds]
    'delay': 60,
    # Acquire data from this metrics endpoint
    'cl_PROMETHEUS': 'http://10.255.102.55:5051', #'http://127.0.0.1:5051', # 5054
    'cl_MX_PATH': 'metrics',
    'el_PROMETHEUS': 'http://127.0.0.1:8551', # 6060

    'el_MX_PATH': 'debug/metrics/prometheus',
    # Destination file
    'dstf': '/var/blockchain/node_status/health',
    # OK for health_guard
    'syncing': 'node_health_ok',
    # ERR for health_guard (anything but ^)
    'not_syncing': 'node_unhealthy',
    # Blocks / min limit (lower bound)
    'bs_lim': 1,
    # Max block creation time (upper bound)
    'bc_lim': 180
}

patterns = {
    'clb': 'beacon_head_slot (\d+)',
    'elb': 'chain_head_block (\d+)'
}

rexo = {
    'clb': re.compile(patterns['clb']),
    'elb': re.compile(patterns['elb'])
}


def extract_vals(llst):
    '''
    Extracts beaconslot from list of metrics lines
    '''
    b = 0
    c = 0

    for el in llst:
        clb = rexo['clb'].match(el) # Consensu Layer BlockHeight
        elb = rexo['elb'].match(el) # Execution Layer BlockHeight

        if clb:
            c = int(clb.group(1))
        if elb:
            c = int(elb.group(1))

    return c


def set_file_content(fn, content):
    try:
        with open(fn, 'w') as fh:
            fh.write(content)

    except IOError as err:
        print(f'ERROR: can\'t write into {fn} [{err}])')
    finally:
        fh.close()


cl_beacon_slot   = [0 , 0]
el_beacon_slot   = [0 , 0]

while (1):
    condition = 0
    rel = ''
    el_rel = ''
    ### Read all Ethereum metrics
    try:
        response = requests.get(config['cl_PROMETHEUS'] + '/' + config['cl_MX_PATH'])
        el_response = requests.get(config['el_PROMETHEUS'] + '/' + config['el_MX_PATH'])
        rel = str(response.text).splitlines()
        el_rel = str(el_response.text).splitlines()
    except requests.exceptions.RequestException as reqerr:
        print(f'ERROR: can\'t reach metrics! [{reqerr}]')
        set_file_content(config['dstf'], config['not_syncing'])

    ### Get the beacon_slots
    cl_beacon_slot[1] = extract_vals(rel)
    el_beacon_slot[1] = extract_vals(el_rel)

    print(f'cl_beacon_slot[0] = {cl_beacon_slot[0]} |  el_beacon_slot[0] = {el_beacon_slot[0]}')
    print(f'cl_beacon_slot[1] = {cl_beacon_slot[1]} |  el_beacon_slot[1] = {el_beacon_slot[1]}')


    ### Evaluate condition(s)
    bs_diff_lob = config['bs_lim']*config['delay']/60 # beaconslot ... long term avg
    bc_upb = config['bc_lim'] # ETH block creation time upper-bound


    if (cl_beacon_slot[1] > cl_beacon_slot[0]) & (el_beacon_slot[1] > el_beacon_slot[0]) :
        # OK
        set_file_content(config['dstf'], config['syncing'])
    else:
        # ERROR
        set_file_content(config['dstf'], config['not_syncing'])

    ### set & increment
    cl_beacon_slot[0] = cl_beacon_slot[1]
    el_beacon_slot[0] = el_beacon_slot[1]
    ### Wait and repeat
    time.sleep(config['delay'])
