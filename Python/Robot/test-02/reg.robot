#
# https://github.com/robotframework/HowToWriteGoodTestCases/blob/master/HowToWriteGoodTestCases.rst
#

*** Settings ***
Library  Selenium2Library
# Library  SeleniumLibrary
# Suite Setup     Open browser    ${URL}   ${BROWSER}
# Suite Teardown  Close All Browsers

*** Variables ***

${vcera}            15.7.2022
${dnes}             16.7.2022
${zajtra}           17.7.2022
${rano}             rano
${vecer}            vecer
${BROWSER}          Chrome
${SEARCH_URL}       https://register.spokojnamacka.sk
${name}             Gabo Rajnak
${phone}            0948605036
${email}            gabriel.rajnak@gmail.com
${prichod}          10.10.2022
${cas_prichodu}     id:prichod1
${odchod}           15.10.2022
${cas_odchodu}      id:odchod2
${pocet}            1
${note}             super_hash_text

# ==========================================================================
*** Test Cases ***

# Dummy Test
#     Priprava
#     Close All Browsers

Prichod vcera
    Priprava
    Input Text      id:prichod   ${vcera}    
    Akcia
    Page Should Contain          Zadajte
    Koniec

Odchod zajtra 
    Priprava
    Input Text      id:odchod   ${vcera}    
    Akcia
    Page Should Contain          Zadajte
    Koniec
   
Vysoky pocet zvierat
    Priprava
    Input Text      id:count     99
    Akcia
    Page Should Contain          Skontrolujte
    Koniec

Nula pocet zvierat
    Priprava
    Input Text      id:count     0
    Akcia
    Page Should Contain          Skontrolujte
    Koniec
# ======================================================================================
*** Keywords ***

Priprava
    Open Browser  ${SEARCH_URL}  browser=${BROWSER}  
    sleep  2s
    Input Text      id:name      ${name}
    Input Text      id:phone     ${phone}
    Input Text      id:email     ${email}
    Input Text      id:prichod   ${prichod}
    Click Element                ${cas_prichodu}
    Input Text      id:odchod    ${odchod}
    Click Element                ${cas_odchodu}
    Input Text      id:count     1    

Koniec
    Input Text      id:note      ${note}
    sleep   1
    Close All Browsers

Akcia    
    Input Text      id:note      ${note}
    # tento keypress mi nefunguje
    # Press Keys  None  TAB
