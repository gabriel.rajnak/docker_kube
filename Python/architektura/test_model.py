from mysql_model import *

config= {'host':'localhost','port':3306,'login':'root','password':'1q2w3e4r'}
table = "login"
# =========================================================================
def test_convert():
    data = {'id':10,'name':'gabo','age':48}    
    m = MySQLModel(config, table)
    assert m.data_to_str({}) == ""
    assert m.data_to_str({'id':5}) == "id='5'"
    assert m.data_to_str(data) == "id='10',name='gabo',age='48'"
# =========================================================================
def test_get():    
    m = MySQLModel(config, table)
    assert m.get('1') == {} , "should be empty"    
    res =m.get('2')
    print(res)
    assert res['name'] == 'Gabo' , "should be Gabo"
# =========================================================================
def test_create():    
    m = MySQLModel(config, table)
    assert m.create({'id':'0','data':'new'}) == '11'
# =========================================================================
def test_update():
    m = MySQLModel(config, table)
    assert m.update({'id':'7','data':'new'}) == '7'
# =========================================================================
def test_delete():
    m = MySQLModel(config, table)
    assert m.delete('5') =='5'
    
# =========================================================================
def test_wrapper():
    pass
# =========================================================================
