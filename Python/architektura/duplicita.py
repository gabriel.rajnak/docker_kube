import os
SRC = '/Users/bitcoin/tmp/mirror_zaloha/filmy'

'''
    Program na upratanie dokumentov, foto, video

    src_dir - adresar, ktory chcem pridat
    dst_dir - cielovy adresak, kam sa moze pridat ak je vsetko v poroadku


    1. AK nie je duplicita, zarad do dalsieho spracovania
        - 
    2. Ak existuje duplicita ignoruj / presun do adrchivu

'''

def get_info_file(file):
    parts = file.split('/')
    file_name = parts[-1]
    extension = file_name.split('.')[-1].lower()
    path = file[0 : len(file)-len(file_name)]
    try:
        file_size = os.path.getsize(file)        
    except:
        file_size = 0        
    if os.path.isdir(file):
        extension ='_dir'
        file_size = 0
        xtype ='d'
    else:
        xtype ='f'

    return {'file':file_name,'path':path,'size':file_size,'ext':extension,'type':xtype}

def find_file(file):
    '''
    Find file in local database
    SELECT * FROM table WHERE name==file.name AND size==file.size

    Table:
        - id
        - name 
        - path 
        - size
    '''
    return False

def insert_file(file):
    '''
    Inser file to local database
    '''
    print("\t\tInsert to DB ",file)

def pass_file(file):
    # move file to destination dir
    new_path = "xxxx"
    print("Move to repository ",file)

    insert_file(file)
    pass

def backup_file(file):
    # move to backup dir / ignore file
    print("Backup ",file)
    pass


def file_list(myPath):
    try:
        filelist=  os.listdir(myPath)
    except:
        filelist= []
    return filelist




def process_file(src):
    file = get_info_file(src)
    if file['type'] == 'f' :
        if find_file(file) ==False:
            pass_file(file)
        else:
            backup_file(file)


def main():
    flist = file_list(SRC)
    # print(flist)
    for f in flist:
        #print(f)
        process_file(f)



if __name__ == '__main__':
    main()