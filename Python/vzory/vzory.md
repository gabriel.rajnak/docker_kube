# Vzory (design patterns)
## Tvorba
- factory
- singleton
- prototyp
- builder

## Chovanie
- observer
- strategy
- template method
- state
- memento
- interpret
- mediator
- iterator
- chain of responsibility

---
### Factory
- tvori objekty

```python
class Employee:

    def __init__(self, a,b,c):
        pass


def createEmployee()
    return new Emplyee(1,2,3)
```

### Singleton


